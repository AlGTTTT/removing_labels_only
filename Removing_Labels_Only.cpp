
#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

#include "Removing_Labels_Only_function.cpp"

int main()
{
	int doRemovingOfLabels_BlackWhiteImage(

		const Image& image_in,

		//const PARAMETERS_REMOVAL_OF_LABELS *sPARAMETERS_REMOVAL_OF_LABELSf,

		Image& image_out);

	int
		nRes;

		////////////////////////////////////////////////////////////////////////////////////////////////
/*
	fout_lr = fopen("wMain_RemovingLabels.txt", "w");

	if (fout_lr == NULL)
	{
		printf("\n\n fout_lr == NULL");
		getchar(); exit(1);
	} //if (fout_lr == NULL)
*/
	Image image_in;

	//image_in.read("IMAGO_CASE_03_MGaddV_0020001_mask.png");

	//image_in.read("01-051-0_RCC_initialmask.png");
	
	//image_in.read("01-108-4_LCC_initialmask.png");
	//image_in.read("01-090-3_RCC_initialmask.png");

	//image_in.read("01-015-4_LMLO_initialmask.png");
	//image_in.read("02-013-0_RCC_initialmask.png");

/////////////////////////////////////////////////////////////
//LookRoughHoles
	//image_in.read("01-284-0_RMLO_initialmask.png");
	//image_in.read("01-341-0_LMLO_initialmask.png");
	//image_in.read("01-341-0_RMLO_initialmask.png");
	//image_in.read("01-371-0_LMLO_initialmask.png");
	//image_in.read("02-014-0_RMLO_initialmask.png");
	//image_in.read("02-026-0_LCC_1_initialmask.png");
	//image_in.read("02-026-0_LMLO_1_initialmask.png");
	//image_in.read("02-031-0_LCC_1_initialmask.png");
	//image_in.read("02-031_LMLO_1_initialmask.png");
	//image_in.read("02-034-0_RCC_1_initialmask.png");
	//image_in.read("02-073_LMLO_initialmask.png");
	//image_in.read("02-073-0_RMLO_initialmask.png");
	//image_in.read("02-095_RMLO_initialmask.png");
	//image_in.read("03-091_RCC_initialmask.png");
	//image_in.read("03-093-0_LMLO_initialmask.png");
	//image_in.read("03-093-0_RCC_initialmask.png");
	//image_in.read("03-093-0_RMLO_initialmask.png");

	//image_in.read("01-144_LCC_initialmask.png");
	//image_in.read("01-139_RMLO_initialmask.png");

	image_in.read("01-221-0_LMLO_initialmask.png");

	Image image_out;
	
	nRes = doRemovingOfLabels_BlackWhiteImage(
				image_in, // Image& image_in,

		image_out); // Image& image_out);

	if (nRes == UNSUCCESSFUL_RETURN) // -1
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n The image has not been processed properly for label removal and mask creation.");
		printf("\n Please adjust the algorithm.");

		fprintf(fout_lr,"\n\n The image has not been processed properly for label removal and mask creation.");
		fprintf(fout_lr,"\n Please adjust the algorithm.");

		fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n Press any key to exit");	getchar(); 
		return UNSUCCESSFUL_RETURN; //
	} // if (nRes == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n After 'doRemovingOfLabels_BlackWhiteImage'");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		//image_out.write("01-001-0-LMLO_c_Mask.png");
			//image_out.write("0010001_Mask.png");

	//image_out.write("IMAGO_CASE_03_MGaddV_0020001_mask_NoLabels.png");
	//image_out.write("01-051-0_RCC_mask_NoLabels.png");
	
	//image_out.write("01-108-4_LCC_mask_NoLabels.png");
	//image_out.write("01-090-3_RCC_mask_NoLabels.png");

	//image_out.write("01-015-4_LMLO_mask_NoLabels.png");
	//image_out.write("02-013-0_RCC_mask_NoLabels.png");

/////////////////////////////////////////////////////////////
//LookRoughHoles
	//image_out.write("01-284-0_RMLO_mask_NoLabels.png");
	//image_out.write("01-341-0_LMLO_mask_NoLabels.png");
	//image_out.write("01-341-0_RMLO_mask_NoLabels.png");
	//image_out.write("01-371-0_LMLO_mask_NoLabels.png");
	//image_out.write("02-014-0_RMLO_mask_NoLabels.png");
	//image_out.write("02-026-0_LCC_1_mask_NoLabels.png");
	//image_out.write("02-026-0_LMLO_1_mask_NoLabels.png");
	//image_out.write("02-031-0_LCC_1_mask_NoLabels.png");
	//image_out.write("02-031_LMLO_1_mask_NoLabels.png");
	//image_out.write("02-034-0_RCC_1_mask_NoLabels.png");
	//image_out.write("02-073_LMLO_mask_NoLabels.png");
	//image_out.write("02-073-0_RMLO_mask_NoLabels.png");
	//image_out.write("02-095_RMLO_mask_NoLabels.png");
	//image_out.write("03-091_RCC_mask_NoLabels.png");
	//image_out.write("03-093-0_LMLO_mask_NoLabels.png");
	//image_out.write("03-093-0_RCC_mask_NoLabels.png");
	//image_out.write("03-093-0_RMLO_mask_NoLabels.png");

	//image_out.write("01-144_LCC_mask_NoLabels.png");
	//image_out.write("01-139_RMLO_LCC_mask_NoLabels.png");

	image_out.write("01-221-0_LMLO_mask_NoLabels.png");

	printf("\n\n The mask image without labels has been saved");

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n The mask image without labels has been saved");

	fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n Please press any key to exit"); getchar(); //exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	return SUCCESSFUL_RETURN;
} //int main()

//printf("\n\n Please press any key:"); getchar();