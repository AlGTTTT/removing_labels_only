//#include "Removing_Labels_Fr_Color_Images_2.h"
#include "Removing_Labels_Only_2.h"

using namespace imago;

FILE *fout_lr;

//int
	//nPrintInsideShadingBoundaryOfObject_By_Padding_Points_And_B_Spline_Glob = 0, // no printing
	//iIterGlob,
	//nWidGlob;

int doRemovingOfLabels_BlackWhiteImage(
	const Image& image_in,

	//const PARAMETERS_REMOVAL_OF_LABELS *sParameters_Removal_Of_Labelsf,

	Image& image_out)
{

	int Initializing_Color_To_CurSize(
		const int nImageWidthf,
		const int nImageLengthf,

		COLOR_IMAGE *sColor_Imagef);

	int DeterminingSideOfObjectLocation_AndBackground_BlackWhite(
		//const PARAMETERS_REMOVAL_OF_LABELS *sParameters_Removal_Of_Labelsf,

		COLOR_IMAGE *sColor_Imagef);

	int Boundaries_And_ErasingWhiteRows(

		const int nDiffOfLensBetweenLast_2WhitePixelsMinf,
		const int nDistFor_ObjectNeighborsAtCertainAnglesf,
		const int nDistFor_ObjectNeighborsAtCertainAngles_CloseToBoundaryf,

		const int nLength_HalfRange_CloseToBoundaryf,

		const int nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf,

		const float fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf,

		const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,
		const float fRatioOfWhiteAndBlackPixelsInARowMaxf,

		const int nWidToCloseToEdgef,
		const int nLen_ToFindBlackPixelsf,

		COLOR_IMAGE *sColor_Imagef,

		int &nNumOfErasedRowsTotf,
		int &nNumOfPixelConvertedFromWhiteToBlackTotf);
/*
	int DeterminingLenOfObjectBoundary_BlackWhite(

		const int nDiffOfLensBetweenLast_2WhitePixelsMinf,

		const float fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf,

		const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,

		COLOR_IMAGE *sColor_Imagef);
*/

	int Erasing_Labels_BlackWhite(

		COLOR_IMAGE *sColor_Imagef);

	void MedianFiterTo_AnArea_InAMaskImage(
		const int nWidMinf,
		const int nWidMaxf,//inclusive

		const int nLenMinf,
		const int nLenMaxf,////inclusive

		COLOR_IMAGE *sColor_Imagef);

	int Smoothing_Boundary_Jaggedness_InAMaskImage(

		const int nBoundaryJumpMaxf,

		const int nWidOfJaggedSegmentMaxf,

		const int nNumOfIters_ForShadingBondaryPixelsMaxf,

		const float fCoefDist_ForNippleToTopf,
		const float fCoefDist_ForNippleToBottomf,

		//////////////////////////////
		int &nNumOfJaggedSegmentsTotf,

		COLOR_IMAGE *sColor_Imagef);

	void MedianFiterTo_BackgroundArea_InAMaskImage(

		int &nNumOfPixels_ShadedToBlackTotf,

		COLOR_IMAGE *sColor_Imagef);

	int FillingOut_BlackObjectPixels_WhiteBlack_Image(
		const int nWidOfABlackAreaMaxf,
		const int nLenOfABlackAreaMaxf,
		const int nDropInBoundary_ForBlackAreaMaxf,

		int &nNumOf_ErasedBlackPixelsTotf,
		COLOR_IMAGE *sColor_Imagef); //[nImageSizeMax]

	void MedianFiterTo_ObjectArea_InAMaskImage(

		const int nNumOfIters_ForMedianFiterTo_ObjectAreaMaxf,
		int &nNumOfPixels_ShadedToWhiteTotf,

		COLOR_IMAGE *sColor_Imagef);

	void MedianFiterTo_BoundaryArea(

		int &nNumOfChangedPixelsCloseToBoundaryTotf,
		COLOR_IMAGE *sColor_Imagef);

	int MedianFiterTo_BlackAreaBehindBoundary(

		int &nWidToStartForBlackAreaBehindBoundaryf,

		int &nWid_BlackPixelsBehindBoundaryMinf,
		int &nWid_BlackPixelsBehindBoundaryMaxf,

		int &nNumOfChangedPixelsBehindBoundaryTotf,

		COLOR_IMAGE *sColor_Imagef);

	int
		nRes;

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	int
		i,
		j,

		nSizeOfImage, // = nImageWidth*nImageHeight,

		nIndexOfPixelCur,

		iLen,
		iWid,

		iIterForMedianFilterf,

		nIterOfSmoothingf,
		nNumOfChangedPixelsCloseToBoundaryTotf,

		nRed,
		nGreen,
		nBlue,

		nWidToStartForBlackAreaBehindBoundaryf,

		nWid_BlackPixelsBehindBoundaryMinf,
		nWid_BlackPixelsBehindBoundaryMaxf,

		nNumOfChangedPixelsBehindBoundaryTotf,
				
		nIntensity_Read_Test_ImageMax = -nLarge,

		nNumOfErasedRowsTot,

		nNumOf_ErasedBlackPixelsTot,
		nNumOfPixelConvertedFromWhiteToBlackTot,

		nNumOfJaggedSegmentsTotf,

		nImageWidth,
		nImageHeight;
	////////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	fout_lr = fopen("wMain_RemovingLabels.txt", "w");

	if (fout_lr == NULL)
	{
		printf("\n\n fout_lr == NULL");
		//getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fout_lr == NULL)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	  // size of image
	nImageWidth = image_in.width();
	nImageHeight = image_in.height();

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);
	fprintf(fout_lr, "\n\n nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nImageWidth > nLenMax || nImageWidth < nLenMin)
	{
//#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in reading the image width: nImageWidth = %d", nImageWidth);
		printf("\n\n An error in reading the image width: nImageWidth = %d", nImageWidth);
		//getchar(); exit(1);
		
//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nImageWidth > nLenMax || nImageWidth < nLenMin)

	if (nImageHeight > nWidMax || nImageHeight < nWidMin)
	{
//#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in reading the image height: nImageHeight = %d", nImageHeight);
		fprintf(fout_lr, "\n\n An error in reading the image height: nImageHeight = %d", nImageHeight);
//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nImageHeight > nWidMax || nImageHeight < nWidMin)

	int bytesOfWidth = image_in.pitchInBytes();

	Image imageToSave(nImageWidth, nImageHeight, bytesOfWidth);

	///////////////////////////////////////////////////////
	COLOR_IMAGE sColor_Image; //

	nRes = Initializing_Color_To_CurSize(
		nImageHeight, //const int nImageWidthf,
		nImageWidth, //const int nImageLengthf,

		&sColor_Image); // COLOR_IMAGE *sColor_Imagef);

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		//if (sColor_Imagef->nLenObjectBoundary_Arr == nullptr || sColor_Imagef->nRed_Arr == nullptr || sColor_Imagef->nGreen_Arr == nullptr || sColor_Imagef->nBlue_Arr == nullptr ||
			//sColor_Imagef->nIsAPixelBackground_Arr == nullptr)

		delete[] sColor_Image.nLenObjectBoundary_Arr;

		delete[] sColor_Image.nRed_Arr;
		delete[] sColor_Image.nGreen_Arr;
		delete[] sColor_Image.nBlue_Arr;
		delete[] sColor_Image.nIsAPixelBackground_Arr;

		return UNSUCCESSFUL_RETURN;
	} //if (nRes == UNSUCCESSFUL_RETURN)

	nSizeOfImage = nImageWidth*nImageHeight;

	//finding 'nIntensity_Read_Test_ImageMax' to decide if the intensity rescaling to (0,255) is needed
	for (j = 0; j < nImageHeight; j++)
	{
		for (i = 0; i < nImageWidth; i++)
		{
			nIndexOfPixelCur = i + j*nImageWidth;

			nRed = image_in(j, i, R);
			nGreen = image_in(j, i, G);
			nBlue = image_in(j, i, B);

			if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)
			{
#ifndef COMMENT_OUT_ALL_PRINTS

				fprintf(fout_lr, "\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, i = %d, j = %d", nRed, nGreen, nBlue, i, j);
				printf("\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, i = %d, j = %d", nRed, nGreen, nBlue, i, j);
				//printf("\n\n Please press any key to exit");
				//getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} // if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)

			if (nRed > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nRed;

			if (nGreen > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nGreen;

			if (nBlue > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nBlue;

		} // for (int i = 0; i < nImageWidth; i++)

	}//for (int j = 0; j < nImageHeight; j++)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
	fprintf(fout_lr, "\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	////////////////////////////////////////////////////////////////////////////////////////////

	for (j = 0; j < nImageHeight; j++)
	{
		for (i = 0; i < nImageWidth; i++)
		{
			nIndexOfPixelCur = i + j*nImageWidth;

			nRed = image_in(j, i, R);
			nGreen = image_in(j, i, G);
			nBlue = image_in(j, i, B);

			iLen = i;
			iWid = j;

			if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			{
				//rescaling

				sColor_Image.nRed_Arr[nIndexOfPixelCur] = nRed / 256;

				sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nGreen / 256;
				sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nBlue / 256;

				//////////////////////////////////////////////////////////////////////////////////////////////////////
				sColor_Image.nRed_Arr[nIndexOfPixelCur] = nRed;
				sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nGreen;
				sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nBlue;

			} //if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			else
			{
				// no rescaling

				sColor_Image.nRed_Arr[nIndexOfPixelCur] = nRed;
				sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nGreen;
				sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nBlue;
			} //else

		} // for (int i = 0; i < nImageWidth; i++)
	}//for (int j = 0; j < nImageHeight; j++)

	 ///////////////////////////////////////////////////////////////////////////
	nRes = DeterminingSideOfObjectLocation_AndBackground_BlackWhite(

		&sColor_Image); //COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		delete[] sColor_Image.nRed_Arr;
		delete[] sColor_Image.nGreen_Arr;
		delete[] sColor_Image.nBlue_Arr;

		delete[] sColor_Image.nLenObjectBoundary_Arr;
		delete[] sColor_Image.nIsAPixelBackground_Arr;
		return UNSUCCESSFUL_RETURN;
	}// if (nRes == UNSUCCESSFUL_RETURN)


#ifndef COMMENT_OUT_ALL_PRINTS
	printf( "\n\n 'DeterminingSideOfObjectLocation_AndBackground_BlackWhite' has been passed");
	fprintf(fout_lr, "\n\n 'DeterminingSideOfObjectLocation_AndBackground_BlackWhite' has been passed");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	nRes = Boundaries_And_ErasingWhiteRows(

		nDiffOfLensBetweenLast_2WhitePixelsMin, //const int nDiffOfLensBetweenLast_2WhitePixelsMinf,
		nDistFor_ObjectNeighborsAtCertainAngles, //const int nDistFor_ObjectNeighborsAtCertainAnglesf,
		nDistFor_ObjectNeighborsAtCertainAngles_CloseToBoundary, //const int nDistFor_ObjectNeighborsAtCertainAngles_CloseToBoundaryf,

			nLength_HalfRange_CloseToBoundary, //const int nLength_HalfRange_CloseToBoundaryf,

		nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRow, //const int nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf,

		fCoefDist_ForWhiteObjectPixels_ToImageEdgeMax, //const float fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf,

		fCoefDist_ForVerticalNeighbors_ToImageEdgeMax, //const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,

		fRatioOfWhiteAndBlackPixelsInARowMax, //const float fRatioOfWhiteAndBlackPixelsInARowMaxf,

		nWidToCloseToEdge, //const int nWidToCloseToEdgef,
		nLen_ToFindBlackPixels, //const int nLen_ToFindBlackPixelsf,

		&sColor_Image, //COLOR_IMAGE *sColor_Imagef,

		nNumOfErasedRowsTot, //int &nNumOfErasedRowsTotf,

		nNumOfPixelConvertedFromWhiteToBlackTot); // int &nNumOfPixelConvertedFromWhiteToBlackTotf)

		if (nRes == UNSUCCESSFUL_RETURN)
		{
			delete[] sColor_Image.nRed_Arr;
			delete[] sColor_Image.nGreen_Arr;
			delete[] sColor_Image.nBlue_Arr;

			delete[] sColor_Image.nLenObjectBoundary_Arr;
			delete[] sColor_Image.nIsAPixelBackground_Arr;
			return UNSUCCESSFUL_RETURN;
		}// if (nRes == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'Boundaries_And_ErasingWhiteRows' has been passed, nNumOfErasedRowsTot = %d, nNumOfPixelConvertedFromWhiteToBlackTot = %d", nNumOfErasedRowsTot,nNumOfPixelConvertedFromWhiteToBlackTot);
		fprintf(fout_lr, "\n\n 'Boundaries_And_ErasingWhiteRows' has been passed, nNumOfErasedRowsTot = %d, nNumOfPixelConvertedFromWhiteToBlackTot = %d", nNumOfErasedRowsTot, nNumOfPixelConvertedFromWhiteToBlackTot);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS


	/////////////////////////////////////////////////////////////////
	 nRes = Erasing_Labels_BlackWhite(

		&sColor_Image); //COLOR_IMAGE *sColor_Imagef);
						/////////////////////////////////////////////////////////////////////////////////////////////
	if (nRes == UNSUCCESSFUL_RETURN)
	{
		delete[] sColor_Image.nRed_Arr;
		delete[] sColor_Image.nGreen_Arr;
		delete[] sColor_Image.nBlue_Arr;

		delete[] sColor_Image.nLenObjectBoundary_Arr;

		delete[] sColor_Image.nIsAPixelBackground_Arr;
		return UNSUCCESSFUL_RETURN;
	}// if (nRes == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'Erasing_Labels_BlackWhite' has been passed");
	fprintf(fout_lr, "\n\n 'Erasing_Labels_BlackWhite' has been passed");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//////////////////////////////////////////////////////////////
// iters of smoothing
	nIterOfSmoothingf = 0;
MarkNextIterOfSmoothing:	nIterOfSmoothingf += 1; 

	nRes = Smoothing_Boundary_Jaggedness_InAMaskImage(

		nBoundaryJumpMax, //const int nBoundaryJumpMaxf,

			nWidOfJaggedSegmentMax, //const int nWidOfJaggedSegmentMaxf,
		nNumOfIters_ForShadingBondaryPixelsMax, //const int nNumOfIters_ForShadingBondaryPixelsMaxf,

			fCoefDist_ForNippleToTop, //const float fCoefDist_ForNippleToTopf,
			fCoefDist_ForNippleToBottom, //const float fCoefDist_ForNippleToBottomf,
		//////////////////////////////
		nNumOfJaggedSegmentsTotf, //int &nNumOfJaggedSegmentsTotf,

		&sColor_Image); // COLOR_IMAGE *sColor_Imagef);

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		delete[] sColor_Image.nRed_Arr;
		delete[] sColor_Image.nGreen_Arr;
		delete[] sColor_Image.nBlue_Arr;

		delete[] sColor_Image.nLenObjectBoundary_Arr;
		delete[] sColor_Image.nIsAPixelBackground_Arr;
		return UNSUCCESSFUL_RETURN;
	}// if (nRes == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n After 'Smoothing_Boundary_Jagg...': nNumOfJaggedSegmentsTotf = %d, nIterOfSmoothingf = %d", nNumOfJaggedSegmentsTotf, nIterOfSmoothingf);
	fprintf(fout_lr, "\n\n After 'Smoothing_Boundary_Jagg...' nNumOfJaggedSegmentsTotf = %d, nIterOfSmoothingf = %d", nNumOfJaggedSegmentsTotf, nIterOfSmoothingf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nIterOfSmoothingf == 1)
	{
		nRes = Erasing_Labels_BlackWhite(

			&sColor_Image); //COLOR_IMAGE *sColor_Imagef);
							/////////////////////////////////////////////////////////////////////////////////////////////
		if (nRes == UNSUCCESSFUL_RETURN)
		{
			delete[] sColor_Image.nRed_Arr;
			delete[] sColor_Image.nGreen_Arr;
			delete[] sColor_Image.nBlue_Arr;

			delete[] sColor_Image.nLenObjectBoundary_Arr;

			delete[] sColor_Image.nIsAPixelBackground_Arr;
			return UNSUCCESSFUL_RETURN;
		}// if (nRes == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'Erasing_Labels_BlackWhite' has been passed");
		fprintf(fout_lr, "\n\n 'Erasing_Labels_BlackWhite' has been passed");

		fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	} // if (nIterOfSmoothingf == 1)

	if (nNumOfJaggedSegmentsTotf > 0 && nIterOfSmoothingf <= nIterOfSmoothingMax)
	{
		goto MarkNextIterOfSmoothing;
	}//if (nNumOfJaggedSegmentsTotf > 0  && nIterOfSmoothingf <= nIterOfSmoothingMax)
/////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////

	for (iIterForMedianFilterf = 0; iIterForMedianFilterf < nNumOfIterForMedianFilter_BoundaryMax; iIterForMedianFilterf++)
	{
		//iIterGlob = iIte_MedFilf;
		MedianFiterTo_BoundaryArea(

			nNumOfChangedPixelsCloseToBoundaryTotf, //int &nNumOfChangedPixelsCloseToBoundaryTotf,
			&sColor_Image); // MASK_IMAGE *sMask_Imagef);

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'MedianFiterTo_BoundaryArea': iIterForMedianFilterf = %d, nNumOfChangedPixelsCloseToBoundaryTotf = %d", iIterForMedianFilterf, nNumOfChangedPixelsCloseToBoundaryTotf);

		fprintf(fout_lr, "\n\n 'MedianFiterTo_BoundaryArea': iIterForMedianFilterf = %d, nNumOfChangedPixelsCloseToBoundaryTotf = %d", iIterForMedianFilterf, nNumOfChangedPixelsCloseToBoundaryTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		if (nNumOfChangedPixelsCloseToBoundaryTotf < nNumOfChangedPixelsMin)
		{

#ifndef COMMENT_OUT_ALL_PRINTS
			printf( "\n\n Break from 'MedianFiterTo_BoundaryArea' by nNumOfChangedPixelsCloseToBoundaryTotf = %d < nNumOfChangedPixelsMin = %d ",
					nNumOfChangedPixelsCloseToBoundaryTotf, nNumOfChangedPixelsMin);

			fprintf(fout_lr, "\n\n Break from 'MedianFiterTo_BoundaryArea' by nNumOfChangedPixelsCloseToBoundaryTotf = %d < nNumOfChangedPixelsMin = %d ",
					nNumOfChangedPixelsCloseToBoundaryTotf, nNumOfChangedPixelsMin);
		#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			break;
		} // if (nNumOfChangedPixelsCloseToBoundaryTotf < nNumOfChangedPixelsMin)
	}//for (iIterForMedianFilterf = 0; iIterForMedianFilterf < nNumOfIterForMedianFilter_BoundaryMax; iIterForMedianFilterf++)

/////////////////////////////////////////////////////////////////
	nWidToStartForBlackAreaBehindBoundaryf = 0;
	nWid_BlackPixelsBehindBoundaryMinf = -1;
	nWid_BlackPixelsBehindBoundaryMaxf = -1;
	for (iIterForMedianFilterf = 0; iIterForMedianFilterf < nNumOfIterForMedianFilter_BlackAreasBehindBoundaryMax; iIterForMedianFilterf++)
	{
		//iIterGlob = iIte_MedFilf;
		nRes = MedianFiterTo_BlackAreaBehindBoundary(

			nWidToStartForBlackAreaBehindBoundaryf, //int &nWidToStartForBlackAreaBehindBoundaryf,

			nWid_BlackPixelsBehindBoundaryMinf, //int &nWid_BlackPixelsBehindBoundaryMinf,
			
			nWid_BlackPixelsBehindBoundaryMaxf, //int &nWid_BlackPixelsBehindBoundaryMaxf,

			nNumOfChangedPixelsBehindBoundaryTotf, //int &nNumOfChangedPixelsBehindBoundaryTotf,

			&sColor_Image); // MASK_IMAGE *sMask_Imagef);


		if (nRes == UNSUCCESSFUL_RETURN)
		{
			delete[] sColor_Image.nRed_Arr;
			delete[] sColor_Image.nGreen_Arr;
			delete[] sColor_Image.nBlue_Arr;

			delete[] sColor_Image.nLenObjectBoundary_Arr;

			delete[] sColor_Image.nIsAPixelBackground_Arr;

			return UNSUCCESSFUL_RETURN;
		} // if (nRes == UNSUCCESSFUL_RETUR
		
		if (nRes == -2)
		{
			//No black pixels behind the boundary
			break;
		} //if (nRes == -2)

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'MedianFiterTo_BlackAreaBehindBoundary': iIterForMedianFilterf = %d, nNumOfChangedPixelsBehindBoundaryTotf = %d",
			iIterForMedianFilterf, nNumOfChangedPixelsBehindBoundaryTotf);

		printf("\n nWid_BlackPixelsBehindBoundaryMinf = %d, nWid_BlackPixelsBehindBoundaryMaxf = %d, nWidToStartForBlackAreaBehindBoundaryf = %d",
			nWid_BlackPixelsBehindBoundaryMinf, nWid_BlackPixelsBehindBoundaryMaxf, nWidToStartForBlackAreaBehindBoundaryf);

		fprintf(fout_lr, "\n\n 'MedianFiterTo_BlackAreaBehindBoundary': iIterForMedianFilterf = %d, nNumOfChangedPixelsBehindBoundaryTotf = %d",
			iIterForMedianFilterf, nNumOfChangedPixelsBehindBoundaryTotf);

		fprintf(fout_lr, "\n nWid_BlackPixelsBehindBoundaryMinf = %d, nWid_BlackPixelsBehindBoundaryMaxf = %d, nWidToStartForBlackAreaBehindBoundaryf = %d",
			nWid_BlackPixelsBehindBoundaryMinf, nWid_BlackPixelsBehindBoundaryMaxf, nWidToStartForBlackAreaBehindBoundaryf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		if (nNumOfChangedPixelsBehindBoundaryTotf < nNumOfChangedPixels_BehindBoundaryMin)
		{

#ifndef COMMENT_OUT_ALL_PRINTS
		
			printf("\n\n Break from 'MedianFiterTo_BlackAreaBehindBoundary' by nNumOfChangedPixelsBehindBoundaryTotf = %d < nNumOfChangedPixels_BehindBoundaryMin = %d ",
				nNumOfChangedPixelsBehindBoundaryTotf, nNumOfChangedPixels_BehindBoundaryMin);

			fprintf(fout_lr, "\n\n Break from 'MedianFiterTo_BlackAreaBehindBoundary' by nNumOfChangedPixelsBehindBoundaryTotf = %d < nNumOfChangedPixels_BehindBoundaryMin = %d ",
				nNumOfChangedPixelsBehindBoundaryTotf, nNumOfChangedPixels_BehindBoundaryMin);
			
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			break;
		} // if (nNumOfChangedPixelsBehindBoundaryTotf < nNumOfChangedPixels_BehindBoundaryMin)

	}//for (iIterForMedianFilterf = 0; iIterForMedianFilterf < nNumOfIterForMedianFilter_BlackAreasBehindBoundaryMax; iIterForMedianFilterf++)
/////////////////////////////////////////////////
	
	nRes = FillingOut_BlackObjectPixels_WhiteBlack_Image(
		nWidOfABlackAreaMax, //const int nWidOfABlackAreaMaxf,
		nLenOfABlackAreaMax, //const int nLenOfABlackAreaMaxf,

		nDropInBoundary_ForBlackAreaMax, //const int nDropInBoundary_ForBlackAreaMaxf,

		nNumOf_ErasedBlackPixelsTot, //int &nNumOf_ErasedBlackPixelsTotf,
		&sColor_Image); //COLOR_IMAGE *sColor_Imagef); //[nImageSizeMax]

						/////////////////////////////////////////////////////////////////////////////////////////////
	if (nRes == UNSUCCESSFUL_RETURN)
	{
		delete[] sColor_Image.nRed_Arr;
		delete[] sColor_Image.nGreen_Arr;
		delete[] sColor_Image.nBlue_Arr;

		delete[] sColor_Image.nLenObjectBoundary_Arr;

		delete[] sColor_Image.nIsAPixelBackground_Arr;
		return UNSUCCESSFUL_RETURN;
	}// if (nRes == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n After 'FillingOut_Black...': nNumOf_ErasedBlackPixelsTot = %d", nNumOf_ErasedBlackPixelsTot);
	fprintf(fout_lr, "\n\n  After 'FillingOut_Black...': nNumOf_ErasedBlackPixelsTot = %d", nNumOf_ErasedBlackPixelsTot);

	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

///////////////////////////////////////////////////////////////////////////

/*
//median filter to the whole image
	for (iIterForMedianFilterf = 0; iIterForMedianFilterf < nNumOfItersForMedianFilter; iIterForMedianFilterf++)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'MedianFiterTo_AnArea_InAMaskImage: iIterForMedianFilterf = %d", iIterForMedianFilterf);
		fprintf(fout_lr, "\n\n 'MedianFiterTo_AnArea_InAMaskImage: iIterForMedianFilterf = %d", iIterForMedianFilterf); 
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		MedianFiterTo_AnArea_InAMaskImage(
			0, //const int nWidMinf,
			nImageHeight - 1, //const int nWidMaxf,//inclusive

			0, //const int nLenMinf,
			nImageWidth - 1, //const int nLenMaxf,////inclusive

			&sColor_Image); // COLOR_IMAGE *sColor_Imagef);

	}//for (iIterForMedianFilterf = 0; iIterForMedianFilterf < nNumOfItersForMedianFilter; iIterForMedianFilterf++)

*/
///////////////////////////////////////////////////////////////////////////
	  // Save to file
	for (j = 0; j < imageToSave.height(); j++)
	{
		for (i = 0; i < imageToSave.width(); i++)
		{
			iLen = i;
			iWid = j;

			nIndexOfPixelCur = iLen + iWid*nImageWidth;

			//if (sColor_Image.nIsAPixelBackground_Arr[nIndexOfPixelCur] == 3)
			if (sColor_Image.nIsAPixelBackground_Arr[nIndexOfPixelCur] == 0 || sColor_Image.nIsAPixelBackground_Arr[nIndexOfPixelCur] == 3)
			{
				imageToSave(j, i, R) = sColor_Image.nRed_Arr[nIndexOfPixelCur]; // nRed;
				imageToSave(j, i, G) = sColor_Image.nGreen_Arr[nIndexOfPixelCur]; //nGreen;
				imageToSave(j, i, B) = sColor_Image.nBlue_Arr[nIndexOfPixelCur]; // nBlue;
			} //if (sColor_Image.nIsAPixelBackground_Arr[nIndexOfPixelCur] == 0 || sColor_Image.nIsAPixelBackground_Arr[nIndexOfPixelCur] == 3)
			else
			{
				imageToSave(j, i, R) = image_in(j, i, R);
				imageToSave(j, i, G) = image_in(j, i, G);
				imageToSave(j, i, B) = image_in(j, i, B);

			}//else

			imageToSave(j, i, A) = image_in(j, i, A);
			//image_out(j, i, A) = image_in(j, i, A);

		} // for (i = 0; i < imageToSave.width(); i++)

	} // for (j = 0; j < imageToSave.height(); j++)

	  ////////////////////////////////////////////////////////////////////////////
	image_out = imageToSave;

	delete[] sColor_Image.nRed_Arr;
	delete[] sColor_Image.nGreen_Arr;
	delete[] sColor_Image.nBlue_Arr;

	delete[] sColor_Image.nLenObjectBoundary_Arr;
	delete[] sColor_Image.nIsAPixelBackground_Arr;

#ifndef COMMENT_OUT_ALL_PRINTS
	fflush(fout_lr); 
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	/////////////////////////////////////////////////
	return SUCCESSFUL_RETURN;
} //int doRemovingOfLabels_BlackWhiteImage(...
  ///////////////////////////////////////////////////////////////////////

int DeterminingSideOfObjectLocation_AndBackground_BlackWhite(
	COLOR_IMAGE *sColor_Imagef)

{
	int
		nIndexOfPixelCurf,
		nWidCurf = (int)((float)(sColor_Imagef->nWidth)*fShareOfWidthForTheSamePixelIntensities_1),
		iLenf,

		nNumOfWhitePixelsTotf = 0,
		nNumOfBlackPixelsTotf = 0,

		nRedCurf;
	
	float
		fPercentOfWhitePixels_ForSidef = 0.0;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'DeterminingSideOfObjectLocation_AndBackground_BlackWhite'\n");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
	for (iLenf = 0; iLenf < nLenForSideOfObjectLocation; iLenf++)
	{
		nIndexOfPixelCurf = iLenf + (nWidCurf*sColor_Imagef->nLength); // nLenMax);

		nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
		//nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
		//nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

		if (nRedCurf == nIntensityStatBlack)
		{
			nNumOfBlackPixelsTotf += 1;
			continue;
		} // 
		else if (nRedCurf == nIntensityStatWhite)
		{
			nNumOfWhitePixelsTotf += 1;
			continue;
		}//else if (nRedCurf == nIntensityStatWhite)

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n  An error in 'DeterminingSideOfObjectLocation_AndBackground_BlackWhite': nRedCurf = %d, no left or right object location", nRedCurf);
		fprintf(fout_lr, "\n\n An error in 'DeterminingSideOfObjectLocation_AndBackground_BlackWhite': nRedCurf = %d, no left or right object location", nRedCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	} // for (iLenf = 0; iLenf < nLenForSideOfObjectLocation; iLenf++)

	
	/////////////////////////////////////////////////////////////////
	fPercentOfWhitePixels_ForSidef = (float)(nNumOfWhitePixelsTotf) / (float)(nLenForSideOfObjectLocation);
	if (fPercentOfWhitePixels_ForSidef >= fPercentOfWhitePixels_ForSideMin)
	{
		sColor_Imagef->nSideOfObjectLocation = -1; // object to the left

		sColor_Imagef->nIntensityOfBackground_Red = nIntensityStatBlack;
		sColor_Imagef->nIntensityOfBackground_Green = nIntensityStatBlack;
		sColor_Imagef->nIntensityOfBackground_Blue = nIntensityStatBlack;

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n  'DeterminingSideOfObjectLocation_AndBackground_BlackWhite': the left side, fPercentOfWhitePixels_ForSidef = %E", fPercentOfWhitePixels_ForSidef);
		fprintf(fout_lr, "\n\n 'DeterminingSideOfObjectLocation_AndBackground_BlackWhite': the left side, fPercentOfWhitePixels_ForSidef = %E", fPercentOfWhitePixels_ForSidef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return SUCCESSFUL_RETURN;
	}//if (fPercentOfWhitePixels_ForSidef >= fPercentOfWhitePixels_ForSideMin)
	else if (fPercentOfWhitePixels_ForSidef < 1.0 -  fPercentOfWhitePixels_ForSideMin)
	{
		sColor_Imagef->nSideOfObjectLocation = 1; // object to the right

		sColor_Imagef->nIntensityOfBackground_Red = nIntensityStatBlack;
		sColor_Imagef->nIntensityOfBackground_Green = nIntensityStatBlack;
		sColor_Imagef->nIntensityOfBackground_Blue = nIntensityStatBlack;

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n  'DeterminingSideOfObjectLocation_AndBackground_BlackWhite': the right side, fPercentOfWhitePixels_ForSidef = %E", fPercentOfWhitePixels_ForSidef);
		fprintf(fout_lr, "\n\n 'DeterminingSideOfObjectLocation_AndBackground_BlackWhite': the right side, fPercentOfWhitePixels_ForSidef = %E", fPercentOfWhitePixels_ForSidef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		return SUCCESSFUL_RETURN;
	}//else

	return UNSUCCESSFUL_RETURN;
} //int DeterminingSideOfObjectLocation_AndBackground_BlackWhite(...
  ////////////////////////////////////////////////////////////////////////////////

int DeterminingLenOfObjectBoundary_BlackWhite(

	const int nDiffOfLensBetweenLast_2WhitePixelsMinf,

	const float fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf,

	const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,

	COLOR_IMAGE *sColor_Imagef)
{
	void Verifying_IfAPixel_HasObjectNeighborsAtCertainAngles(
		const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

		const int nDistFor_ObjectNeighborsAtCertainAnglesf,

		const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,

		const int nWidCurf,
		const int nLenCurf,

		int &nNumOfObjectNeighborsAtCertainAnglesf);

	int
		nResf,

		nNumOfBlackNeighboingPixelsFrTheSameRowf,

		nIndexOfPixelCurf,
	
		//nWidCurf = (int)(sColor_Imagef->nWidth*fShareOfWidthForTheSamePixelIntensities_1),
		iWidf,
		iLenf,
		nLenSidef,

		nRedCurf,
		nGreenCurf,
		nBlueCurf,

		nLenOfSmearedBoundaryf,

		nLenOfLastSuitableObject_LongAverf = -1,

		nForCharacteristicsOfNeighborsf,
		nRedPrevf = 0,
	

		nNumOfRightBoundariesf,
		nNumOfLeftBoundariesf,

		nLenOfObjectBoundaryCurf,

		nIsObjectPixelBelowf = 0, // no

		nNumOfAddedWidf = 0,

		nWidBelowf,
		nDiffOfLens_Maxf = nDistBetweenBoundariesOfNeighborWids_Max + nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min,

		nDistOf1stVerticalNeighbors_ToImageEdgeMinf = (int)(fCoefDist_ForVerticalNeighbors_ToImageEdgeMax*(float)(sColor_Imagef->nLength)),
		nNumOfNonZeroObjectBoundariesf = 0,
		nWidForfNonZeroObjectBoundariesf = nLarge,

		nDiffOfLensBetweenLast_2WhitePixelsf,
		nNumOfObjectNeighborsAtCertainAnglesf,

		nImageWidthf = sColor_Imagef->nWidth,
		nImageLengthf = sColor_Imagef->nLength,

		nDiffOfLensForBoundariesf,

		nLen_OfLastWhite_ObjectPixelf,

		nNumOfPixelConvertedFromWhiteToBlackTotf = 0,
		nNumOfPixelConvertedFromWhiteToBlackInARowf = 0,

		nLen_OfLastPixelConvertedFromWhiteToBlackf = -1,

		nDist_ForWhiteObjectPixels_ToImageEdgeMaxfMinf,
		nLengthOfSegment_WithBlackPixels_Curf;
	///////////////////////////////////////////////////////////////////////

	nDist_ForWhiteObjectPixels_ToImageEdgeMaxfMinf = (int)(fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf*(float)(nImageLengthf)); // 0.15*nImageLengthf

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n //////////////////////////////////////////////////////////////////////////////");
	fprintf(fout_lr, "\n\n 'DeterminingLenOfObjectBoundary_BlackWhite': sColor_Imagef->nSideOfObjectLocation = %d, sColor_Imagef->nIntensityOfBackground_Red = %d, ..._Green = %d, ....Blue = %d",
		sColor_Imagef->nSideOfObjectLocation, sColor_Imagef->nIntensityOfBackground_Red, sColor_Imagef->nIntensityOfBackground_Green, sColor_Imagef->nIntensityOfBackground_Blue);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//object to the left  
	if (sColor_Imagef->nSideOfObjectLocation == -1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n The object is to the left");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			nLen_OfLastPixelConvertedFromWhiteToBlackf = -1;

			nNumOfPixelConvertedFromWhiteToBlackInARowf = 0;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n  'DeterminingLenOfObjectBoundary_BlackWhite' (left): iWidf = %d", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			if (iWidf > 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] == -1)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error: the object boundary has not been found at iWidf - 1 = %d", iWidf - 1);
				printf("\n\n An error: the object boundary has not been found at iWidf - 1 = %d", iWidf - 1);

				//printf("\n\n Please adjust the settings and press any key to exit"); getchar();  exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			} //if (iWidf > 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] == -1)

//Initially, assuming that all pixels in the row are black
			sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = 0; 

			nLen_OfLastWhite_ObjectPixelf = 0;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n The length boundaries for a left object, iWidf = %d\n", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				//nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				//nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				if (nRedCurf == nIntensityStatMax)
				{
					if (nImageLengthf - iLenf < nDist_ForWhiteObjectPixels_ToImageEdgeMaxfMinf)
					{
						nNumOfPixelConvertedFromWhiteToBlackTotf += 1;
						nNumOfPixelConvertedFromWhiteToBlackInARowf += 1;

						sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;
						sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;
						sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;

						nLen_OfLastPixelConvertedFromWhiteToBlackf = iLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n A white pixel from the left object is too close to the opposite edge; converting the pixel to the black one: iWidf = %d, iLenf = %d\n",
							iWidf, iLenf);

						fprintf(fout_lr, "\n nLen_OfLastPixelConvertedFromWhiteToBlackf = %d, nNumOfPixelConvertedFromWhiteToBlackInARowf = %d, nNumOfPixelConvertedFromWhiteToBlackTotf = %d\n",
							nLen_OfLastPixelConvertedFromWhiteToBlackf, nNumOfPixelConvertedFromWhiteToBlackInARowf, nNumOfPixelConvertedFromWhiteToBlackTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						nRedPrevf = nRedCurf;
						continue;
					}//if (nImageLengthf - iLenf < nDist_ForWhiteObjectPixels_ToImageEdgeMaxfMinf)

					nLengthOfSegment_WithBlackPixels_Curf = 0;

					nDiffOfLensBetweenLast_2WhitePixelsf = iLenf - nLen_OfLastWhite_ObjectPixelf;
					if (nDiffOfLensBetweenLast_2WhitePixelsf > nDiffOfLensBetweenLast_2WhitePixelsMinf)
					{
						Verifying_IfAPixel_HasObjectNeighborsAtCertainAngles(
							sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

							nDiffOfLensBetweenLast_2WhitePixelsMinf, //const int nDistFor_ObjectNeighborsAtCertainAnglesf,

							fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf, //const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,

							iWidf, //const int nWidCurf,
							iLenf, //const int nLenCurf,

							nNumOfObjectNeighborsAtCertainAnglesf); // int &nNumOfObjectNeighborsAtCertainAnglesf);

						if (nNumOfObjectNeighborsAtCertainAnglesf == 0)
						{
							nNumOfPixelConvertedFromWhiteToBlackTotf += 1;
							nNumOfPixelConvertedFromWhiteToBlackInARowf += 1;

							sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;
							sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;
							sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;

							nLen_OfLastPixelConvertedFromWhiteToBlackf = iLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n Not a pixel from the left object; converting a white pixel to the black one: iWidf = %d, iLenf = %d\n",
								iWidf, iLenf);

							fprintf(fout_lr, "\n nLen_OfLastPixelConvertedFromWhiteToBlackf = %d, nNumOfPixelConvertedFromWhiteToBlackInARowf = %d, nNumOfPixelConvertedFromWhiteToBlackTotf = %d\n",
								nLen_OfLastPixelConvertedFromWhiteToBlackf, nNumOfPixelConvertedFromWhiteToBlackInARowf, nNumOfPixelConvertedFromWhiteToBlackTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							nRedPrevf = nRedCurf;
							continue;
						}//if (nNumOfObjectNeighborsAtCertainAnglesf == 0)
						else if (nNumOfObjectNeighborsAtCertainAnglesf >= 1)
						{
#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n A pixel from continuation of the left object, iWidf = %d, iLenf = %d\n", iWidf, iLenf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							nLen_OfLastWhite_ObjectPixelf = iLenf;
							nRedPrevf = nRedCurf;
							continue;
						}//else if (nNumOfObjectNeighborsAtCertainAnglesf >= 1)

					}// if (nDiffOfLensBetweenLast_2WhitePixelsf > nDiffOfLensBetweenLast_2WhitePixelsMinf)

					nLen_OfLastWhite_ObjectPixelf = iLenf;
					nRedPrevf = nRedCurf;
					continue;
				} //if (nRedCurf == nIntensityStatMax)

			} // for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)

			sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = nLen_OfLastWhite_ObjectPixelf;

#ifndef COMMENT_OUT_ALL_PRINTS			
			fprintf(fout_lr, "\n\n Final current sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//	MarkFinalBoundaryForLeftObject:	continue;
		} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)

	} // if (sColor_Imagef->nSideOfObjectLocation == -1)
	  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//object to the right
	if (sColor_Imagef->nSideOfObjectLocation == 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n The object is to the right");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			nLen_OfLastPixelConvertedFromWhiteToBlackf = -1;

			nNumOfPixelConvertedFromWhiteToBlackInARowf = 0;

			sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = nImageLengthf - 1; //initially

			nLen_OfLastWhite_ObjectPixelf = 0;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n  'DeterminingLenOfObjectBoundary_BlackWhite' (right): iWidf = %d", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			if (iWidf > 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] == -1)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error: the object boundary has not been found at iWidf - 1 = %d", iWidf - 1);
				printf("\n\n An error: the object boundary has not been found at iWidf - 1 = %d", iWidf - 1);

				//printf("\n\n Please adjust the settings and press any key to exit"); getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS				

				return UNSUCCESSFUL_RETURN;
			} //if (iWidf > 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] == -1)

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n The length boundaries for a right object, iWidf = %d\n", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				//nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				//nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				if (nRedCurf == nIntensityStatMax)
				{
					//if (nImageLengthf - iLenf < nDist_ForWhiteObjectPixels_ToImageEdgeMaxfMinf)
					if (iLenf < nDist_ForWhiteObjectPixels_ToImageEdgeMaxfMinf)
					{
						nNumOfPixelConvertedFromWhiteToBlackTotf += 1;
						nNumOfPixelConvertedFromWhiteToBlackInARowf += 1;

						sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;
						sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;
						sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;

						nLen_OfLastPixelConvertedFromWhiteToBlackf = iLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n A white pixel from the right object is too close to the opposite edge; converting the pixel to the black one: iWidf = %d, iLenf = %d\n",
							iWidf, iLenf);

						fprintf(fout_lr, "\n nLen_OfLastPixelConvertedFromWhiteToBlackf = %d, nNumOfPixelConvertedFromWhiteToBlackInARowf = %d, nNumOfPixelConvertedFromWhiteToBlackTotf = %d\n",
							nLen_OfLastPixelConvertedFromWhiteToBlackf, nNumOfPixelConvertedFromWhiteToBlackInARowf, nNumOfPixelConvertedFromWhiteToBlackTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						nRedPrevf = nRedCurf;
						continue;
					}//if (iLenf < nDist_ForWhiteObjectPixels_ToImageEdgeMaxfMinf)

					nLengthOfSegment_WithBlackPixels_Curf = 0;

					//nDiffOfLensBetweenLast_2WhitePixelsf = iLenf - nLen_OfLastWhite_ObjectPixelf;
					nDiffOfLensBetweenLast_2WhitePixelsf = nLen_OfLastWhite_ObjectPixelf - iLenf;

					if (nDiffOfLensBetweenLast_2WhitePixelsf > nDiffOfLensBetweenLast_2WhitePixelsMinf)
					{
						Verifying_IfAPixel_HasObjectNeighborsAtCertainAngles(
							sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

							nDiffOfLensBetweenLast_2WhitePixelsMinf, //const int nDistFor_ObjectNeighborsAtCertainAnglesf,

							fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf, //const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,

							iWidf, //const int nWidCurf,
							iLenf, //const int nLenCurf,

							nNumOfObjectNeighborsAtCertainAnglesf); // int &nNumOfObjectNeighborsAtCertainAnglesf);

						if (nNumOfObjectNeighborsAtCertainAnglesf == 0)
						{
							nNumOfPixelConvertedFromWhiteToBlackTotf += 1;
							nNumOfPixelConvertedFromWhiteToBlackInARowf += 1;

							sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;
							sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;
							sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;

							nLen_OfLastPixelConvertedFromWhiteToBlackf = iLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n Not a pixel from the right object; converting a white pixel to the black one: iWidf = %d, iLenf = %d\n",
								iWidf, iLenf);

							fprintf(fout_lr, "\n nLen_OfLastPixelConvertedFromWhiteToBlackf = %d, nNumOfPixelConvertedFromWhiteToBlackInARowf = %d, nNumOfPixelConvertedFromWhiteToBlackTotf = %d\n",
								nLen_OfLastPixelConvertedFromWhiteToBlackf, nNumOfPixelConvertedFromWhiteToBlackInARowf, nNumOfPixelConvertedFromWhiteToBlackTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							nRedPrevf = nRedCurf;
							continue;
						}//if (nNumOfObjectNeighborsAtCertainAnglesf == 0)
						else if (nNumOfObjectNeighborsAtCertainAnglesf >= 1)
						{
#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n A pixel from continuation of the right object, iWidf = %d, iLenf = %d\n", iWidf, iLenf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							nLen_OfLastWhite_ObjectPixelf = iLenf;
							nRedPrevf = nRedCurf;
							continue;
						}//else if (nNumOfObjectNeighborsAtCertainAnglesf >= 1)

					}// if (nDiffOfLensBetweenLast_2WhitePixelsf > nDiffOfLensBetweenLast_2WhitePixelsMinf)

					nLen_OfLastWhite_ObjectPixelf = iLenf;
					nRedPrevf = nRedCurf;
					continue;
				} //if (nRedCurf == nIntensityStatMax)
	
			} //for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)

		} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)

	} // if (sColor_Imagef->nSideOfObjectLocation == 1) //

	return SUCCESSFUL_RETURN;
} //int DeterminingLenOfObjectBoundary_BlackWhite(...
  ////////////////////////////////////////////////////////////////////////////////////

int Boundaries_And_ErasingWhiteRows(

	const int nDiffOfLensBetweenLast_2WhitePixelsMinf,
	const int nDistFor_ObjectNeighborsAtCertainAnglesf,
		const int nDistFor_ObjectNeighborsAtCertainAngles_CloseToBoundaryf,
	
		const int nLength_HalfRange_CloseToBoundaryf,

	const int nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf,

	const float fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf,

	const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,
	const float fRatioOfWhiteAndBlackPixelsInARowMaxf,

	const int nWidToCloseToEdgef,
	const int nLen_ToFindBlackPixelsf,

	COLOR_IMAGE *sColor_Imagef,

	int &nNumOfErasedRowsTotf,
	int &nNumOfPixelConvertedFromWhiteToBlackTotf)
{
	void BoundaryForASmearedRow_BlackWhite(
		const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]
		const int nDiffOfLensBetweenLast_2WhitePixelsMinf,

		const int nDistFor_ObjectNeighborsAtCertainAnglesf,
			//const int nDistFor_ObjectNeighborsAtCertainAngles_CloseToBoundaryf,

		const float fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf,

		const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,

		const int nWidCurf,

		const int nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf,

		float &fRatioOfWhiteAndBlackPixelsInARowf,

		int &nLenOfReasonableSmearedBoundaryf,
		int &nLenOfSmearedBoundaryf);

	void BoundaryForASmearedRow_WithRangeOfLength(
		const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

		const int nDiffOfLensBetweenLast_2WhitePixelsMinf,

		const int nDistFor_ObjectNeighborsAtCertainAnglesf, // == nDiffOfLensBetweenLast_2WhitePixelsMinf here
			//const int nDistFor_ObjectNeighborsAtCertainAngles_CloseToBoundaryf,

		const float fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf,

		const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,

		const int nWidCurf,
		const int nLenMinf,
		const int nLenMaxf,

		const int nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf,

		float &fRatioOfWhiteAndBlackPixelsInARowf,
		int &nLenOfReasonableSmearedBoundaryf,
		int &nLenOfSmearedBoundaryf);

	int
		nResf,

		nIndexOfPixelCurf,

		//nWidCurf = (int)(sColor_Imagef->nWidth*fShareOfWidthForTheSamePixelIntensities_1),
		iWidf,
		iLenf,
		nLenSidef,

		nLenMinf,
		nLenMaxf,

		nRedCurf,
		//nGreenCurf,
		//nBlueCurf,

		nLenOfReasonableSmearedBoundaryCurRowf,
		nLenOfReasonableSmearedBoundaryPrevRowf = -1,
		nLenOfSmearedBoundaryCurRowf,

		nDiffOfLensBetweenLast_2WhitePixelsf,
		nNumOfObjectNeighborsAtCertainAnglesf,

		nImageWidthf = sColor_Imagef->nWidth,
		nImageLengthf = sColor_Imagef->nLength,

		nWidthMinf,
		nWidthMaxf,

		nNumOfPixelConvertedFromWhiteToBlackInARowf = 0,

		nLengthOfSegment_WithBlackPixels_Curf;

	float
		fRatioOfWhiteAndBlackPixelsInARowf;

	///////////////////////////////////////////////////////////////////////
	nNumOfErasedRowsTotf = 0;
	nNumOfPixelConvertedFromWhiteToBlackTotf = 0;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n //////////////////////////////////////////////////////////////////////////////");
	fprintf(fout_lr, "\n\n 'Boundaries_And_ErasingWhiteRows': sColor_Imagef->nSideOfObjectLocation = %d, sColor_Imagef->nIntensityOfBackground_Red = %d, ..._Green = %d, ....Blue = %d",
		sColor_Imagef->nSideOfObjectLocation, sColor_Imagef->nIntensityOfBackground_Red, sColor_Imagef->nIntensityOfBackground_Green, sColor_Imagef->nIntensityOfBackground_Blue);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nWidToCloseToEdgef >= (int)(0.5*(float)(nImageWidthf)) )
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in 'Boundaries_And_ErasingWhiteRows': nImageWidthf = %d is too small relative to nWidToCloseToEdgef = %d", nImageWidthf,nWidToCloseToEdgef);
		printf("\n\n An error in 'Boundaries_And_ErasingWhiteRows': nImageWidthf = %d is too small relative to nWidToCloseToEdgef = %d", nImageWidthf, nWidToCloseToEdgef);

		//printf("\n\n Please adjust the settings and press any key to exit"); getchar();  exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return UNSUCCESSFUL_RETURN;
	} // if (nWidToCloseToEdgef >= (int)(0.5*(float)(nImageWidthf))
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//object to the left  
	if (sColor_Imagef->nSideOfObjectLocation == -1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n The object is to the left");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifndef COMMENT_OUT_ALL_PRINTS
		for (iWidf = nWidSubimageToPrintMin; iWidf < nWidSubimageToPrintMax; iWidf++)
		{
		fprintf(fout_lr, "\n\n 'Boundaries_And_ErasingWhiteRows' (printing): iWidf = %d\n", iWidf);

			for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				//nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				//nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				fprintf(fout_lr, " %d:%d", iLenf, nRedCurf);

			}//for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)

		} //for (iWidf = nWidSubimageToPrintMin; iWidf < nWidSubimageToPrintMax; iWidf++)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

//top left
		for (iWidf = nWidToCloseToEdgef; iWidf >= 0; iWidf--)
		{
			nNumOfPixelConvertedFromWhiteToBlackInARowf = 0;

			BoundaryForASmearedRow_BlackWhite(
				sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

				nDiffOfLensBetweenLast_2WhitePixelsMinf, //const int nDiffOfLensBetweenLast_2WhitePixelsMinf,
				nDistFor_ObjectNeighborsAtCertainAnglesf, //const int nDistFor_ObjectNeighborsAtCertainAnglesf,
					//nDistFor_ObjectNeighborsAtCertainAngles_CloseToBoundaryf, //const int nDistFor_ObjectNeighborsAtCertainAngles_CloseToBoundaryf,

				fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf, //const float fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf,

				fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf, //const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,

				iWidf, //const int nWidCurf,

				nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf, //const int nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf,

				fRatioOfWhiteAndBlackPixelsInARowf, //float &fRatioOfWhiteAndBlackPixelsInARowf,

				nLenOfReasonableSmearedBoundaryCurRowf, //int &nLenOfReasonableSmearedBoundaryf,
				nLenOfSmearedBoundaryCurRowf); // int &nLenOfSmearedBoundaryf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Boundaries_And_ErasingWhiteRows' (top left): nLenOfReasonableSmearedBoundaryCurRowf = %d, nLenOfSmearedBoundaryCurRowf = %d, iWidf = %d",
				nLenOfReasonableSmearedBoundaryCurRowf, nLenOfSmearedBoundaryCurRowf, iWidf);
	
			//printf("\n\n Please adjust the settings and press any key to exit"); getchar();  exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			if (nLenOfReasonableSmearedBoundaryCurRowf > nLenOfSmearedBoundaryCurRowf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'Boundaries_And_ErasingWhiteRows' (top left): nLenOfReasonableSmearedBoundaryCurRowf = %d > nLenOfSmearedBoundaryCurRowf = %d, iWidf = %d", 
					nLenOfReasonableSmearedBoundaryCurRowf,nLenOfSmearedBoundaryCurRowf, iWidf);
				printf("\n\n An error in 'Boundaries_And_ErasingWhiteRows' (top left): nLenOfReasonableSmearedBoundaryCurRowf = %d > nLenOfSmearedBoundaryCurRowf = %d, iWidf = %d",
					nLenOfReasonableSmearedBoundaryCurRowf, nLenOfSmearedBoundaryCurRowf, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			}//if (nLenOfReasonableSmearedBoundaryCurRowf > nLenOfSmearedBoundaryCurRowf)

			if (fRatioOfWhiteAndBlackPixelsInARowf > fRatioOfWhiteAndBlackPixelsInARowMaxf)
			{
//too many white pixels for a regular row

				if (iWidf == nWidToCloseToEdgef)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'Boundaries_And_ErasingWhiteRows' (top left): a white row is not supposed to be at iWidf = nWidToCloseToEdgef = %d",	iWidf);
					printf("\n\n An error in 'Boundaries_And_ErasingWhiteRows'(top left): a white row is not supposed to be at iWidf = nWidToCloseToEdgef = %d", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

					return UNSUCCESSFUL_RETURN;
				} // if (iWidf == nWidToCloseToEdgef)

				goto MarkErasingLeftTop;
			}//if (fRatioOfWhiteAndBlackPixelsInARowf > fRatioOfWhiteAndBlackPixelsInARowMaxf)
			else
			{
				nLenOfReasonableSmearedBoundaryPrevRowf = nLenOfReasonableSmearedBoundaryCurRowf;

				sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = nLenOfReasonableSmearedBoundaryCurRowf;

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n  (top left) sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				continue; //for (iWidf = nWidToCloseToEdgef; iWidf >= 0; iWidf--)
			}//else

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n  'Boundaries_And_ErasingWhiteRows' (top left): iWidf = %d", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

MarkErasingLeftTop:	nNumOfErasedRowsTotf += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Boundaries_And_ErasingWhiteRows'  (top left): erasing a row at iWidf = %d, fRatioOfWhiteAndBlackPixelsInARowf = %E > fRatioOfWhiteAndBlackPixelsInARowMaxf = %E", 
				iWidf, fRatioOfWhiteAndBlackPixelsInARowf, fRatioOfWhiteAndBlackPixelsInARowMaxf);

			fprintf(fout_lr, "\n nLenOfReasonableSmearedBoundaryPrevRowf  = %d, nNumOfErasedRowsTotf = %d", nLenOfReasonableSmearedBoundaryPrevRowf, nNumOfErasedRowsTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				if (iLenf <= nLenOfReasonableSmearedBoundaryPrevRowf)
				{
					sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nIntensityStatMax;
					sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nIntensityStatMax;
					sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nIntensityStatMax;
				} // if (iLenf <= nLenOfReasonableSmearedBoundaryPrevRowf)
				else
				{
					sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;
					sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;
					sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;

					nNumOfPixelConvertedFromWhiteToBlackInARowf += 1;
					nNumOfPixelConvertedFromWhiteToBlackTotf += 1;
				}//else
	
			} // for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)

		nLenOfReasonableSmearedBoundaryCurRowf = nLenOfReasonableSmearedBoundaryPrevRowf;

		sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = nLenOfReasonableSmearedBoundaryPrevRowf;
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n The end of iWidf = %d (top left),  nNumOfErasedRowsTotf = %d, nNumOfPixelConvertedFromWhiteToBlackInARowf = %d, nNumOfPixelConvertedFromWhiteToBlackTotf = %d",
				iWidf, nNumOfErasedRowsTotf,nNumOfPixelConvertedFromWhiteToBlackInARowf, nNumOfPixelConvertedFromWhiteToBlackTotf);
			fprintf(fout_lr, "\n sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		} //for (iWidf = nWidToCloseToEdgef; iWidf >= 0; iWidf--)
//the end of top
///////////////////////////////////////////////////////////////////////////////////
//bottom
		for (iWidf = nImageWidthf - nWidToCloseToEdgef; iWidf < nImageWidthf; iWidf++)
		{
			nNumOfPixelConvertedFromWhiteToBlackInARowf = 0;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n  'Boundaries_And_ErasingWhiteRows' (bottom left): iWidf = %d", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	
			BoundaryForASmearedRow_BlackWhite(
				sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

				nDiffOfLensBetweenLast_2WhitePixelsMinf, //const int nDiffOfLensBetweenLast_2WhitePixelsMinf,
				nDistFor_ObjectNeighborsAtCertainAnglesf, //const int nDistFor_ObjectNeighborsAtCertainAnglesf,
					//nDistFor_ObjectNeighborsAtCertainAngles_CloseToBoundaryf, //const int nDistFor_ObjectNeighborsAtCertainAngles_CloseToBoundaryf,

				fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf, //const float fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf,

				fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf, //const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,

				iWidf, //const int nWidCurf,

				nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf, //const int nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf,

				fRatioOfWhiteAndBlackPixelsInARowf, //float &fRatioOfWhiteAndBlackPixelsInARowf,

				nLenOfReasonableSmearedBoundaryCurRowf, //int &nLenOfReasonableSmearedBoundaryf,
				nLenOfSmearedBoundaryCurRowf); // int &nLenOfSmearedBoundaryf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Boundaries_And_ErasingWhiteRows' (bottom left): nLenOfReasonableSmearedBoundaryCurRowf = %d, nLenOfSmearedBoundaryCurRowf = %d, iWidf = %d",
				nLenOfReasonableSmearedBoundaryCurRowf, nLenOfSmearedBoundaryCurRowf, iWidf);

			//printf("\n\n Please adjust the settings and press any key to exit"); getchar();  exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			if (nLenOfReasonableSmearedBoundaryCurRowf > nLenOfSmearedBoundaryCurRowf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'Boundaries_And_ErasingWhiteRows' (bottom left): nLenOfReasonableSmearedBoundaryCurRowf = %d > nLenOfSmearedBoundaryCurRowf = %d, iWidf = %d",
					nLenOfReasonableSmearedBoundaryCurRowf, nLenOfSmearedBoundaryCurRowf, iWidf);
				printf("\n\n An error in 'Boundaries_And_ErasingWhiteRows' (bottom left): nLenOfReasonableSmearedBoundaryCurRowf = %d > nLenOfSmearedBoundaryCurRowf = %d, iWidf = %d",
					nLenOfReasonableSmearedBoundaryCurRowf, nLenOfSmearedBoundaryCurRowf, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			}//if (nLenOfReasonableSmearedBoundaryCurRowf > nLenOfSmearedBoundaryCurRowf)

			if (fRatioOfWhiteAndBlackPixelsInARowf > fRatioOfWhiteAndBlackPixelsInARowMaxf)
			{
				//too many white pixels for a regular row

				if (iWidf == nWidToCloseToEdgef)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'Boundaries_And_ErasingWhiteRows' (bottom left): a white row is not supposed to be at iWidf = nWidToCloseToEdgef = %d", iWidf);
					printf("\n\n An error in 'Boundaries_And_ErasingWhiteRows'(bottom left): a white row is not supposed to be at iWidf = nWidToCloseToEdgef = %d", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

					return UNSUCCESSFUL_RETURN;
				} // if (iWidf == nWidToCloseToEdgef)

				goto MarkErasingLeftBottom;
			}//if (fRatioOfWhiteAndBlackPixelsInARowf > fRatioOfWhiteAndBlackPixelsInARowMaxf)
			else
			{
				nLenOfReasonableSmearedBoundaryPrevRowf = nLenOfReasonableSmearedBoundaryCurRowf;

				sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = nLenOfReasonableSmearedBoundaryCurRowf;

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n  (bottom left) sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				continue; //for (iWidf = nWidToCloseToEdgef; iWidf >= 0; iWidf--)
			}//else

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n  'Boundaries_And_ErasingWhiteRows'(bottom left): iWidf = %d", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		MarkErasingLeftBottom:	nNumOfErasedRowsTotf += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Boundaries_And_ErasingWhiteRows' (bottom left): erasing a row at iWidf = %d, fRatioOfWhiteAndBlackPixelsInARowf = %E > fRatioOfWhiteAndBlackPixelsInARowMaxf = %E",
				iWidf, fRatioOfWhiteAndBlackPixelsInARowf, fRatioOfWhiteAndBlackPixelsInARowMaxf);

			fprintf(fout_lr, "\n nLenOfReasonableSmearedBoundaryPrevRowf  = %d, nNumOfErasedRowsTotf = %d", nLenOfReasonableSmearedBoundaryPrevRowf, nNumOfErasedRowsTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				if (iLenf <= nLenOfReasonableSmearedBoundaryPrevRowf)
				{
					sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nIntensityStatMax;
					sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nIntensityStatMax;
					sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nIntensityStatMax;
				} // if (iLenf <= nLenOfReasonableSmearedBoundaryPrevRowf)
				else
				{
					sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;
					sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;
					sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;

					nNumOfPixelConvertedFromWhiteToBlackInARowf += 1;
					nNumOfPixelConvertedFromWhiteToBlackTotf += 1;
				}//else

			} // for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)

			nLenOfReasonableSmearedBoundaryCurRowf = nLenOfReasonableSmearedBoundaryPrevRowf;
			sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = nLenOfReasonableSmearedBoundaryPrevRowf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n The end of iWidf = %d (bottom left),  nNumOfErasedRowsTotf = %d, nNumOfPixelConvertedFromWhiteToBlackInARowf = %d, nNumOfPixelConvertedFromWhiteToBlackTotf = %d",
				iWidf, nNumOfErasedRowsTotf, nNumOfPixelConvertedFromWhiteToBlackInARowf, nNumOfPixelConvertedFromWhiteToBlackTotf);

			fprintf(fout_lr, "\n sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		} //for (iWidf = nImageWidthf - nWidToCloseToEdgef; iWidf < nImageWidthf; iWidf++)
//the end of bottom

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n The end of iWidf loops (top and bottom, left object): nNumOfErasedRowsTotf = %d, nNumOfPixelConvertedFromWhiteToBlackTotf = %d", nNumOfErasedRowsTotf,nNumOfPixelConvertedFromWhiteToBlackTotf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//finding the remaining (centre) object boundaries (left object)
		nWidthMinf = nWidToCloseToEdgef;
		nWidthMaxf = nImageWidthf - nWidToCloseToEdgef;

		nLenOfReasonableSmearedBoundaryPrevRowf = sColor_Imagef->nLenObjectBoundary_Arr[nWidthMinf - 1];
		for (iWidf = nWidthMinf; iWidf < nWidthMaxf; iWidf++)
		{
			nNumOfPixelConvertedFromWhiteToBlackInARowf = 0;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n  'Boundaries_And_ErasingWhiteRows' (left centre): iWidf = %d", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			BoundaryForASmearedRow_BlackWhite(
				sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

				nDiffOfLensBetweenLast_2WhitePixelsMinf, //const int nDiffOfLensBetweenLast_2WhitePixelsMinf,
				nDistFor_ObjectNeighborsAtCertainAnglesf, //const int nDistFor_ObjectNeighborsAtCertainAnglesf,
					//nDistFor_ObjectNeighborsAtCertainAngles_CloseToBoundaryf, //const int nDistFor_ObjectNeighborsAtCertainAngles_CloseToBoundaryf,

				fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf, //const float fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf,

				fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf, //const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,

				iWidf, //const int nWidCurf,

				nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf, //const int nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf,

				fRatioOfWhiteAndBlackPixelsInARowf, //float &fRatioOfWhiteAndBlackPixelsInARowf,

				nLenOfReasonableSmearedBoundaryCurRowf, //int &nLenOfReasonableSmearedBoundaryf,
				nLenOfSmearedBoundaryCurRowf); // int &nLenOfSmearedBoundaryf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Boundaries_And_ErasingWhiteRows' (left centre): nLenOfReasonableSmearedBoundaryCurRowf = %d, nLenOfSmearedBoundaryCurRowf = %d, iWidf = %d",
				nLenOfReasonableSmearedBoundaryCurRowf, nLenOfSmearedBoundaryCurRowf, iWidf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			if (nLenOfReasonableSmearedBoundaryCurRowf > nLenOfSmearedBoundaryCurRowf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'Boundaries_And_ErasingWhiteRows' (left centre): nLenOfReasonableSmearedBoundaryCurRowf = %d > nLenOfSmearedBoundaryCurRowf = %d, iWidf = %d",
					nLenOfReasonableSmearedBoundaryCurRowf, nLenOfSmearedBoundaryCurRowf, iWidf);
				printf("\n\n An error in 'Boundaries_And_ErasingWhiteRows' (left centre): nLenOfReasonableSmearedBoundaryCurRowf = %d > nLenOfSmearedBoundaryCurRowf = %d, iWidf = %d",
					nLenOfReasonableSmearedBoundaryCurRowf, nLenOfSmearedBoundaryCurRowf, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			}//if (nLenOfReasonableSmearedBoundaryCurRowf > nLenOfSmearedBoundaryCurRowf)

			if (fRatioOfWhiteAndBlackPixelsInARowf > fRatioOfWhiteAndBlackPixelsInARowMaxf)
			{
	//too many white pixels for a regular row

				goto MarkErasingLeftCentre;
			}//if (fRatioOfWhiteAndBlackPixelsInARowf > fRatioOfWhiteAndBlackPixelsInARowMaxf)
			else
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				if (iWidf == nOneWidToPrint)
				{
					fprintf(fout_lr, "\n\n nLenOfReasonableSmearedBoundaryCurRowf = %d, sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] = %d, nLenOfReasonableSmearedBoundaryPrevRowf = %d",
						nLenOfReasonableSmearedBoundaryCurRowf, iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1],
						nLenOfReasonableSmearedBoundaryPrevRowf);
				} //if (iWidf == nOneWidToPrint)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				if (nLenOfReasonableSmearedBoundaryCurRowf == 0 && (sColor_Imagef->nLenObjectBoundary_Arr[iWidf] == 0 || sColor_Imagef->nLenObjectBoundary_Arr[iWidf] == -1) &&
					sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] <= nDistFor_ObjectNeighborsAtCertainAnglesf && 
						sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] > 0)
				{
					nLenMinf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] - nLength_HalfRange_CloseToBoundaryf;
					if (nLenMinf < 0)
						nLenMinf = 0;

					nLenMaxf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] + nLength_HalfRange_CloseToBoundaryf;
					if (nLenMaxf > nImageLengthf - 1)
						nLenMaxf = nImageLengthf - 1;

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n  Applying 'BoundaryForASmearedRow_WithRangeOfLength' (left centre): iWidf = %d, nLenMinf = %d, nLenMaxf = %d, nLenOfReasonableSmearedBoundaryPrevRowf = %d", 
						iWidf, nLenMinf, nLenMaxf, nLenOfReasonableSmearedBoundaryPrevRowf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					BoundaryForASmearedRow_WithRangeOfLength(
						sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

						nDiffOfLensBetweenLast_2WhitePixelsMinf, //const int nDiffOfLensBetweenLast_2WhitePixelsMinf,

						nDistFor_ObjectNeighborsAtCertainAngles_CloseToBoundaryf, //const int nDistFor_ObjectNeighborsAtCertainAnglesf, // == nDiffOfLensBetweenLast_2WhitePixelsMinf here
							//const int nDistFor_ObjectNeighborsAtCertainAngles_CloseToBoundaryf,

						fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf, //const float fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf,

						fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf, //const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,

						iWidf, //const int nWidCurf,
						nLenMinf, //const int nLenMinf,
						nLenMaxf, //const int nLenMaxf,

						nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf, //const int nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf,

						fRatioOfWhiteAndBlackPixelsInARowf, //float &fRatioOfWhiteAndBlackPixelsInARowf,

						nLenOfReasonableSmearedBoundaryCurRowf, //int &nLenOfReasonableSmearedBoundaryf,
						nLenOfSmearedBoundaryCurRowf); // int &nLenOfSmearedBoundaryf)

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n After 'BoundaryForASmearedRow_WithRangeOfLength' (left centre): nLenOfReasonableSmearedBoundaryCurRowf = %d, nLenOfSmearedBoundaryCurRowf = %d, iWidf = %d",
							nLenOfReasonableSmearedBoundaryCurRowf, nLenOfSmearedBoundaryCurRowf, iWidf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
				} // if (sColor_Imagef->nLenObjectBoundary_Arr[iWidf] == 0 && sColor_Imagef->nLenObjectBoundary_Arr[iWidf] <= nDistFor_ObjectNeighborsAtCertainAnglesf ...

				nLenOfReasonableSmearedBoundaryPrevRowf = nLenOfReasonableSmearedBoundaryCurRowf;

				sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = nLenOfReasonableSmearedBoundaryCurRowf;
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n  (left centre) sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				continue; //for (iWidf = nWidToCloseToEdgef; iWidf >= 0; iWidf--)
			}//else

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n  'Boundaries_And_ErasingWhiteRows'(left centre): iWidf = %d", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		MarkErasingLeftCentre:	nNumOfErasedRowsTotf += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Boundaries_And_ErasingWhiteRows' (left centre): erasing a row at iWidf = %d, fRatioOfWhiteAndBlackPixelsInARowf = %E > fRatioOfWhiteAndBlackPixelsInARowMaxf = %E",
				iWidf, fRatioOfWhiteAndBlackPixelsInARowf, fRatioOfWhiteAndBlackPixelsInARowMaxf);

			fprintf(fout_lr, "\n nLenOfReasonableSmearedBoundaryPrevRowf  = %d, nNumOfErasedRowsTotf = %d", nLenOfReasonableSmearedBoundaryPrevRowf, nNumOfErasedRowsTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				if (iLenf <= nLenOfReasonableSmearedBoundaryPrevRowf)
				{
					sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nIntensityStatMax;
					sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nIntensityStatMax;
					sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nIntensityStatMax;
				} // if (iLenf <= nLenOfReasonableSmearedBoundaryPrevRowf)
				else
				{
					sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;
					sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;
					sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;

					nNumOfPixelConvertedFromWhiteToBlackInARowf += 1;
					nNumOfPixelConvertedFromWhiteToBlackTotf += 1;
				}//else

			} // for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)

			nLenOfReasonableSmearedBoundaryCurRowf = nLenOfReasonableSmearedBoundaryPrevRowf;
			sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = nLenOfReasonableSmearedBoundaryPrevRowf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n The end of iWidf = %d (left centre),  nNumOfErasedRowsTotf = %d, nNumOfPixelConvertedFromWhiteToBlackInARowf = %d, nNumOfPixelConvertedFromWhiteToBlackTotf = %d",
				iWidf, nNumOfErasedRowsTotf, nNumOfPixelConvertedFromWhiteToBlackInARowf, nNumOfPixelConvertedFromWhiteToBlackTotf);

			fprintf(fout_lr, "\n sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		} //for (iWidf = nWidthMinf; iWidf < nWidthMaxf; iWidf++)

	} // if (sColor_Imagef->nSideOfObjectLocation == -1)
	  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//object to the right
	if (sColor_Imagef->nSideOfObjectLocation == 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n The object is to the right");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifndef COMMENT_OUT_ALL_PRINTS
		for (iWidf = nWidSubimageToPrintMin; iWidf < nWidSubimageToPrintMax; iWidf++)
		{
			fprintf(fout_lr, "\n\n 'Boundaries_And_ErasingWhiteRows' (printing): iWidf = %d\n", iWidf);

			for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				//nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				//nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				fprintf(fout_lr, " %d:%d", iLenf, nRedCurf);

			}//for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)

		} //for (iWidf = nWidSubimageToPrintMin; iWidf < nWidSubimageToPrintMax; iWidf++)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
//top right
		for (iWidf = nWidToCloseToEdgef; iWidf >= 0; iWidf--)
		{
			nNumOfPixelConvertedFromWhiteToBlackInARowf = 0;

			BoundaryForASmearedRow_BlackWhite(
				sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

				nDiffOfLensBetweenLast_2WhitePixelsMinf, //const int nDiffOfLensBetweenLast_2WhitePixelsMinf,
				nDistFor_ObjectNeighborsAtCertainAnglesf, //const int nDistFor_ObjectNeighborsAtCertainAnglesf,
					//nDistFor_ObjectNeighborsAtCertainAngles_CloseToBoundaryf, //const int nDistFor_ObjectNeighborsAtCertainAngles_CloseToBoundaryf,

				fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf, //const float fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf,

				fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf, //const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,

				iWidf, //const int nWidCurf,

				nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf, //const int nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf,

				fRatioOfWhiteAndBlackPixelsInARowf, //float &fRatioOfWhiteAndBlackPixelsInARowf,

				nLenOfReasonableSmearedBoundaryCurRowf, //int &nLenOfReasonableSmearedBoundaryf,
				nLenOfSmearedBoundaryCurRowf); // int &nLenOfSmearedBoundaryf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Boundaries_And_ErasingWhiteRows' (top right): nLenOfReasonableSmearedBoundaryCurRowf = %d, nLenOfSmearedBoundaryCurRowf = %d, iWidf = %d",
				nLenOfReasonableSmearedBoundaryCurRowf, nLenOfSmearedBoundaryCurRowf, iWidf);

			//printf("\n\n Please adjust the settings and press any key to exit"); getchar();  exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			if (nLenOfReasonableSmearedBoundaryCurRowf > nLenOfSmearedBoundaryCurRowf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'Boundaries_And_ErasingWhiteRows' (top right): nLenOfReasonableSmearedBoundaryCurRowf = %d > nLenOfSmearedBoundaryCurRowf = %d, iWidf = %d",
					nLenOfReasonableSmearedBoundaryCurRowf, nLenOfSmearedBoundaryCurRowf, iWidf);
				printf("\n\n An error in 'Boundaries_And_ErasingWhiteRows' (top right): nLenOfReasonableSmearedBoundaryCurRowf = %d > nLenOfSmearedBoundaryCurRowf = %d, iWidf = %d",
					nLenOfReasonableSmearedBoundaryCurRowf, nLenOfSmearedBoundaryCurRowf, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			}//if (nLenOfReasonableSmearedBoundaryCurRowf > nLenOfSmearedBoundaryCurRowf)

			if (fRatioOfWhiteAndBlackPixelsInARowf > fRatioOfWhiteAndBlackPixelsInARowMaxf)
			{
				//too many white pixels for a regular row

				if (iWidf == nWidToCloseToEdgef)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'Boundaries_And_ErasingWhiteRows' (top right): a white row is not supposed to be at iWidf = nWidToCloseToEdgef = %d", iWidf);
					printf("\n\n An error in 'Boundaries_And_ErasingWhiteRows'(top right): a white row is not supposed to be at iWidf = nWidToCloseToEdgef = %d", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

					return UNSUCCESSFUL_RETURN;
				} // if (iWidf == nWidToCloseToEdgef)

				goto MarkErasingRightTop;
			}//if (fRatioOfWhiteAndBlackPixelsInARowf > fRatioOfWhiteAndBlackPixelsInARowMaxf)
			else
			{
				nLenOfReasonableSmearedBoundaryPrevRowf = nLenOfReasonableSmearedBoundaryCurRowf;

				sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = nLenOfReasonableSmearedBoundaryCurRowf;

#ifndef COMMENT_OUT_ALL_PRINTS
				printf( "\n\n  (top right) sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
				fprintf(fout_lr, "\n\n  (top right) sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS


				continue; //for (iWidf = nWidToCloseToEdgef; iWidf >= 0; iWidf--)
			}//else

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n  'Boundaries_And_ErasingWhiteRows' (top right): iWidf = %d", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		MarkErasingRightTop:	nNumOfErasedRowsTotf += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Boundaries_And_ErasingWhiteRows'  (top right): erasing a row at iWidf = %d, fRatioOfWhiteAndBlackPixelsInARowf = %E > fRatioOfWhiteAndBlackPixelsInARowMaxf = %E",
				iWidf, fRatioOfWhiteAndBlackPixelsInARowf, fRatioOfWhiteAndBlackPixelsInARowMaxf);

			fprintf(fout_lr, "\n nLenOfReasonableSmearedBoundaryPrevRowf  = %d, nNumOfErasedRowsTotf = %d", nLenOfReasonableSmearedBoundaryPrevRowf, nNumOfErasedRowsTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				//if (iLenf <= nLenOfReasonableSmearedBoundaryPrevRowf)
				if (iLenf >= nLenOfReasonableSmearedBoundaryPrevRowf)
				{
					sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nIntensityStatMax;
					sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nIntensityStatMax;
					sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nIntensityStatMax;
				} // if (iLenf <= nLenOfReasonableSmearedBoundaryPrevRowf)
				else
				{
					sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;
					sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;
					sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;

					nNumOfPixelConvertedFromWhiteToBlackInARowf += 1;
					nNumOfPixelConvertedFromWhiteToBlackTotf += 1;
				}//else

			} // for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)

			nLenOfReasonableSmearedBoundaryCurRowf = nLenOfReasonableSmearedBoundaryPrevRowf;
			sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = nLenOfReasonableSmearedBoundaryPrevRowf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n The end of iWidf = %d (top right),  nNumOfErasedRowsTotf = %d, nNumOfPixelConvertedFromWhiteToBlackInARowf = %d, nNumOfPixelConvertedFromWhiteToBlackTotf = %d",
				iWidf, nNumOfErasedRowsTotf, nNumOfPixelConvertedFromWhiteToBlackInARowf, nNumOfPixelConvertedFromWhiteToBlackTotf);
			fprintf(fout_lr, "\n\n sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		} //for (iWidf = nWidToCloseToEdgef; iWidf >= 0; iWidf--)
//the end of right top
///////////////////////////////////////////////////////////////////////////////////
//bottom right
		for (iWidf = nImageWidthf - nWidToCloseToEdgef; iWidf < nImageWidthf; iWidf++)
		{
			nNumOfPixelConvertedFromWhiteToBlackInARowf = 0;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n  'Boundaries_And_ErasingWhiteRows' (bottom right): iWidf = %d", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			BoundaryForASmearedRow_BlackWhite(
				sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

				nDiffOfLensBetweenLast_2WhitePixelsMinf, //const int nDiffOfLensBetweenLast_2WhitePixelsMinf,
				nDistFor_ObjectNeighborsAtCertainAnglesf, //const int nDistFor_ObjectNeighborsAtCertainAnglesf,
					//nDistFor_ObjectNeighborsAtCertainAngles_CloseToBoundaryf, //const int nDistFor_ObjectNeighborsAtCertainAngles_CloseToBoundaryf,

				fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf, //const float fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf,

				fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf, //const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,

				iWidf, //const int nWidCurf,

				nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf, //const int nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf,

				fRatioOfWhiteAndBlackPixelsInARowf, //float &fRatioOfWhiteAndBlackPixelsInARowf,

				nLenOfReasonableSmearedBoundaryCurRowf, //int &nLenOfReasonableSmearedBoundaryf,
				nLenOfSmearedBoundaryCurRowf); // int &nLenOfSmearedBoundaryf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Boundaries_And_ErasingWhiteRows' (bottom right): nLenOfReasonableSmearedBoundaryCurRowf = %d, nLenOfSmearedBoundaryCurRowf = %d, iWidf = %d",
				nLenOfReasonableSmearedBoundaryCurRowf, nLenOfSmearedBoundaryCurRowf, iWidf);

			//printf("\n\n Please adjust the settings and press any key to exit"); getchar();  exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			if (nLenOfReasonableSmearedBoundaryCurRowf > nLenOfSmearedBoundaryCurRowf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'Boundaries_And_ErasingWhiteRows' (bottom right): nLenOfReasonableSmearedBoundaryCurRowf = %d > nLenOfSmearedBoundaryCurRowf = %d, iWidf = %d",
					nLenOfReasonableSmearedBoundaryCurRowf, nLenOfSmearedBoundaryCurRowf, iWidf);
				printf("\n\n An error in 'Boundaries_And_ErasingWhiteRows' (bottom right): nLenOfReasonableSmearedBoundaryCurRowf = %d > nLenOfSmearedBoundaryCurRowf = %d, iWidf = %d",
					nLenOfReasonableSmearedBoundaryCurRowf, nLenOfSmearedBoundaryCurRowf, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			}//if (nLenOfReasonableSmearedBoundaryCurRowf > nLenOfSmearedBoundaryCurRowf)

			if (fRatioOfWhiteAndBlackPixelsInARowf > fRatioOfWhiteAndBlackPixelsInARowMaxf)
			{
				//too many white pixels for a regular row

				if (iWidf == nWidToCloseToEdgef)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'Boundaries_And_ErasingWhiteRows' (bottom right): a white row is not supposed to be at iWidf = nWidToCloseToEdgef = %d", iWidf);
					printf("\n\n An error in 'Boundaries_And_ErasingWhiteRows'(bottom right): a white row is not supposed to be at iWidf = nWidToCloseToEdgef = %d", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

					return UNSUCCESSFUL_RETURN;
				} // if (iWidf == nWidToCloseToEdgef)

				goto MarkErasingRightBottom;
			}//if (fRatioOfWhiteAndBlackPixelsInARowf > fRatioOfWhiteAndBlackPixelsInARowMaxf)
			else
			{
				nLenOfReasonableSmearedBoundaryPrevRowf = nLenOfReasonableSmearedBoundaryCurRowf;

				sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = nLenOfReasonableSmearedBoundaryCurRowf;

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n  (bottom right) sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				continue; //for (iWidf = nWidToCloseToEdgef; iWidf >= 0; iWidf--)
			}//else

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n  'Boundaries_And_ErasingWhiteRows'(bottom right): iWidf = %d", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		MarkErasingRightBottom:	nNumOfErasedRowsTotf += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Boundaries_And_ErasingWhiteRows' (bottom right): erasing a row at iWidf = %d, fRatioOfWhiteAndBlackPixelsInARowf = %E > fRatioOfWhiteAndBlackPixelsInARowMaxf = %E",
				iWidf, fRatioOfWhiteAndBlackPixelsInARowf, fRatioOfWhiteAndBlackPixelsInARowMaxf);

			fprintf(fout_lr, "\n nLenOfReasonableSmearedBoundaryPrevRowf  = %d, nNumOfErasedRowsTotf = %d", nLenOfReasonableSmearedBoundaryPrevRowf, nNumOfErasedRowsTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				//if (iLenf <= nLenOfReasonableSmearedBoundaryPrevRowf)
				if (iLenf >= nLenOfReasonableSmearedBoundaryPrevRowf)
				{
					sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nIntensityStatMax;
					sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nIntensityStatMax;
					sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nIntensityStatMax;
				} // if (iLenf <= nLenOfReasonableSmearedBoundaryPrevRowf)
				else
				{
					sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;
					sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;
					sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;

					nNumOfPixelConvertedFromWhiteToBlackInARowf += 1;
					nNumOfPixelConvertedFromWhiteToBlackTotf += 1;
				}//else

			} // for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)

			nLenOfReasonableSmearedBoundaryCurRowf = nLenOfReasonableSmearedBoundaryPrevRowf;
			sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = nLenOfReasonableSmearedBoundaryPrevRowf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n The end of iWidf = %d (bottom right),  nNumOfErasedRowsTotf = %d, nNumOfPixelConvertedFromWhiteToBlackInARowf = %d, nNumOfPixelConvertedFromWhiteToBlackTotf = %d",
				iWidf, nNumOfErasedRowsTotf, nNumOfPixelConvertedFromWhiteToBlackInARowf, nNumOfPixelConvertedFromWhiteToBlackTotf);
			fprintf(fout_lr, "\n sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		} //for (iWidf = nImageWidthf - nWidToCloseToEdgef; iWidf < nImageWidthf; iWidf++)

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n The end of iWidf loops (top and bottom, right object): nNumOfErasedRowsTotf = %d, nNumOfPixelConvertedFromWhiteToBlackTotf = %d", nNumOfErasedRowsTotf, nNumOfPixelConvertedFromWhiteToBlackTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//finding the remaining (centre) object boundaries (right object)
		nWidthMinf = nWidToCloseToEdgef;
		nWidthMaxf = nImageWidthf - nWidToCloseToEdgef;

		nLenOfReasonableSmearedBoundaryPrevRowf = sColor_Imagef->nLenObjectBoundary_Arr[nWidthMinf - 1];
		for (iWidf = nWidthMinf; iWidf < nWidthMaxf; iWidf++)
		{
			nNumOfPixelConvertedFromWhiteToBlackInARowf = 0;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n  'Boundaries_And_ErasingWhiteRows' (right centre): iWidf = %d", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			BoundaryForASmearedRow_BlackWhite(
				sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

				nDiffOfLensBetweenLast_2WhitePixelsMinf, //const int nDiffOfLensBetweenLast_2WhitePixelsMinf,
				nDistFor_ObjectNeighborsAtCertainAnglesf, //const int nDistFor_ObjectNeighborsAtCertainAnglesf,
					//nDistFor_ObjectNeighborsAtCertainAngles_CloseToBoundaryf, //const int nDistFor_ObjectNeighborsAtCertainAngles_CloseToBoundaryf,

				fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf, //const float fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf,

				fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf, //const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,

				iWidf, //const int nWidCurf,

				nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf, //const int nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf,

				fRatioOfWhiteAndBlackPixelsInARowf, //float &fRatioOfWhiteAndBlackPixelsInARowf,

				nLenOfReasonableSmearedBoundaryCurRowf, //int &nLenOfReasonableSmearedBoundaryf,
				nLenOfSmearedBoundaryCurRowf); // int &nLenOfSmearedBoundaryf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Boundaries_And_ErasingWhiteRows' (right centre): nLenOfReasonableSmearedBoundaryCurRowf = %d, nLenOfSmearedBoundaryCurRowf = %d, iWidf = %d",
				nLenOfReasonableSmearedBoundaryCurRowf, nLenOfSmearedBoundaryCurRowf, iWidf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			if (nLenOfReasonableSmearedBoundaryCurRowf > nLenOfSmearedBoundaryCurRowf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'Boundaries_And_ErasingWhiteRows' (right centre): nLenOfReasonableSmearedBoundaryCurRowf = %d > nLenOfSmearedBoundaryCurRowf = %d, iWidf = %d",
					nLenOfReasonableSmearedBoundaryCurRowf, nLenOfSmearedBoundaryCurRowf, iWidf);
				printf("\n\n An error in 'Boundaries_And_ErasingWhiteRows' (right centre): nLenOfReasonableSmearedBoundaryCurRowf = %d > nLenOfSmearedBoundaryCurRowf = %d, iWidf = %d",
					nLenOfReasonableSmearedBoundaryCurRowf, nLenOfSmearedBoundaryCurRowf, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			}//if (nLenOfReasonableSmearedBoundaryCurRowf > nLenOfSmearedBoundaryCurRowf)

			if (fRatioOfWhiteAndBlackPixelsInARowf > fRatioOfWhiteAndBlackPixelsInARowMaxf)
			{
				//too many white pixels for a regular row

				goto MarkErasingRightCentre;
			}//if (fRatioOfWhiteAndBlackPixelsInARowf > fRatioOfWhiteAndBlackPixelsInARowMaxf)
			else
			{
				nLenOfReasonableSmearedBoundaryPrevRowf = nLenOfReasonableSmearedBoundaryCurRowf;

				sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = nLenOfReasonableSmearedBoundaryCurRowf;

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n (right centre) sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				continue; //for (iWidf = nWidToCloseToEdgef; iWidf >= 0; iWidf--)
			}//else

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n  'Boundaries_And_ErasingWhiteRows'(right centre): iWidf = %d", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		MarkErasingRightCentre:	nNumOfErasedRowsTotf += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Boundaries_And_ErasingWhiteRows' (right centre): erasing a row at iWidf = %d, fRatioOfWhiteAndBlackPixelsInARowf = %E > fRatioOfWhiteAndBlackPixelsInARowMaxf = %E",
				iWidf, fRatioOfWhiteAndBlackPixelsInARowf, fRatioOfWhiteAndBlackPixelsInARowMaxf);

			fprintf(fout_lr, "\n nLenOfReasonableSmearedBoundaryPrevRowf  = %d, nNumOfErasedRowsTotf = %d", nLenOfReasonableSmearedBoundaryPrevRowf, nNumOfErasedRowsTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				//if (iLenf <= nLenOfReasonableSmearedBoundaryPrevRowf)
				if (iLenf >= nLenOfReasonableSmearedBoundaryPrevRowf)
				{
					sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nIntensityStatMax;
					sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nIntensityStatMax;
					sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nIntensityStatMax;
				} // if (iLenf <= nLenOfReasonableSmearedBoundaryPrevRowf)
				else
				{
					sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;
					sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;
					sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nIntensityStatBlack;

					nNumOfPixelConvertedFromWhiteToBlackInARowf += 1;
					nNumOfPixelConvertedFromWhiteToBlackTotf += 1;
				}//else

			} // for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)

			nLenOfReasonableSmearedBoundaryCurRowf = nLenOfReasonableSmearedBoundaryPrevRowf;
			sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = nLenOfReasonableSmearedBoundaryPrevRowf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n The end of iWidf = %d (right centre),  nNumOfErasedRowsTotf = %d, nNumOfPixelConvertedFromWhiteToBlackInARowf = %d, nNumOfPixelConvertedFromWhiteToBlackTotf = %d",
				iWidf, nNumOfErasedRowsTotf, nNumOfPixelConvertedFromWhiteToBlackInARowf, nNumOfPixelConvertedFromWhiteToBlackTotf);
			fprintf(fout_lr, "\n sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		} //for (iWidf = nWidthMinf; iWidf < nWidthMaxf; iWidf++)

	} // if (sColor_Imagef->nSideOfObjectLocation == 1) //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

	return SUCCESSFUL_RETURN;
} //int Boundaries_And_ErasingWhiteRows(...


////////////////////////////////////////////////////////////////////////////////////////
int CharacteristicsOf_ObjectNeighborsInARow_BlackWhite(

	const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

	const int nWidCurf,
	const int nLenCurf,

		const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
	/////////////////////////////////////////////////////////////
	int &nNumOfBlackNeighboingPixelsFrTheSameRowf,
	int &nAverIntensityOverNeighboingPixelsFrTheSameRowf,
	int &nDiffBetweenMaxAndMinf)
{
	int
		nIndexOfPixelCurf,

		//iWidf,
		iLenf,

		nLenMinf,
		nLenMaxf,

		nRedCurf,
		nGreenCurf,
		nBlueCurf,

		nRedAverf = 0,
		nGreenAverf = 0,
		nBlueAverf = 0,

		nAverOfRedGreenBluef = 0,
		nAverOfRedGreenBlueMinf = nLarge,
		nAverOfRedGreenBlueMaxf = -nLarge,

		nLenOfPixelSegmentOfOneRowf;

	float
		fRatioOfLengthsf;

	if (nWidCurf < 0 || nWidCurf > sColor_Imagef->nWidth - 1 || nLenCurf <= 0 || nLenCurf >= sColor_Imagef->nLength - 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'CharacteristicsOf_ObjectNeighborsInARow_BlackWhite' 1: ...->nWidth - 1 = %d, nWidCurf = %d, ...->nLength - 1 = %d, nLenCurf = %d",
			sColor_Imagef->nWidth - 1, nWidCurf, sColor_Imagef->nLength - 1, nLenCurf);

		fprintf(fout_lr, "\n\n An error in 'CharacteristicsOf_ObjectNeighborsInARow_BlackWhite' 1: ...->nWidth - 1 = %d, nWidCurf = %d, ...->nLength - 1 = %d, nLenCurf = %d",
			sColor_Imagef->nWidth - 1, nWidCurf, sColor_Imagef->nLength - 1, nLenCurf);
		//printf("\n\n Please press any key to exit");	getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nWidCurf < 0 || nWidCurf > sColor_Imagef->nWidth - 1 || nLenCurf <= 0 || nLenCurf >= sColor_Imagef->nLength - 1)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n ////////////////////////////////////////////////////////////");
	fprintf(fout_lr, "\n 'CharacteristicsOf_ObjectNeighborsInARow_BlackWhite' 1: nWidCurf = %d, nLenCurf = %d, nLenOf_ObjectNeighboringPixelsFrTheSameRowf = %d, sColor_Imagef->nSideOfObjectLocation = %d",
							nWidCurf,nLenCurf, nLenOf_ObjectNeighboringPixelsFrTheSameRowf, sColor_Imagef->nSideOfObjectLocation);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	nNumOfBlackNeighboingPixelsFrTheSameRowf = 0;
	nAverIntensityOverNeighboingPixelsFrTheSameRowf = 0;
	nDiffBetweenMaxAndMinf = -nLarge;

	///////////////////////////////////////////////////////////////
	//object to the left   
	if (sColor_Imagef->nSideOfObjectLocation == -1)
	{
		//start counting from the left
		nLenMinf = nLenCurf - nLenOf_ObjectNeighboringPixelsFrTheSameRowf;
		if (nLenMinf < 0)
			nLenMinf = 0;

		nLenOfPixelSegmentOfOneRowf = nLenCurf - nLenMinf + 1;


#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_lr, "\n 'CharacteristicsOf_ObjectNeighborsInARow_BlackWhite' (left):  nLenMinf = %d, nLenCurf = %d, nLenOfPixelSegmentOfOneRowf = %d", nLenMinf,nLenCurf, nLenOfPixelSegmentOfOneRowf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		for (iLenf = nLenMinf; iLenf <= nLenCurf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (nWidCurf*sColor_Imagef->nLength); // nLenMax);

			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
			nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			nAverOfRedGreenBluef = (nRedCurf + nGreenCurf + nBlueCurf) / 3;

			if (nAverOfRedGreenBluef < nAverOfRedGreenBlueMinf)
			{
				nAverOfRedGreenBlueMinf = nAverOfRedGreenBluef;
			}// if (nAverOfRedGreenBluef < nAverOfRedGreenBlueMinf)

			if (nAverOfRedGreenBluef > nAverOfRedGreenBlueMaxf)
			{
				nAverOfRedGreenBlueMaxf = nAverOfRedGreenBluef;
			}// if (nAverOfRedGreenBluef > nAverOfRedGreenBlueMaxf)

			nRedAverf += nRedCurf;
			nGreenAverf += nGreenCurf;
			nBlueAverf += nBlueCurf;

			//if (nRedCurf == sColor_Imagef->nIntensityOfBackground_Red && nGreenCurf == sColor_Imagef->nIntensityOfBackground_Green && 
			//nBlueCurf == sColor_Imagef->nIntensityOfBackground_Blue)
			//if (nRedCurf < nIntensityCloseToBlackMax && nGreenCurf < nIntensityCloseToBlackMax &&
				//nBlueCurf < nIntensityCloseToBlackMax)
			if (nRedCurf <= nIntensityCloseToBlackMax && nGreenCurf <= nIntensityCloseToBlackMax &&
				nBlueCurf <= nIntensityCloseToBlackMax)
			{
				nNumOfBlackNeighboingPixelsFrTheSameRowf += 1;
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'CharacteristicsOf_ObjectNeighborsInARow_BlackWhite' (left): the next  nNumOfBlackNeighboingPixelsFrTheSameRowf = %d, nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d",
					nNumOfBlackNeighboingPixelsFrTheSameRowf, nRedCurf, nGreenCurf, nBlueCurf);

				fprintf(fout_lr, "\n iLenf = %d, nLenCurf = %d, nWidCurf = %d, nLenOfPixelSegmentOfOneRowf = %d", iLenf, nLenCurf, nWidCurf, nLenOfPixelSegmentOfOneRowf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			} // if (nRedCurf <= nIntensityCloseToBlackMax && nGreenCurf <= nIntensityCloseToBlackMax && ...

/*
			if (nWidCurf == 9 && nLenCurf == 1)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n iLenf = %d, nLenCurf = %d, nWidCurf = %d, nLenOfPixelSegmentOfOneRowf = %d", iLenf, nLenCurf, nWidCurf, nLenOfPixelSegmentOfOneRowf);
				fprintf(fout_lr, "\n nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d, nNumOfBlackNeighboingPixelsFrTheSameRowf = %d",
					nRedCurf, nGreenCurf, nBlueCurf, nNumOfBlackNeighboingPixelsFrTheSameRowf);

				fprintf(fout_lr, "\n nRedAverf = %d, nGreenAverf = %d, nBlueAverf = %d", nRedAverf, nGreenAverf, nBlueAverf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			}//if (nWidCurf == 9 && nLenCurf == 1)
*/

		} //for (iLenf = nLenMinf; iLenf <= nLenCurf; iLenf++)

		nRedAverf = nRedAverf / nLenOfPixelSegmentOfOneRowf;
		nGreenAverf = nGreenAverf / nLenOfPixelSegmentOfOneRowf;
		nBlueAverf = nBlueAverf / nLenOfPixelSegmentOfOneRowf;

		nAverIntensityOverNeighboingPixelsFrTheSameRowf = (nRedAverf + nGreenAverf + nBlueAverf) / 3;

		nDiffBetweenMaxAndMinf = nAverOfRedGreenBlueMaxf - nAverOfRedGreenBlueMinf;

	} // if (sColor_Imagef->nSideOfObjectLocation == -1)

	  ////////////////////////////////////////////////////////////////////////////////////////////
	  //object to the right
	if (sColor_Imagef->nSideOfObjectLocation == 1)
	{
		//still, start counting from the left
		nLenMaxf = nLenCurf + nLenOf_ObjectNeighboringPixelsFrTheSameRowf;

		if (nLenMaxf > sColor_Imagef->nLength - 1)
			nLenMaxf = sColor_Imagef->nLength - 1;

		nLenOfPixelSegmentOfOneRowf = nLenMaxf - nLenCurf + 1;

#ifndef COMMENT_OUT_ALL_PRINTS
	
		fprintf(fout_lr, "\n 'CharacteristicsOf_ObjectNeighborsInARow_BlackWhite' (right):  nLenCurf = %d, nLenMaxf = %d, nLenOfPixelSegmentOfOneRowf = %d", nLenCurf, nLenMaxf, nLenOfPixelSegmentOfOneRowf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		for (iLenf = nLenCurf; iLenf <= nLenMaxf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (nWidCurf*sColor_Imagef->nLength); // nLenMax);

			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
			nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			nAverOfRedGreenBluef = (nRedCurf + nGreenCurf + nBlueCurf) / 3;

			if (nAverOfRedGreenBluef < nAverOfRedGreenBlueMinf)
			{
				nAverOfRedGreenBlueMinf = nAverOfRedGreenBluef;
			}// if (nAverOfRedGreenBluef < nAverOfRedGreenBlueMinf)

			if (nAverOfRedGreenBluef > nAverOfRedGreenBlueMaxf)
			{
				nAverOfRedGreenBlueMaxf = nAverOfRedGreenBluef;
			}// if (nAverOfRedGreenBluef > nAverOfRedGreenBlueMaxf)

			nRedAverf += nRedCurf;
			nGreenAverf += nGreenCurf;
			nBlueAverf += nBlueCurf;

			//if (nRedCurf < nIntensityCloseToBlackMax && nGreenCurf < nIntensityCloseToBlackMax &&
				//nBlueCurf < nIntensityCloseToBlackMax)
			if (nRedCurf <= nIntensityCloseToBlackMax && nGreenCurf <= nIntensityCloseToBlackMax &&
				nBlueCurf <= nIntensityCloseToBlackMax)
			{
				nNumOfBlackNeighboingPixelsFrTheSameRowf += 1;
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'CharacteristicsOf_ObjectNeighborsInARow_BlackWhite' (right): the next  nNumOfBlackNeighboingPixelsFrTheSameRowf = %d, nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d",
					nNumOfBlackNeighboingPixelsFrTheSameRowf, nRedCurf, nGreenCurf, nBlueCurf);

				fprintf(fout_lr, "\n iLenf = %d, nLenCurf = %d, nWidCurf = %d, nLenOfPixelSegmentOfOneRowf = %d", iLenf, nLenCurf, nWidCurf, nLenOfPixelSegmentOfOneRowf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			} // if (nRedCurf <= nIntensityCloseToBlackMax && nGreenCurf <= nIntensityCloseToBlackMax && ...

		} //for (iLenf = nLenCurf; iLenf <= nLenMaxf; iLenf++)

		nRedAverf = nRedAverf / nLenOfPixelSegmentOfOneRowf;
		nGreenAverf = nGreenAverf / nLenOfPixelSegmentOfOneRowf;
		nBlueAverf = nBlueAverf / nLenOfPixelSegmentOfOneRowf;

		nAverIntensityOverNeighboingPixelsFrTheSameRowf = (nRedAverf + nGreenAverf + nBlueAverf) / 3;

		nDiffBetweenMaxAndMinf = nAverOfRedGreenBlueMaxf - nAverOfRedGreenBlueMinf;
	} // if (sColor_Imagef->nSideOfObjectLocation == 1)

	  //scaling the number of black pixels in case of closeness to the edge
	if (nLenOfPixelSegmentOfOneRowf < nLenOf_ObjectNeighboringPixelsFrTheSameRowf)
	{
		fRatioOfLengthsf = (float)(nLenOf_ObjectNeighboringPixelsFrTheSameRowf) / (float)(nLenOfPixelSegmentOfOneRowf);

		nNumOfBlackNeighboingPixelsFrTheSameRowf = (int)((float)(nNumOfBlackNeighboingPixelsFrTheSameRowf)*fRatioOfLengthsf);
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'CharacteristicsOf_ObjectNeighborsInARow_BlackWhite': the final nNumOfBlackNeighboingPixelsFrTheSameRowf = %d,  nLenCurf = %d, nWidCurf = %d",	nNumOfBlackNeighboingPixelsFrTheSameRowf, nLenCurf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	} //if (nLenOfPixelSegmentOfOneRowf < nLenOf_ObjectNeighboringPixelsFrTheSameRowf)

	return SUCCESSFUL_RETURN;
} //int CharacteristicsOf_ObjectNeighborsInARow_BlackWhite(...
  ////////////////////////////////////////////////////////////////////////////////////

//Using just Red for grayscale images
void Charac_AverOf_ObjectNeighborsInARow_BlackWhite(

	const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

	const int nWidCurf,
	const int nLenCurf,

	const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
	/////////////////////////////////////////////////////////////
	int &nAverIntensityOverNeighboingPixelsFrTheSameRowf)
{
	int
		nIndexOfPixelCurf,

		//iWidf,
		iLenf,

		nLenMinf,
		nLenMaxf,

		nRedCurf,
	//	nGreenCurf,
	//	nBlueCurf,

		nRedAverf = 0,
	//	nGreenAverf = 0,
	//	nBlueAverf = 0,

		nLenOfPixelSegmentOfOneRowf;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n ////////////////////////////////////////////////////////////");
	fprintf(fout_lr, "\n 'Charac_AverOf_ObjectNeighborsInARow' 1: nWidCurf = %d, nLenCurf = %d, nLenOf_ObjectNeighboringPixelsFrTheSameRowf = %d, sColor_Imagef->nSideOfObjectLocation = %d",
		nWidCurf, nLenCurf, nLenOf_ObjectNeighboringPixelsFrTheSameRowf, sColor_Imagef->nSideOfObjectLocation);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	nAverIntensityOverNeighboingPixelsFrTheSameRowf = 0;

	///////////////////////////////////////////////////////////////
	//object to the left   
	if (sColor_Imagef->nSideOfObjectLocation == -1)
	{
		//start counting from the left
		nLenMinf = nLenCurf - nLenOf_ObjectNeighboringPixelsFrTheSameRowf;
		if (nLenMinf < 0)
			nLenMinf = 0;

		nLenOfPixelSegmentOfOneRowf = nLenCurf - nLenMinf + 1;

#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_lr, "\n 'Charac_AverOf_ObjectNeighborsInARow' (left):  nLenMinf = %d, nLenCurf = %d, nLenOfPixelSegmentOfOneRowf = %d", nLenMinf, nLenCurf, nLenOfPixelSegmentOfOneRowf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		for (iLenf = nLenMinf; iLenf <= nLenCurf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (nWidCurf*sColor_Imagef->nLength); // nLenMax);

			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
	
			nRedAverf += nRedCurf;

/*
			if (nWidCurf == 9 && nLenCurf == 1)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n iLenf = %d, nLenCurf = %d, nWidCurf = %d, nLenOfPixelSegmentOfOneRowf = %d", iLenf, nLenCurf, nWidCurf, nLenOfPixelSegmentOfOneRowf);
				fprintf(fout_lr, "\n nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d, nNumOfBlackNeighboingPixelsFrTheSameRowf = %d",
					nRedCurf, nGreenCurf, nBlueCurf, nNumOfBlackNeighboingPixelsFrTheSameRowf);

				fprintf(fout_lr, "\n nRedAverf = %d, nGreenAverf = %d, nBlueAverf = %d", nRedAverf, nGreenAverf, nBlueAverf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			}//if (nWidCurf == 9 && nLenCurf == 1)
*/

		} //for (iLenf = nLenMinf; iLenf <= nLenCurf; iLenf++)

		nRedAverf = nRedAverf / nLenOfPixelSegmentOfOneRowf;

		//nAverIntensityOverNeighboingPixelsFrTheSameRowf = (nRedAverf + nGreenAverf + nBlueAverf) / 3;
		nAverIntensityOverNeighboingPixelsFrTheSameRowf = nRedAverf;

	} // if (sColor_Imagef->nSideOfObjectLocation == -1)

	  ////////////////////////////////////////////////////////////////////////////////////////////
	  //object to the right
	if (sColor_Imagef->nSideOfObjectLocation == 1)
	{
		//still, start counting from the left
		nLenMaxf = nLenCurf + nLenOf_ObjectNeighboringPixelsFrTheSameRowf;

		if (nLenMaxf > sColor_Imagef->nLength - 1)
			nLenMaxf = sColor_Imagef->nLength - 1;

		nLenOfPixelSegmentOfOneRowf = nLenMaxf - nLenCurf + 1;

#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_lr, "\n 'Charac_AverOf_ObjectNeighborsInARow' (right):  nLenCurf = %d, nLenMaxf = %d, nLenOfPixelSegmentOfOneRowf = %d", nLenCurf, nLenMaxf, nLenOfPixelSegmentOfOneRowf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		for (iLenf = nLenCurf; iLenf <= nLenMaxf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (nWidCurf*sColor_Imagef->nLength); // nLenMax);

			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
			nRedAverf += nRedCurf;
		} //for (iLenf = nLenCurf; iLenf <= nLenMaxf; iLenf++)

		nRedAverf = nRedAverf / nLenOfPixelSegmentOfOneRowf;

		//nAverIntensityOverNeighboingPixelsFrTheSameRowf = (nRedAverf + nGreenAverf + nBlueAverf) / 3;
		nAverIntensityOverNeighboingPixelsFrTheSameRowf = nRedAverf;

	} // if (sColor_Imagef->nSideOfObjectLocation == 1)

	//return SUCCESSFUL_RETURN;
} //void Charac_AverOf_ObjectNeighborsInARow_BlackWhite(...
/////////////////////////////////////////////////////////////////////////////////////////

//to be completed for the right object
//for grayscale images

void BoundaryForASmearedRow_BlackWhite(
	const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

	const int nDiffOfLensBetweenLast_2WhitePixelsMinf,

	const int nDistFor_ObjectNeighborsAtCertainAnglesf, // == nDiffOfLensBetweenLast_2WhitePixelsMinf here
		//const int nDistFor_ObjectNeighborsAtCertainAngles_CloseToBoundaryf,

	const float fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf,

	const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,

	const int nWidCurf,

	const int nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf,

	float &fRatioOfWhiteAndBlackPixelsInARowf,
	int &nLenOfReasonableSmearedBoundaryf,
	int &nLenOfSmearedBoundaryf)
{
	void Charac_AverOf_ObjectNeighborsInARow_BlackWhite(

		const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

		const int nWidCurf,
		const int nLenCurf,

		const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
		/////////////////////////////////////////////////////////////
		int &nAverIntensityOverNeighboingPixelsFrTheSameRowf);

	void Verifying_IfAPixel_HasObjectNeighborsAtCertainAngles(
		const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

		const int nDistFor_ObjectNeighborsAtCertainAnglesf,

		const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,

		const int nWidCurf,
		const int nLenCurf,

		int &nNumOfObjectNeighborsAtCertainAnglesf);

	int
		iLenf,
		nLenCurf,
		nIndexOfPixelCurf,

		nRedCurf,
		//nGreenCurf,
		//nBlueCurf,

		nRedPrevf = -1,
		nImageWidthf = sColor_Imagef->nWidth,
		nImageLengthf = sColor_Imagef->nLength,

		nLengthOfSegment_WithBlackPixels_Curf = 0,

		nDist_ForWhiteObjectPixels_ToImageEdgeMaxfMinf, // = (int)(fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf*(float)(nImageLengthf)); // 0.15*nImageLengthf

		nLenOfLastSuitableObjectPixelf = -1,

		nNumOfBlackPixelsInARowTotf = 0,
		nNumOfWhitePixelsInARowTotf = 0,

		nNumfOfChangesFromBlackToWhiteTotf = 0,

		nNumOfObjectNeighborsAtCertainAnglesf,
		nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf;

////////////////////////////////////////////////////////////////////////////
	nDist_ForWhiteObjectPixels_ToImageEdgeMaxfMinf = (int)(fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf*(float)(nImageLengthf)); // 0.15*nImageLengthf

	if (nWidCurf == nOneWidToPrint)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n 'BoundaryForASmearedRow_BlackWhite' (beginning):  nWidCurf == %d", nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	} //if (nWidCurf == nOneWidToPrint)

//object to the left
	if (sColor_Imagef->nSideOfObjectLocation == -1)
	{
		nLenOfSmearedBoundaryf = 0; // initailly
		nLenOfReasonableSmearedBoundaryf = 0;

		for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (nWidCurf*sColor_Imagef->nLength); // nLenMax);

			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
			//nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			//nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			if (nWidCurf == nOneWidToPrint)
			{
				if (nRedPrevf == nIntensityStatBlack && nRedCurf != nRedPrevf)
				{
					nNumfOfChangesFromBlackToWhiteTotf += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n A new nNumfOfChangesFromBlackToWhiteTotf = %d, nRedCurf = %d, iLenf = %d, nWidCurf == %d",
						nNumfOfChangesFromBlackToWhiteTotf, nRedCurf, iLenf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				} //if (nRedPrevf == nIntensityStatBlack && nRedCurf != nRedPrevf)

				if (nRedCurf > nIntensityStatBlack && nRedCurf < nIntensityStatMax)
				{

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n A warning for nRedCurf = %d, iLenf = %d, nWidCurf == %d", nRedCurf, iLenf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				}//if (nRedCurf > nIntensityStatBlack && nRedCurf < nIntensityStatMax)

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n nRedCurf = %d, iLenf = %d, nWidCurf == %d", nRedCurf, iLenf,nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			} //if (nWidCurf == nOneWidToPrint)

			//if (nRedCurf >= nIntensityCloseToBlackMax)
			if (nRedCurf > nIntensityCloseToBlackMax)
			{
				nLenOfLastSuitableObjectPixelf = iLenf;
				nLengthOfSegment_WithBlackPixels_Curf = 0;

				nNumOfWhitePixelsInARowTotf += 1;

				if (iLenf == sColor_Imagef->nLength - 1)
				{
// the white segment could be connected to the edge

					nLenCurf = iLenf;
					goto MarkCharac_Aver_Left;
				}//if (iLenf == sColor_Imagef->nLength - 1)
//skipping
				nRedPrevf = nRedCurf;
				continue;
			} // if (nRedCurf > nIntensityCloseToBlackMax)...

			nNumOfBlackPixelsInARowTotf += 1;
			nLengthOfSegment_WithBlackPixels_Curf += 1;

			if (nLengthOfSegment_WithBlackPixels_Curf == 1 && iLenf > 0)
			{
				//object
				nLenCurf = iLenf - 1;

MarkCharac_Aver_Left:	Charac_AverOf_ObjectNeighborsInARow_BlackWhite(

					sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

					nWidCurf, //const int nWidCurf,

//the prev white pixel is located one pixel to the left
					nLenCurf, //const int nLenCurf,

					nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf, //const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
					/////////////////////////////////////////////////////////////
					nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf); // int &nAverIntensityOverNeighboingPixelsFrTheSameRowf);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'BoundaryForASmearedRow_BlackWhite' 1:  nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf = %d, iLenf = %d, nWidCurf = %d",
					nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf, iLenf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				if (nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf < nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n Skipping iLenf = %d,  nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf = %d < nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin = %d",
						iLenf, nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf, nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					nRedPrevf = nRedCurf;
					continue;
				} //if (nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf < nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin)

				if (nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf >= nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin)
				{
					Verifying_IfAPixel_HasObjectNeighborsAtCertainAngles(
						sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

						nDistFor_ObjectNeighborsAtCertainAnglesf, //const int nDistFor_ObjectNeighborsAtCertainAnglesf,

						fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf, //const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,

						nWidCurf, //const int nWidCurf,
						nLenCurf, //const int nLenCurf,

						nNumOfObjectNeighborsAtCertainAnglesf); // int &nNumOfObjectNeighborsAtCertainAnglesf)

					if (nNumOfObjectNeighborsAtCertainAnglesf == 0)
					{
	#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n Not a pixel from the boundary of the left object; nWidCurf = %d, iLenf = %d, nLenCurf = %d\n",nWidCurf, iLenf, nLenCurf);
	#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						nRedPrevf = nRedCurf;
						continue; //for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
					}//if (nNumOfObjectNeighborsAtCertainAnglesf == 0)
					else if (nNumOfObjectNeighborsAtCertainAnglesf >= 1)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n A pixel from the boundary of the left object; nNumOfObjectNeighborsAtCertainAnglesf = %d, nWidCurf = %d, iLenf = %d, nLenCurf = %d\n", 
							nNumOfObjectNeighborsAtCertainAnglesf,nWidCurf, iLenf, nLenCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					}//else if (nNumOfObjectNeighborsAtCertainAnglesf >= 1)

//////////////////////////////////////////////
//to ensure that the boundary belongs to the breast (a white pixel)
					nLenOfSmearedBoundaryf = nLenCurf; // iLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n 'BoundaryForASmearedRow_BlackWhite' 1: a new nLenOfSmearedBoundaryf = %d, iLenf = %d,  nWidCurf = %d", nLenOfSmearedBoundaryf, iLenf, nWidCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					if (nImageLengthf - iLenf >= nDist_ForWhiteObjectPixels_ToImageEdgeMaxfMinf)
					{
//to ensure that the boundary belongs to the breast (a white pixel)
						nLenOfReasonableSmearedBoundaryf = nLenCurf; // iLenf;
							
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n 'BoundaryForASmearedRow_BlackWhite' 1: a new nLenOfReasonableSmearedBoundaryf = %d, iLenf = %d,  nWidCurf = %d", 
							nLenOfReasonableSmearedBoundaryf, iLenf, nWidCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						nRedPrevf = nRedCurf;
						continue;
					}//if (nImageLengthf - iLenf >= nDist_ForWhiteObjectPixels_ToImageEdgeMaxfMinf)
					else
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n 'BoundaryForASmearedRow_BlackWhite' 1: too close to the image edge at iLenf = %d,  nWidCurf = %d", iLenf, nWidCurf);
						fprintf(fout_lr, "\n  nLenOfReasonableSmearedBoundaryf = %d remains the same", nLenOfReasonableSmearedBoundaryf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					}//else

				} //if (nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf < nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin)
			
			} // if (nLengthOfSegment_WithBlackPixels_Curf == 1  && iLenf > 0))

			nRedPrevf = nRedCurf;
		} // for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)

		if (nNumOfBlackPixelsInARowTotf == 0)
		{
			nLenOfSmearedBoundaryf = sColor_Imagef->nLength - 1;
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'BoundaryForASmearedRow_BlackWhite' : a completely white row,  nLenOfSmearedBoundaryf = %d, nWidCurf = %d", 
				nLenOfSmearedBoundaryf, nWidCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		}//if (nNumOfBlackPixelsInARowTotf == 0)

		if (nNumOfWhitePixelsInARowTotf == 0)
		{
			nLenOfSmearedBoundaryf = 0;
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'BoundaryForASmearedRow_WhiteWhite' : a completely black row,  nLenOfSmearedBoundaryf = %d, nWidCurf = %d",
				nLenOfSmearedBoundaryf, nWidCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		}//if (nNumOfWhitePixelsInARowTotf == 0)

		if (nNumOfBlackPixelsInARowTotf == 0)
		{
			fRatioOfWhiteAndBlackPixelsInARowf = fLarge;
		}//if (nNumOfBlackPixelsInARowTotf == 0)
		else
		{
			fRatioOfWhiteAndBlackPixelsInARowf = (float)(nNumOfWhitePixelsInARowTotf) / (float)(nNumOfBlackPixelsInARowTotf);
		}//else

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n The end of 'BoundaryForASmearedRow_BlackWhite' left,  nNumOfBlackPixelsInARowTotf = %d,  nNumOfWhitePixelsInARowTotf = %d, nWidCurf = %d",
			nNumOfBlackPixelsInARowTotf, nNumOfWhitePixelsInARowTotf, nWidCurf);

		fprintf(fout_lr, "\n fRatioOfWhiteAndBlackPixelsInARowf = %E, nLenOfReasonableSmearedBoundaryf = %d,  nLenOfSmearedBoundaryf = %d",
			fRatioOfWhiteAndBlackPixelsInARowf, nLenOfReasonableSmearedBoundaryf, nLenOfSmearedBoundaryf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		if (nWidCurf == nOneWidToPrint)
		{

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n The total nNumfOfChangesFromBlackToWhiteTotf = %d, nWidCurf == %d",	nNumfOfChangesFromBlackToWhiteTotf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		} //if (nWidCurf == nOneWidToPrint)

	} // if (sColor_Imagef->nSideOfObjectLocation == -1) 
///////////////////////////////////////////////////////////////////////////////////////////////////////

//object to the right
	if (sColor_Imagef->nSideOfObjectLocation == 1)
	{
		nLenOfSmearedBoundaryf = sColor_Imagef->nLength - 1; // initailly
		nLenOfReasonableSmearedBoundaryf = sColor_Imagef->nLength - 1; // initailly

		for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)
		{
			nIndexOfPixelCurf = iLenf + (nWidCurf*sColor_Imagef->nLength); // nLenMax);

			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
			//nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			//nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			if (nWidCurf == nOneWidToPrint)
			{
				if (nRedPrevf == nIntensityStatBlack && nRedCurf != nRedPrevf)
				{
					nNumfOfChangesFromBlackToWhiteTotf += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n A new nNumfOfChangesFromBlackToWhiteTotf = %d, nRedCurf = %d, iLenf = %d, nWidCurf == %d",
						nNumfOfChangesFromBlackToWhiteTotf, nRedCurf, iLenf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				} //if (nRedPrevf == nIntensityStatBlack && nRedCurf != nRedPrevf)

				if (nRedCurf > nIntensityStatBlack && nRedCurf < nIntensityStatMax)
				{

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n A warning for nRedCurf = %d, iLenf = %d, nWidCurf == %d", nRedCurf, iLenf, nWidCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				}//if (nRedCurf > nIntensityStatBlack && nRedCurf < nIntensityStatMax)

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n 'BoundaryForASmearedRow_BlackWhite' (right): nRedCurf = %d, iLenf = %d, nWidCurf == %d", nRedCurf, iLenf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			} //if (nWidCurf == nOneWidToPrint)
/////////////////////////////

			//if (nRedCurf >= nIntensityCloseToBlackMax)
			if (nRedCurf > nIntensityCloseToBlackMax)
			{
				nLenOfLastSuitableObjectPixelf = iLenf;

				nLengthOfSegment_WithBlackPixels_Curf = 0;

				nNumOfWhitePixelsInARowTotf += 1;

				if (iLenf == 0)
				{
			// the white segment could be connected to the edge

					nLenCurf = iLenf;
					goto MarkCharac_Aver_Right;
				}//if (iLenf == 0)

				nRedPrevf = nRedCurf;
				continue;
			} // if (nRedCurf > nIntensityCloseToBlackMax)...

			nNumOfBlackPixelsInARowTotf += 1;
			nLengthOfSegment_WithBlackPixels_Curf += 1;

			if (nLengthOfSegment_WithBlackPixels_Curf == 1 && iLenf < nImageLengthf - 1)
			{
				//object
				nLenCurf = iLenf + 1;

MarkCharac_Aver_Right:	Charac_AverOf_ObjectNeighborsInARow_BlackWhite(

					sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

					nWidCurf, //const int nWidCurf,

//the prev white pixel is located one pixel to the right
					nLenCurf, //const int nLenCurf,

					nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf, //const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
					/////////////////////////////////////////////////////////////
					nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf); // int &nAverIntensityOverNeighboingPixelsFrTheSameRowf);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'BoundaryForASmearedRow_BlackWhite' 2:  nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf = %d, iLenf = %d, nWidCurf = %d",
					nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf, iLenf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				if (nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf < nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n Skipping iLenf = %d,  nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf = %d < nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin = %d",
						iLenf, nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf, nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					nRedPrevf = nRedCurf;
					continue;
				} //if (nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf < nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin)

				if (nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf >= nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin)
				{
					Verifying_IfAPixel_HasObjectNeighborsAtCertainAngles(
						sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

						nDistFor_ObjectNeighborsAtCertainAnglesf, //const int nDistFor_ObjectNeighborsAtCertainAnglesf,

						fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf, //const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,

						nWidCurf, //const int nWidCurf,
						nLenCurf, //const int nLenCurf,

						nNumOfObjectNeighborsAtCertainAnglesf); // int &nNumOfObjectNeighborsAtCertainAnglesf)

					if (nNumOfObjectNeighborsAtCertainAnglesf == 0)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n Not a pixel from the boundary of the right object; nWidCurf = %d, iLenf = %d, nLenCurf = %d\n", nWidCurf, iLenf, nLenCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						nRedPrevf = nRedCurf;
						continue; //for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
					}//if (nNumOfObjectNeighborsAtCertainAnglesf == 0)
					else if (nNumOfObjectNeighborsAtCertainAnglesf >= 1)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n A pixel from the boundary of the right object; nNumOfObjectNeighborsAtCertainAnglesf = %d, nWidCurf = %d, iLenf = %d, nLenCurf = %d\n",
							nNumOfObjectNeighborsAtCertainAnglesf, nWidCurf, iLenf, nLenCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					}//else if (nNumOfObjectNeighborsAtCertainAnglesf >= 1)
/////////////////////////////////////////////////////////////////////////////
//to ensure that the boundary belongs to the breast (a white pixel)
					nLenOfSmearedBoundaryf = nLenCurf; // iLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n 'BoundaryForASmearedRow_BlackWhite' (right): a new nLenOfSmearedBoundaryf = %d, iLenf = %d,  nWidCurf = %d", nLenOfSmearedBoundaryf, iLenf, nWidCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					if (iLenf >= nDist_ForWhiteObjectPixels_ToImageEdgeMaxfMinf)
					{
//to ensure that the boundary belongs to the breast (a white pixel)
						nLenOfReasonableSmearedBoundaryf = nLenCurf; // iLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n 'BoundaryForASmearedRow_BlackWhite' (right): a new nLenOfReasonableSmearedBoundaryf = %d, iLenf = %d,  nWidCurf = %d",
							nLenOfReasonableSmearedBoundaryf, iLenf, nWidCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						nRedPrevf = nRedCurf;
						continue;
					}//if (iLenf >= nDist_ForWhiteObjectPixels_ToImageEdgeMaxfMinf)
					else
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n 'BoundaryForASmearedRow_BlackWhite' (right): too close to the image edge at iLenf = %d,  nWidCurf = %d", iLenf, nWidCurf);
						fprintf(fout_lr, "\n  nLenOfReasonableSmearedBoundaryf = %d remains the same", nLenOfReasonableSmearedBoundaryf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					}//else

				} //if (nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf < nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin)

			} // if (nLengthOfSegment_WithBlackPixels_Curf == 1 && iLenf < nImageLengthf - 1)

			nRedPrevf = nRedCurf;
		} //for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)

		if (nNumOfBlackPixelsInARowTotf == 0)
		{
			nLenOfSmearedBoundaryf = 0; // sColor_Imagef->nLength - 1;
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'BoundaryForASmearedRow_BlackWhite' (right): a completely white row,  nLenOfSmearedBoundaryf = %d, nWidCurf = %d",
				nLenOfSmearedBoundaryf, nWidCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		}//if (nNumOfBlackPixelsInARowTotf == 0)

		if (nNumOfWhitePixelsInARowTotf == 0)
		{
			nLenOfSmearedBoundaryf = sColor_Imagef->nLength - 1; //0;
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'BoundaryForASmearedRow_WhiteWhite' (right): a completely black row,  nLenOfSmearedBoundaryf = %d, nWidCurf = %d",
				nLenOfSmearedBoundaryf, nWidCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		}//if (nNumOfWhitePixelsInARowTotf == 0)

		if (nNumOfBlackPixelsInARowTotf == 0)
		{
			fRatioOfWhiteAndBlackPixelsInARowf = fLarge;
		}//if (nNumOfBlackPixelsInARowTotf == 0)
		else
		{
			fRatioOfWhiteAndBlackPixelsInARowf = (float)(nNumOfWhitePixelsInARowTotf) / (float)(nNumOfBlackPixelsInARowTotf);
		}//else

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n The end of 'BoundaryForASmearedRow_BlackWhite' (right),  nNumOfBlackPixelsInARowTotf = %d,  nNumOfWhitePixelsInARowTotf = %d, nWidCurf = %d",
			nNumOfBlackPixelsInARowTotf, nNumOfWhitePixelsInARowTotf, nWidCurf);

		fprintf(fout_lr, "\n fRatioOfWhiteAndBlackPixelsInARowf = %E, nLenOfReasonableSmearedBoundaryf = %d,  nLenOfSmearedBoundaryf = %d",
			fRatioOfWhiteAndBlackPixelsInARowf, nLenOfReasonableSmearedBoundaryf, nLenOfSmearedBoundaryf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	} // if (sColor_Imagef->nSideOfObjectLocation == 1)

}//void BoundaryForASmearedRow_BlackWhite(...
/////////////////////////////////////////////////////////////////////////////////////////


void BoundaryForASmearedRow_WithRangeOfLength(
	const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

	const int nDiffOfLensBetweenLast_2WhitePixelsMinf,

	const int nDistFor_ObjectNeighborsAtCertainAnglesf, // == nDiffOfLensBetweenLast_2WhitePixelsMinf here
		//const int nDistFor_ObjectNeighborsAtCertainAngles_CloseToBoundaryf,

	const float fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf,

	const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,

	const int nWidCurf,
	const int nLenMinf,
	const int nLenMaxf,

	const int nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf,

	float &fRatioOfWhiteAndBlackPixelsInARowf,
	int &nLenOfReasonableSmearedBoundaryf,
	int &nLenOfSmearedBoundaryf)
{
	void Charac_AverOf_ObjectNeighborsInARow_BlackWhite(

		const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

		const int nWidCurf,
		const int nLenCurf,

		const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
		/////////////////////////////////////////////////////////////
		int &nAverIntensityOverNeighboingPixelsFrTheSameRowf);

	void Verifying_IfAPixel_HasObjectNeighborsAtCertainAngles(
		const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

		const int nDistFor_ObjectNeighborsAtCertainAnglesf,

		const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,

		const int nWidCurf,
		const int nLenCurf,

		int &nNumOfObjectNeighborsAtCertainAnglesf);

	int
		iLenf,
		nLenCurf,
		nIndexOfPixelCurf,

		nRedCurf,
		//nGreenCurf,
		//nBlueCurf,

		nRedPrevf = -1,
		nImageWidthf = sColor_Imagef->nWidth,
		nImageLengthf = sColor_Imagef->nLength,

		nLengthOfSegment_WithBlackPixels_Curf = 0,

		nDist_ForWhiteObjectPixels_ToImageEdgeMaxfMinf, // = (int)(fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf*(float)(nImageLengthf)); // 0.15*nImageLengthf

		nLenOfLastSuitableObjectPixelf = -1,

		nNumOfBlackPixelsInARowTotf = 0,
		nNumOfWhitePixelsInARowTotf = 0,

		nNumfOfChangesFromBlackToWhiteTotf = 0,

		nNumOfObjectNeighborsAtCertainAnglesf,
		nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf;

	////////////////////////////////////////////////////////////////////////////
	nDist_ForWhiteObjectPixels_ToImageEdgeMaxfMinf = (int)(fCoefDist_ForWhiteObjectPixels_ToImageEdgeMaxf*(float)(nImageLengthf)); // 0.15*nImageLengthf

//object to the left
	if (sColor_Imagef->nSideOfObjectLocation == -1)
	{
		nLenOfSmearedBoundaryf = 0; // initailly
		nLenOfReasonableSmearedBoundaryf = 0;

		for (iLenf = nLenMinf; iLenf < nLenMaxf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (nWidCurf*sColor_Imagef->nLength); // nLenMax);

			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
			//nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			//nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			if (nWidCurf == nOneWidToPrint)
			{
				if (nRedPrevf == nIntensityStatBlack && nRedCurf != nRedPrevf)
				{
					nNumfOfChangesFromBlackToWhiteTotf += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n A new nNumfOfChangesFromBlackToWhiteTotf = %d, nRedCurf = %d, iLenf = %d, nWidCurf == %d",
						nNumfOfChangesFromBlackToWhiteTotf, nRedCurf, iLenf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				} //if (nRedPrevf == nIntensityStatBlack && nRedCurf != nRedPrevf)

				if (nRedCurf > nIntensityStatBlack && nRedCurf < nIntensityStatMax)
				{

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n A warning for nRedCurf = %d, iLenf = %d, nWidCurf == %d", nRedCurf, iLenf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				}//if (nRedCurf > nIntensityStatBlack && nRedCurf < nIntensityStatMax)

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n nRedCurf = %d, iLenf = %d, nWidCurf == %d", nRedCurf, iLenf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			} //if (nWidCurf == nOneWidToPrint)

			//if (nRedCurf >= nIntensityCloseToBlackMax)
			if (nRedCurf > nIntensityCloseToBlackMax)
			{
				nLenOfLastSuitableObjectPixelf = iLenf;
				nLengthOfSegment_WithBlackPixels_Curf = 0;

				nNumOfWhitePixelsInARowTotf += 1;

				if (iLenf == sColor_Imagef->nLength - 1)
				{
					// the white segment could be connected to the edge

					nLenCurf = iLenf;
					goto MarkCharac_Aver_Left;
				}//if (iLenf == sColor_Imagef->nLength - 1)
//skipping
				nRedPrevf = nRedCurf;
				continue;
			} // if (nRedCurf > nIntensityCloseToBlackMax)...

			nNumOfBlackPixelsInARowTotf += 1;
			nLengthOfSegment_WithBlackPixels_Curf += 1;

			if (nLengthOfSegment_WithBlackPixels_Curf == 1 && iLenf > 0)
			{
				//object
				nLenCurf = iLenf - 1;

			MarkCharac_Aver_Left:	Charac_AverOf_ObjectNeighborsInARow_BlackWhite(

				sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

				nWidCurf, //const int nWidCurf,

//the prev white pixel is located one pixel to the left
				nLenCurf, //const int nLenCurf,

				nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf, //const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
				/////////////////////////////////////////////////////////////
				nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf); // int &nAverIntensityOverNeighboingPixelsFrTheSameRowf);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'BoundaryForASmearedRow_WithRangeOfLength' 1:  nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf = %d, iLenf = %d, nWidCurf = %d",
					nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf, iLenf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					if (nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf < nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n Skipping iLenf = %d,  nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf = %d < nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin = %d",
							iLenf, nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf, nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						nRedPrevf = nRedCurf;
						continue;
					} //if (nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf < nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin)

					if (nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf >= nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin)
					{
						Verifying_IfAPixel_HasObjectNeighborsAtCertainAngles(
							sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

							nDistFor_ObjectNeighborsAtCertainAnglesf, //const int nDistFor_ObjectNeighborsAtCertainAnglesf,

							fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf, //const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,

							nWidCurf, //const int nWidCurf,
							nLenCurf, //const int nLenCurf,

							nNumOfObjectNeighborsAtCertainAnglesf); // int &nNumOfObjectNeighborsAtCertainAnglesf)

						if (nNumOfObjectNeighborsAtCertainAnglesf == 0)
						{
#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n Not a pixel from the boundary of the left object; nWidCurf = %d, iLenf = %d, nLenCurf = %d\n", nWidCurf, iLenf, nLenCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							nRedPrevf = nRedCurf;
							continue; //for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
						}//if (nNumOfObjectNeighborsAtCertainAnglesf == 0)
						else if (nNumOfObjectNeighborsAtCertainAnglesf >= 1)
						{
#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n A pixel from the boundary of the left object; nNumOfObjectNeighborsAtCertainAnglesf = %d, nWidCurf = %d, iLenf = %d, nLenCurf = %d\n",
								nNumOfObjectNeighborsAtCertainAnglesf, nWidCurf, iLenf, nLenCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						}//else if (nNumOfObjectNeighborsAtCertainAnglesf >= 1)

//////////////////////////////////////////////
					nLenOfSmearedBoundaryf = iLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n 'BoundaryForASmearedRow_WithRangeOfLength' 1: a new nLenOfSmearedBoundaryf = %d, iLenf = %d,  nWidCurf = %d", nLenOfSmearedBoundaryf, iLenf, nWidCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					if (nImageLengthf - iLenf >= nDist_ForWhiteObjectPixels_ToImageEdgeMaxfMinf)
					{
						nLenOfReasonableSmearedBoundaryf = iLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n 'BoundaryForASmearedRow_WithRangeOfLength' 1: a new nLenOfReasonableSmearedBoundaryf = %d, iLenf = %d,  nWidCurf = %d",
							nLenOfReasonableSmearedBoundaryf, iLenf, nWidCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						nRedPrevf = nRedCurf;
						continue;
					}//if (nImageLengthf - iLenf >= nDist_ForWhiteObjectPixels_ToImageEdgeMaxfMinf)
					else
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n 'BoundaryForASmearedRow_WithRangeOfLength' 1: too close to the image edge at iLenf = %d,  nWidCurf = %d", iLenf, nWidCurf);
						fprintf(fout_lr, "\n  nLenOfReasonableSmearedBoundaryf = %d remains the same", nLenOfReasonableSmearedBoundaryf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					}//else

				} //if (nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf < nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin)

			} // if (nLengthOfSegment_WithBlackPixels_Curf == 1  && iLenf > 0))

			nRedPrevf = nRedCurf;
		} // for (iLenf = nLenMinf; iLenf < nLenMaxf; iLenf++)

		if (nNumOfBlackPixelsInARowTotf == 0)
		{
			nLenOfSmearedBoundaryf = sColor_Imagef->nLength - 1;
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'BoundaryForASmearedRow_WithRangeOfLength' : a completely white row,  nLenOfSmearedBoundaryf = %d, nWidCurf = %d",
				nLenOfSmearedBoundaryf, nWidCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		}//if (nNumOfBlackPixelsInARowTotf == 0)

		if (nNumOfWhitePixelsInARowTotf == 0)
		{
			nLenOfSmearedBoundaryf = 0;
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'BoundaryForASmearedRow_WhiteWhite' : a completely black row,  nLenOfSmearedBoundaryf = %d, nWidCurf = %d",
				nLenOfSmearedBoundaryf, nWidCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		}//if (nNumOfWhitePixelsInARowTotf == 0)

		if (nNumOfBlackPixelsInARowTotf == 0)
		{
			fRatioOfWhiteAndBlackPixelsInARowf = fLarge;
		}//if (nNumOfBlackPixelsInARowTotf == 0)
		else
		{
			fRatioOfWhiteAndBlackPixelsInARowf = (float)(nNumOfWhitePixelsInARowTotf) / (float)(nNumOfBlackPixelsInARowTotf);
		}//else

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n The end of 'BoundaryForASmearedRow_WithRangeOfLength' left,  nNumOfBlackPixelsInARowTotf = %d,  nNumOfWhitePixelsInARowTotf = %d, nWidCurf = %d",
			nNumOfBlackPixelsInARowTotf, nNumOfWhitePixelsInARowTotf, nWidCurf);

		fprintf(fout_lr, "\n fRatioOfWhiteAndBlackPixelsInARowf = %E, nLenOfReasonableSmearedBoundaryf = %d,  nLenOfSmearedBoundaryf = %d",
			fRatioOfWhiteAndBlackPixelsInARowf, nLenOfReasonableSmearedBoundaryf, nLenOfSmearedBoundaryf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		if (nWidCurf == nOneWidToPrint)
		{

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n The total nNumfOfChangesFromBlackToWhiteTotf = %d, nWidCurf == %d", nNumfOfChangesFromBlackToWhiteTotf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		} //if (nWidCurf == nOneWidToPrint)

	} // if (sColor_Imagef->nSideOfObjectLocation == -1) 
///////////////////////////////////////////////////////////////////////////////////////////////////////

//object to the right
	if (sColor_Imagef->nSideOfObjectLocation == 1)
	{
		nLenOfSmearedBoundaryf = sColor_Imagef->nLength - 1; // initailly
		nLenOfReasonableSmearedBoundaryf = sColor_Imagef->nLength - 1; // initailly

		//for (iLenf = sColor_Imagef->nLength - 1; iLenf >= 0; iLenf--)
		for (iLenf = nLenMaxf; iLenf >= nLenMinf; iLenf--)
		{
			nIndexOfPixelCurf = iLenf + (nWidCurf*sColor_Imagef->nLength); // nLenMax);

			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
			//nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			//nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			if (nWidCurf == nOneWidToPrint)
			{
				if (nRedPrevf == nIntensityStatBlack && nRedCurf != nRedPrevf)
				{
					nNumfOfChangesFromBlackToWhiteTotf += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n A new nNumfOfChangesFromBlackToWhiteTotf = %d, nRedCurf = %d, iLenf = %d, nWidCurf == %d",
						nNumfOfChangesFromBlackToWhiteTotf, nRedCurf, iLenf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				} //if (nRedPrevf == nIntensityStatBlack && nRedCurf != nRedPrevf)

				if (nRedCurf > nIntensityStatBlack && nRedCurf < nIntensityStatMax)
				{

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n A warning for nRedCurf = %d, iLenf = %d, nWidCurf == %d", nRedCurf, iLenf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				}//if (nRedCurf > nIntensityStatBlack && nRedCurf < nIntensityStatMax)

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n nRedCurf = %d, iLenf = %d, nWidCurf == %d", nRedCurf, iLenf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			} //if (nWidCurf == nOneWidToPrint)
/////////////////////////////

			//if (nRedCurf >= nIntensityCloseToBlackMax)
			if (nRedCurf > nIntensityCloseToBlackMax)
			{
				nLenOfLastSuitableObjectPixelf = iLenf;

				nLengthOfSegment_WithBlackPixels_Curf = 0;

				nNumOfWhitePixelsInARowTotf += 1;

				if (iLenf == 0)
				{
					// the white segment could be connected to the edge

					nLenCurf = iLenf;
					goto MarkCharac_Aver_Right;
				}//if (iLenf == 0)

				nRedPrevf = nRedCurf;
				continue;
			} // if (nRedCurf > nIntensityCloseToBlackMax)...

			nNumOfBlackPixelsInARowTotf += 1;
			nLengthOfSegment_WithBlackPixels_Curf += 1;

			if (nLengthOfSegment_WithBlackPixels_Curf == 1 && iLenf < nImageLengthf - 1)
			{
				//object
				nLenCurf = iLenf + 1;

			MarkCharac_Aver_Right:	Charac_AverOf_ObjectNeighborsInARow_BlackWhite(

				sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

				nWidCurf, //const int nWidCurf,

				//the prev white pixel is located one pixel to the right
				nLenCurf, //const int nLenCurf,

				nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRowf, //const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
				/////////////////////////////////////////////////////////////
				nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf); // int &nAverIntensityOverNeighboingPixelsFrTheSameRowf);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'BoundaryForASmearedRow_WithRangeOfLength' 2:  nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf = %d, iLenf = %d, nWidCurf = %d",
					nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf, iLenf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				if (nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf < nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n Skipping iLenf = %d,  nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf = %d < nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin = %d",
						iLenf, nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf, nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					nRedPrevf = nRedCurf;
					continue;
				} //if (nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf < nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin)

				if (nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf >= nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin)
				{
					Verifying_IfAPixel_HasObjectNeighborsAtCertainAngles(
						sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

						nDistFor_ObjectNeighborsAtCertainAnglesf, //const int nDistFor_ObjectNeighborsAtCertainAnglesf,

						fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf, //const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,

						nWidCurf, //const int nWidCurf,
						nLenCurf, //const int nLenCurf,

						nNumOfObjectNeighborsAtCertainAnglesf); // int &nNumOfObjectNeighborsAtCertainAnglesf)

					if (nNumOfObjectNeighborsAtCertainAnglesf == 0)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n Not a pixel from the boundary of the right object; nWidCurf = %d, iLenf = %d, nLenCurf = %d\n", nWidCurf, iLenf, nLenCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						nRedPrevf = nRedCurf;
						continue; //for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
					}//if (nNumOfObjectNeighborsAtCertainAnglesf == 0)
					else if (nNumOfObjectNeighborsAtCertainAnglesf >= 1)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n A pixel from the boundary of the right object; nNumOfObjectNeighborsAtCertainAnglesf = %d, nWidCurf = %d, iLenf = %d, nLenCurf = %d\n",
							nNumOfObjectNeighborsAtCertainAnglesf, nWidCurf, iLenf, nLenCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					}//else if (nNumOfObjectNeighborsAtCertainAnglesf >= 1)
/////////////////////////////////////////////////////////////////////////////
					nLenOfSmearedBoundaryf = iLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n 'BoundaryForASmearedRow_WithRangeOfLength' (right): a new nLenOfSmearedBoundaryf = %d, iLenf = %d,  nWidCurf = %d", nLenOfSmearedBoundaryf, iLenf, nWidCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					if (iLenf >= nDist_ForWhiteObjectPixels_ToImageEdgeMaxfMinf)
					{
						nLenOfReasonableSmearedBoundaryf = iLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n 'BoundaryForASmearedRow_WithRangeOfLength' (right): a new nLenOfReasonableSmearedBoundaryf = %d, iLenf = %d,  nWidCurf = %d",
							nLenOfReasonableSmearedBoundaryf, iLenf, nWidCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						nRedPrevf = nRedCurf;
						continue;
					}//if (iLenf >= nDist_ForWhiteObjectPixels_ToImageEdgeMaxfMinf)
					else
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n 'BoundaryForASmearedRow_WithRangeOfLength' (right): too close to the image edge at iLenf = %d,  nWidCurf = %d", iLenf, nWidCurf);
						fprintf(fout_lr, "\n  nLenOfReasonableSmearedBoundaryf = %d remains the same", nLenOfReasonableSmearedBoundaryf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					}//else

				} //if (nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf < nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin)

			} // if (nLengthOfSegment_WithBlackPixels_Curf == 1 && iLenf < nImageLengthf - 1)

			nRedPrevf = nRedCurf;
		} //for (iLenf = nLenMaxf; iLenf >= nLenMinf; iLenf--)

		if (nNumOfBlackPixelsInARowTotf == 0)
		{
			nLenOfSmearedBoundaryf = 0; // sColor_Imagef->nLength - 1;
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'BoundaryForASmearedRow_WithRangeOfLength' (right): a completely white row,  nLenOfSmearedBoundaryf = %d, nWidCurf = %d",
				nLenOfSmearedBoundaryf, nWidCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		}//if (nNumOfBlackPixelsInARowTotf == 0)

		if (nNumOfWhitePixelsInARowTotf == 0)
		{
			nLenOfSmearedBoundaryf = sColor_Imagef->nLength - 1; //0;
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'BoundaryForASmearedRow_WhiteWhite' (right): a completely black row,  nLenOfSmearedBoundaryf = %d, nWidCurf = %d",
				nLenOfSmearedBoundaryf, nWidCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		}//if (nNumOfWhitePixelsInARowTotf == 0)

		if (nNumOfBlackPixelsInARowTotf == 0)
		{
			fRatioOfWhiteAndBlackPixelsInARowf = fLarge;
		}//if (nNumOfBlackPixelsInARowTotf == 0)
		else
		{
			fRatioOfWhiteAndBlackPixelsInARowf = (float)(nNumOfWhitePixelsInARowTotf) / (float)(nNumOfBlackPixelsInARowTotf);
		}//else

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n The end of 'BoundaryForASmearedRow_WithRangeOfLength' (right),  nNumOfBlackPixelsInARowTotf = %d,  nNumOfWhitePixelsInARowTotf = %d, nWidCurf = %d",
			nNumOfBlackPixelsInARowTotf, nNumOfWhitePixelsInARowTotf, nWidCurf);

		fprintf(fout_lr, "\n fRatioOfWhiteAndBlackPixelsInARowf = %E, nLenOfReasonableSmearedBoundaryf = %d,  nLenOfSmearedBoundaryf = %d",
			fRatioOfWhiteAndBlackPixelsInARowf, nLenOfReasonableSmearedBoundaryf, nLenOfSmearedBoundaryf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	} // if (sColor_Imagef->nSideOfObjectLocation == 1)

}//void BoundaryForASmearedRow_WithRangeOfLength(...
///////////////////////////////////////////////////////////////

int CharacteristicsOf_BackgroundNeighborsInARow_BlackWhite(

	const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

	const int nWidCurf,
	const int nLenCurf,

	const int nLenOf_BackgroundNeighboringPixelsFrTheSameRowf,
	/////////////////////////////////////////////////////////////
		int &nNumOf_NonBlackNeighboingPixelsFrTheSameRowf,

	int &nAverIntensityOverNeighboingPixelsFrTheSameRowf,
	int &nDiffBetweenMaxAndMinf)
{
	int
		nIndexOfPixelCurf,

		//iWidf,
		iLenf,

		nLenMinf,
		nLenMaxf,

		nRedCurf,
		nGreenCurf,
		nBlueCurf,

		nRedAverf = 0,
		nGreenAverf = 0,
		nBlueAverf = 0,

		nAverOfRedGreenBluef = 0,
		nAverOfRedGreenBlueMinf = nLarge,
		nAverOfRedGreenBlueMaxf = -nLarge,

		nLenOfPixelSegmentOfOneRowf;

	float
		fRatioOfLengthsf;

	if (nWidCurf < 0 || nWidCurf > sColor_Imagef->nWidth - 1 || nLenCurf <= 0 || nLenCurf >= sColor_Imagef->nLength - 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'CharacteristicsOf_BackgroundNeighborsInARow_BlackWhite' 1: ...->nWidth - 1 = %d, nWidCurf = %d, ...->nLength - 1 = %d, nLenCurf = %d",
			sColor_Imagef->nWidth - 1, nWidCurf, sColor_Imagef->nLength - 1, nLenCurf);

		fprintf(fout_lr, "\n\n An error in 'CharacteristicsOf_BackgroundNeighborsInARow_BlackWhite' 1: ...->nWidth - 1 = %d, nWidCurf = %d, ...->nLength - 1 = %d, nLenCurf = %d",
			sColor_Imagef->nWidth - 1, nWidCurf, sColor_Imagef->nLength - 1, nLenCurf);
		//printf("\n\n Please press any key to exit");	getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nWidCurf < 0 || nWidCurf > sColor_Imagef->nWidth - 1 || nLenCurf <= 0 || nLenCurf >= sColor_Imagef->nLength - 1)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n ////////////////////////////////////////////////////////////");
	fprintf(fout_lr, "\n 'CharacteristicsOf_BackgroundNeighborsInARow_BlackWhite' 1: nWidCurf = %d, nLenCurf = %d, nLenOf_BackgroundNeighboringPixelsFrTheSameRowf = %d, sColor_Imagef->nSideOfObjectLocation = %d",
		nWidCurf, nLenCurf, nLenOf_BackgroundNeighboringPixelsFrTheSameRowf, sColor_Imagef->nSideOfObjectLocation);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	nNumOf_NonBlackNeighboingPixelsFrTheSameRowf = 0;
	nAverIntensityOverNeighboingPixelsFrTheSameRowf = 0;
	nDiffBetweenMaxAndMinf = -nLarge;

	////////////////////////////////////////////////////////////////////////////////////////////
	//object to the left
	if (sColor_Imagef->nSideOfObjectLocation == -1)
	{

		//nLenMaxf = nLenCurf + nLenOf_BackgroundNeighboringPixelsFrTheSameRowf;
		nLenMaxf = nLenCurf + nLenOf_BackgroundNeighboringPixelsFrTheSameRowf + 1;

		if (nLenMaxf > sColor_Imagef->nLength - 1)
			nLenMaxf = sColor_Imagef->nLength - 1;

		nLenOfPixelSegmentOfOneRowf = nLenMaxf - nLenCurf + 1;

#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_lr, "\n 'CharacteristicsOf_BackgroundNeighborsInARow_BlackWhite' (left):  nLenCurf = %d, nLenMaxf = %d, nLenOfPixelSegmentOfOneRowf = %d", nLenCurf, nLenMaxf, nLenOfPixelSegmentOfOneRowf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		//for (iLenf = nLenCurf; iLenf <= nLenMaxf; iLenf++)
		for (iLenf = nLenCurf + 1; iLenf <= nLenMaxf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (nWidCurf*sColor_Imagef->nLength); // nLenMax);

			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
			nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			nAverOfRedGreenBluef = (nRedCurf + nGreenCurf + nBlueCurf) / 3;

			if (nAverOfRedGreenBluef < nAverOfRedGreenBlueMinf)
			{
				nAverOfRedGreenBlueMinf = nAverOfRedGreenBluef;
			}// if (nAverOfRedGreenBluef < nAverOfRedGreenBlueMinf)

			if (nAverOfRedGreenBluef > nAverOfRedGreenBlueMaxf)
			{
				nAverOfRedGreenBlueMaxf = nAverOfRedGreenBluef;
			}// if (nAverOfRedGreenBluef > nAverOfRedGreenBlueMaxf)

			nRedAverf += nRedCurf;
			nGreenAverf += nGreenCurf;
			nBlueAverf += nBlueCurf;

			//if (nRedCurf < nIntensityCloseToBlackMax && nGreenCurf < nIntensityCloseToBlackMax &&
				//nBlueCurf < nIntensityCloseToBlackMax)
			if (nRedCurf > nIntensityCloseToBlackMax || nGreenCurf > nIntensityCloseToBlackMax ||
				nBlueCurf > nIntensityCloseToBlackMax)
			{
				nNumOf_NonBlackNeighboingPixelsFrTheSameRowf += 1;
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'CharacteristicsOf_BackgroundNeighborsInARow_BlackWhite' (left): the next  nNumOf_NonBlackNeighboingPixelsFrTheSameRowf = %d, nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d",
					nNumOf_NonBlackNeighboingPixelsFrTheSameRowf, nRedCurf, nGreenCurf, nBlueCurf);

				fprintf(fout_lr, "\n iLenf = %d, nLenCurf = %d, nWidCurf = %d, nLenOfPixelSegmentOfOneRowf = %d", iLenf, nLenCurf, nWidCurf, nLenOfPixelSegmentOfOneRowf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			} // if (nRedCurf > nIntensityCloseToBlackMax || nGreenCurf > nIntensityCloseToBlackMax || ...

		} //for (iLenf = nLenCurf + 1; iLenf <= nLenMaxf; iLenf++)

		nRedAverf = nRedAverf / nLenOfPixelSegmentOfOneRowf;
		nGreenAverf = nGreenAverf / nLenOfPixelSegmentOfOneRowf;
		nBlueAverf = nBlueAverf / nLenOfPixelSegmentOfOneRowf;

		nAverIntensityOverNeighboingPixelsFrTheSameRowf = (nRedAverf + nGreenAverf + nBlueAverf) / 3;

		nDiffBetweenMaxAndMinf = nAverOfRedGreenBlueMaxf - nAverOfRedGreenBlueMinf;
	} // if (sColor_Imagef->nSideOfObjectLocation == -1)

	///////////////////////////////////////////////////////////////
	//object to the right   
	if (sColor_Imagef->nSideOfObjectLocation == 1)
	{
		//start counting from the left
		//nLenMinf = nLenCurf - nLenOf_BackgroundNeighboringPixelsFrTheSameRowf;
		nLenMinf = nLenCurf - nLenOf_BackgroundNeighboringPixelsFrTheSameRowf - 1;
		if (nLenMinf < 0)
			nLenMinf = 0;

		nLenOfPixelSegmentOfOneRowf = nLenCurf - nLenMinf + 1;


#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_lr, "\n 'CharacteristicsOf_BackgroundNeighborsInARow_BlackWhite' (right):  nLenMinf = %d, nLenCurf = %d, nLenOfPixelSegmentOfOneRowf = %d", nLenMinf, nLenCurf, nLenOfPixelSegmentOfOneRowf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		//for (iLenf = nLenMinf; iLenf <= nLenCurf; iLenf++)
		for (iLenf = nLenMinf; iLenf < nLenCurf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (nWidCurf*sColor_Imagef->nLength); // nLenMax);

			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
			nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
			nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

			nAverOfRedGreenBluef = (nRedCurf + nGreenCurf + nBlueCurf) / 3;

			if (nAverOfRedGreenBluef < nAverOfRedGreenBlueMinf)
			{
				nAverOfRedGreenBlueMinf = nAverOfRedGreenBluef;
			}// if (nAverOfRedGreenBluef < nAverOfRedGreenBlueMinf)

			if (nAverOfRedGreenBluef > nAverOfRedGreenBlueMaxf)
			{
				nAverOfRedGreenBlueMaxf = nAverOfRedGreenBluef;
			}// if (nAverOfRedGreenBluef > nAverOfRedGreenBlueMaxf)

			nRedAverf += nRedCurf;
			nGreenAverf += nGreenCurf;
			nBlueAverf += nBlueCurf;

	
		//	if (nRedCurf < nIntensityCloseToBlackMax && nGreenCurf < nIntensityCloseToBlackMax &&
			//	nBlueCurf < nIntensityCloseToBlackMax)
			if (nRedCurf > nIntensityCloseToBlackMax || nGreenCurf > nIntensityCloseToBlackMax ||
				nBlueCurf > nIntensityCloseToBlackMax)
			{
				nNumOf_NonBlackNeighboingPixelsFrTheSameRowf += 1;
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'CharacteristicsOf_BackgroundNeighborsInARow_BlackWhite' (right): the next  nNumOf_NonBlackNeighboingPixelsFrTheSameRowf = %d, nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d",
					nNumOf_NonBlackNeighboingPixelsFrTheSameRowf, nRedCurf, nGreenCurf, nBlueCurf);

				fprintf(fout_lr, "\n iLenf = %d, nLenCurf = %d, nWidCurf = %d, nLenOfPixelSegmentOfOneRowf = %d", iLenf, nLenCurf, nWidCurf, nLenOfPixelSegmentOfOneRowf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			} // if (nRedCurf > nIntensityCloseToBlackMax && nGreenCurf > nIntensityCloseToBlackMax && ...

		} //for (iLenf = nLenMinf; iLenf < nLenCurf; iLenf++)

		nRedAverf = nRedAverf / nLenOfPixelSegmentOfOneRowf;
		nGreenAverf = nGreenAverf / nLenOfPixelSegmentOfOneRowf;
		nBlueAverf = nBlueAverf / nLenOfPixelSegmentOfOneRowf;

		nAverIntensityOverNeighboingPixelsFrTheSameRowf = (nRedAverf + nGreenAverf + nBlueAverf) / 3;

		nDiffBetweenMaxAndMinf = nAverOfRedGreenBlueMaxf - nAverOfRedGreenBlueMinf;

	} // if (sColor_Imagef->nSideOfObjectLocation == 1)

////////////////////////////////////////////////////////////////////
	  //scaling the number of black pixels in case of closeness to the edge
	if (nLenOfPixelSegmentOfOneRowf < nLenOf_BackgroundNeighboringPixelsFrTheSameRowf)
	{
		fRatioOfLengthsf = (float)(nLenOf_BackgroundNeighboringPixelsFrTheSameRowf) / (float)(nLenOfPixelSegmentOfOneRowf);

		nNumOf_NonBlackNeighboingPixelsFrTheSameRowf = (int)((float)(nNumOf_NonBlackNeighboingPixelsFrTheSameRowf)*fRatioOfLengthsf);
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'CharacteristicsOf_BackgroundNeighborsInARow_BlackWhite': the final nNumOf_NonBlackNeighboingPixelsFrTheSameRowf = %d,  nLenCurf = %d, nWidCurf = %d", nNumOf_NonBlackNeighboingPixelsFrTheSameRowf, nLenCurf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	} //if (nLenOfPixelSegmentOfOneRowf < nLenOf_BackgroundNeighboringPixelsFrTheSameRowf)

	return SUCCESSFUL_RETURN;
} //int CharacteristicsOf_BackgroundNeighborsInARow_BlackWhite(...
/////////////////////////////////////////////////////////////////////////////////////////

int Verifying_IfAPixelBelongsToANarrowBoundary_BlackWhite(
	const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

	const int nWidCurf,
	const int nLenCurf,

	const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
	const int nLenOf_BackgroundNeighboringPixelsFrTheSameRowf)
{
	int CharacteristicsOf_ObjectNeighborsInARow_BlackWhite(

		const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

		const int nWidCurf,
		const int nLenCurf,

		const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
		/////////////////////////////////////////////////////////////
		int &nNumOfBlackNeighboingPixelsFrTheSameRowf,
		int &nAverIntensityOverNeighboingPixelsFrTheSameRowf,
		int &nDiffBetweenMaxAndMinf);

	int CharacteristicsOf_BackgroundNeighborsInARow_BlackWhite(

		const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

		const int nWidCurf,
		const int nLenCurf,

		const int nLenOf_BackgroundNeighboringPixelsFrTheSameRowf,
		/////////////////////////////////////////////////////////////
			int &nNumOf_NonBlackNeighboingPixelsFrTheSameRowf,

		int &nAverIntensityOverNeighboingPixelsFrTheSameRowf,
		int &nDiffBetweenMaxAndMinf);

int 
	nResf,
	nNumOfBlackNeighboing_ObjectPixelsFrTheSameRowf,
	nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf,

	nNumOf_NonBlackNeighboing_BackgroundPixelsFrTheSameRowf,
	nAverIntensityOverNeighboing_BackgroundPixelsFrTheSameRowf,
	nDiffBetweenMaxAndMinf;

nResf = CharacteristicsOf_ObjectNeighborsInARow_BlackWhite(

	sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

	nWidCurf, //const int nWidCurf,
	nLenCurf, //const int nLenCurf,

	nLenOf_ObjectNeighboringPixelsFrTheSameRowf, //const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
	/////////////////////////////////////////////////////////////
	nNumOfBlackNeighboing_ObjectPixelsFrTheSameRowf, //int &nNumOfBlackNeighboingPixelsFrTheSameRowf,
	nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf, //int &nAverIntensityOverNeighboingPixelsFrTheSameRowf,
	nDiffBetweenMaxAndMinf); // int &nDiffBetweenMaxAndMinf);

if (nResf == UNSUCCESSFUL_RETURN)
{
	return UNSUCCESSFUL_RETURN;
} // if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
fprintf(fout_lr, "\n\n 'Verifying_IfAPixelBelongsToANarrowBoundary_BlackWhite':  nNumOfBlackNeighboing_ObjectPixelsFrTheSameRowf = %d,  nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf = %d, nLenCurf = %d, nWidCurf = %d", 
	nNumOfBlackNeighboing_ObjectPixelsFrTheSameRowf, nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf,nLenCurf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

if (nNumOfBlackNeighboing_ObjectPixelsFrTheSameRowf > nNumOfBlackNeighboing_ObjectPixels_ShortMax)
{

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n No boundary pixel in 'Verifying_IfAPixelBelongsToANarrowBoundary_BlackWhite':  nNumOfBlackNeighboing_ObjectPixelsFrTheSameRowf = %d > nNumOfBlackNeighboing_ObjectPixels_ShortMax = %d",
		nNumOfBlackNeighboing_ObjectPixelsFrTheSameRowf, nNumOfBlackNeighboing_ObjectPixels_ShortMax);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	return UNSUCCESSFUL_RETURN;
} //if (nNumOfBlackNeighboing_ObjectPixelsFrTheSameRowf > nNumOfBlackNeighboing_ObjectPixels_ShortMax)

if (nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf < nAverIntensOfNeighboing_ObjectPixels_ShortMin)
{

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n No boundary pixel in 'Verifying_IfAPixelBelongsToANarrowBoundary_BlackWhite':  nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf = %d < nAverIntensOfNeighboing_ObjectPixels_ShortMin = %d",
		nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf, nAverIntensOfNeighboing_ObjectPixels_ShortMin);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	return UNSUCCESSFUL_RETURN;
} //if (nAverIntensityOverNeighboing_ObjectPixelsFrTheSameRowf < nAverIntensOfNeighboing_ObjectPixels_ShortMin)
////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	nResf = CharacteristicsOf_BackgroundNeighborsInARow_BlackWhite(

		sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

		nWidCurf, //const int nWidCurf,
		nLenCurf, //const int nLenCurf,

		nLenOf_BackgroundNeighboringPixelsFrTheSameRowf, //const int nLenOf_ObjectNeighboringPixelsFrTheSameRowf,
													 /////////////////////////////////////////////////////////////
		nNumOf_NonBlackNeighboing_BackgroundPixelsFrTheSameRowf, //int &nNumOf_NonBlackNeighboingPixelsFrTheSameRowf,

		nAverIntensityOverNeighboing_BackgroundPixelsFrTheSameRowf, //int &nAverIntensityOverNeighboingPixelsFrTheSameRowf,
		nDiffBetweenMaxAndMinf); // int &nDiffBetweenMaxAndMinf);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		return UNSUCCESSFUL_RETURN;
	} // if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'Verifying_IfAPixelBelongsToANarrowBoundary_BlackWhite':  nNumOf_NonBlackNeighboing_BackgroundPixelsFrTheSameRowf = %d,  nAverIntensityOverNeighboing_BackgroundPixelsFrTheSameRowf = %d, nLenCurf = %d, nWidCurf = %d",
		nNumOf_NonBlackNeighboing_BackgroundPixelsFrTheSameRowf, nAverIntensityOverNeighboing_BackgroundPixelsFrTheSameRowf, nLenCurf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nNumOf_NonBlackNeighboing_BackgroundPixelsFrTheSameRowf > nNumOf_NonBlackNeighboing_BackgroundPixels_ShortMax)
	{

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n No boundary pixel in 'Verifying_IfAPixelBelongsToANarrowBoundary_BlackWhite':  nNumOf_NonBlackNeighboing_BackgroundPixelsFrTheSameRowf = %d > nNumOf_NonBlackNeighboing_BackgroundPixels_ShortMax = %d",
			nNumOf_NonBlackNeighboing_BackgroundPixelsFrTheSameRowf, nNumOf_NonBlackNeighboing_BackgroundPixels_ShortMax);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nNumOfBlackNeighboing_BackgroundPixelsFrTheSameRowf > nNumOfBlackNeighboing_BackgroundPixels_ShortMax)

	if (nAverIntensityOverNeighboing_BackgroundPixelsFrTheSameRowf > nAverIntensOfNeighboing_BackgroundPixels_ShortMax)
	{

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n No boundary pixel in 'Verifying_IfAPixelBelongsToANarrowBoundary_BlackWhite':  nAverIntensityOverNeighboing_BackgroundPixelsFrTheSameRowf = %d > nAverIntensOfNeighboing_BackgroundPixels_ShortMax = %d",
			nAverIntensityOverNeighboing_BackgroundPixelsFrTheSameRowf, nAverIntensOfNeighboing_BackgroundPixels_ShortMax);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nAverIntensityOverNeighboing_BackgroundPixelsFrTheSameRowf > nAverIntensOfNeighboing_BackgroundPixels_ShortMax)

	return SUCCESSFUL_RETURN;
}//int Verifying_IfAPixelBelongsToANarrowBoundary_BlackWhite(...

///////////////////////////////////////////////////////////////////////////////////////////
int Erasing_Labels_BlackWhite(

	COLOR_IMAGE *sColor_Imagef) //[nImageSizeMax]
{
	int
		nIndexOfPixelCurf,

		iWidf,
		iLenf,

		nRedCurf,
	
		nLenOfObjectBoundaryCurf;

	//object to the left   
	if (sColor_Imagef->nSideOfObjectLocation == -1)
	{
		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			//start counting from the right
			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n 'Erasing_Labels_BlackWhite' (object to the left): nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
				nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)
			{
				if (nLenOfObjectBoundaryCurf == -1)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n No boundary/erasing in 'Erasing_Labels_BlackWhite' 1: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
						nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					continue;
				} //if (nLenOfObjectBoundaryCurf == -1)

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'Erasing_Labels_BlackWhite' 1: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
				printf("\n\n An error in 'Erasing_Labels_BlackWhite' 1: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}// if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)
/////////////////////////////////////

			for (iLenf = sColor_Imagef->nLength - 1; iLenf > nLenOfObjectBoundaryCurf; iLenf--)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				//nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				//nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				if (nRedCurf != nIntensityStatBlack) //sColor_Imagef->nIntensityOfBackground_Red)
				{
					sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 3; // changed to the background

					sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nIntensityStatBlack; // sColor_Imagef->nIntensityOfBackground_Red;
					sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nIntensityStatBlack; // sColor_Imagef->nIntensityOfBackground_Red;
					sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nIntensityStatBlack; // sColor_Imagef->nIntensityOfBackground_Red;

				} // if (nRedCurf != nIntensityStatBlack) 

			} //for (iLenf = sColor_Imagef->nLength - 1; iLenf > nLenOfObjectBoundaryCurf; iLenf--)

		} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
	} // if (sColor_Imagef->nSideOfObjectLocation == -1)

	  //object to the right
	if (sColor_Imagef->nSideOfObjectLocation == 1)
	{
		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			//start counting from the left
			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n 'Erasing_Labels_BlackWhite' (object to the right): nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
				nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)
			{
				if (nLenOfObjectBoundaryCurf == -1)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n No boundary/erasing in 'Erasing_Labels_BlackWhite' 2: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
						nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					continue;
				} //if (nLenOfObjectBoundaryCurf == -1)

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'Erasing_Labels_BlackWhite' 2: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
				printf("\n\n An error in 'Erasing_Labels_BlackWhite' 2: nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			}// if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)

			for (iLenf = 0; iLenf < nLenOfObjectBoundaryCurf; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];
				//nGreenCurf = sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf];
				//nBlueCurf = sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf];

				if (nRedCurf != nIntensityStatBlack) //sColor_Imagef->nIntensityOfBackground_Red)
				{
					sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 3; // changed to the background

					sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = nIntensityStatBlack; // sColor_Imagef->nIntensityOfBackground_Red;
					sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = nIntensityStatBlack; // sColor_Imagef->nIntensityOfBackground_Red;
					sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = nIntensityStatBlack; // sColor_Imagef->nIntensityOfBackground_Red;

				} // if (nRedCurf != nIntensityStatBlack) 

			} //for (iLenf = 0; iLenf < nLenOfObjectBoundaryCurf; iLenf++)

		} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
	} // if (sColor_Imagef->nSideOfObjectLocation == 1)

	return SUCCESSFUL_RETURN;
} //int Erasing_Labels_BlackWhite(...

 ///////////////////////////////////////////////////////////////////

int Initializing_Color_To_CurSize(
	const int nImageWidthf,
	const int nImageLengthf,

	COLOR_IMAGE *sColor_Imagef) //[]
{
	int
		nIndexOfPixelCurf,

		nImageSizeCurf = nImageWidthf*nImageLengthf,
		iWidf,
		iLenf;

	sColor_Imagef->nSideOfObjectLocation = 0; // neither left nor right
	sColor_Imagef->nIntensityOfBackground_Red = -1;
	sColor_Imagef->nIntensityOfBackground_Green = -1;
	sColor_Imagef->nIntensityOfBackground_Blue = -1;

	sColor_Imagef->nWidth = nImageWidthf;
	sColor_Imagef->nLength = nImageLengthf;

	//sGrayscale_Imagef->nPixel_ValidOrNotArr = new int[nImageSizeCurf];

	sColor_Imagef->nLenObjectBoundary_Arr = new int[nImageWidthf];

	sColor_Imagef->nRed_Arr = new int[nImageSizeCurf];
	sColor_Imagef->nGreen_Arr = new int[nImageSizeCurf];
	sColor_Imagef->nBlue_Arr = new int[nImageSizeCurf];

	sColor_Imagef->nIsAPixelBackground_Arr = new int[nImageSizeCurf];

	if (sColor_Imagef->nLenObjectBoundary_Arr == nullptr || sColor_Imagef->nRed_Arr == nullptr || sColor_Imagef->nGreen_Arr == nullptr || sColor_Imagef->nBlue_Arr == nullptr || 
		sColor_Imagef->nIsAPixelBackground_Arr == nullptr)
	{
		return UNSUCCESSFUL_RETURN;
	} //if (sColor_Imagef->nLenObjectBoundary_Arr == nullptr || sColor_Imagef->nRed_Arr == nullptr || ...

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = -1;

		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			//sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1; //valid for no restrictions
			sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = -1;
			sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = -1;
			sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = -1;

			sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 0; //all pixels belong to the object
		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
	} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	return SUCCESSFUL_RETURN;
} //int Initializing_Color_To_CurSize(...

///////////////////////////////////////////////////////////////////////////////////////////////
void Verifying_IfAPixel_HasObjectNeighborsAtCertainAngles(
	const COLOR_IMAGE *sColor_Imagef, //[nImageSizeMax]

	const int nDistFor_ObjectNeighborsAtCertainAnglesf,

	const float fCoefDist_ForVerticalNeighbors_ToImageEdgeMaxf,

	const int nWidCurf,
	const int nLenCurf,

	int &nNumOfObjectNeighborsAtCertainAnglesf)
	
{
	int
		nNumOfAnglesf = 4, //up, down, 45 degrees up to the back and 45 degrees down to the back
		iAnglef,

		nWidOfNeighborf,
		nLenOfNeighborf,

		nImageWidthf = sColor_Imagef->nWidth,
		nImageLengthf = sColor_Imagef->nLength,

		nIndexOfPixelCurf,

		nRedOfNeighborf,

		nDistFor_ObjectNeighborsAtCertainAnglesCurf = nDistFor_ObjectNeighborsAtCertainAnglesf, //initially

		nDistFor_ObjectNeighborsAtCertainAnglesInitf = nDistFor_ObjectNeighborsAtCertainAnglesf,
		nItersFor_DistFor_ObjectNeighborsMaxf =  2, //3,
		nItersFor_DistFor_ObjectNeighborsCurf = 0, 

		nIncludeVerticalPixelsOfNotf, //1-- include, 0 -- not

		nIncludeEdgePixelsOfNotf = 0, //1-- include, 0 -- not

		nDistOf1stVerticalNeighbors_ToImageEdgeMinf; // 

/////////////////////////////////////////////////////////
	nDistOf1stVerticalNeighbors_ToImageEdgeMinf = (int)(fCoefDist_ForVerticalNeighbors_ToImageEdgeMax*(float)(nImageLengthf)),

	nNumOfObjectNeighborsAtCertainAnglesf = 0;

	if (sColor_Imagef->nSideOfObjectLocation == -1)  //object to the left
	{
		if (nWidCurf == nOneWidToPrint)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n Verifying...AtCertainAngles: nImageLengthf - nDistOf1stVerticalNeighbors_ToImageEdgeMinf = %d, nWidCurf = %d",
				nImageLengthf - nDistOf1stVerticalNeighbors_ToImageEdgeMinf, nWidCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		} //if (nWidCurf == nOneWidToPrint)

		if (nLenCurf > nImageLengthf - nDistOf1stVerticalNeighbors_ToImageEdgeMinf)
		{
			nIncludeVerticalPixelsOfNotf = 0;
		}//if (nLenCurf > nImageLengthf - nDistOf1stVerticalNeighbors_ToImageEdgeMinf)
		else //if (nLenCurf <= nImageLengthf - nDistOf1stVerticalNeighbors_ToImageEdgeMinf)
		{
			nIncludeVerticalPixelsOfNotf = 1;
		}//if (nLenCurf  <= nImageLengthf - nDistOf1stVerticalNeighbors_ToImageEdgeMinf)

		if (nWidCurf == nOneWidToPrint)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n nIncludeVerticalPixelsOfNotf = %d", nIncludeVerticalPixelsOfNotf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		} //if (nWidCurf == nOneWidToPrint)

////////////////////////////////////////////////////////////////////////
//left
	MarkLoopFor_iAnglef_Left: for (iAnglef = 0; iAnglef < nNumOfAnglesf; iAnglef++)
		{
			if (iAnglef == 0 && nIncludeVerticalPixelsOfNotf == 1) // down
			{
				nWidOfNeighborf = nWidCurf - nDistFor_ObjectNeighborsAtCertainAnglesCurf;

				if (nWidOfNeighborf < 0)
				{
					if (nIncludeEdgePixelsOfNotf == 1)
					{
						nWidOfNeighborf = 0;
					} // if (nIncludeEdgePixelsOfNotf == 1)
					else
					{
						continue;
					}//else

				} //if (nWidOfNeighborf < 0)
				nLenOfNeighborf = nLenCurf;

			} // if (iAnglef == 0 && nIncludeVerticalPixelsOfNotf == 1)
			else if (iAnglef == 0 && nIncludeVerticalPixelsOfNotf == 0) // down
			{
				continue;
			} // if (iAnglef == 0 && nIncludeVerticalPixelsOfNotf == 0)
			else if (iAnglef == 1 && nIncludeVerticalPixelsOfNotf == 1) // Up
			{
				nWidOfNeighborf = nWidCurf + nDistFor_ObjectNeighborsAtCertainAnglesCurf;

				if (nWidOfNeighborf >= nImageWidthf)
				{
					if (nIncludeEdgePixelsOfNotf == 1)
					{
						nWidOfNeighborf = nImageWidthf - 1;
					} // if (nIncludeEdgePixelsOfNotf == 1)
					else
					{
						continue;
					}//else
				} // if (nWidOfNeighborf >= nImageWidthf)
				nLenOfNeighborf = nLenCurf;

			} // else if (iAnglef == 1 && nIncludeVerticalPixelsOfNotf == 1)
			else if (iAnglef == 1 && nIncludeVerticalPixelsOfNotf == 0) // down
			{
				continue;
			} // if (iAnglef == 1 && nIncludeVerticalPixelsOfNotf == 0)
			else if (iAnglef == 2) // down and back
			{
				nWidOfNeighborf = nWidCurf - nDistFor_ObjectNeighborsAtCertainAnglesCurf;
				if (nWidOfNeighborf < 0)
				{
					if (nIncludeEdgePixelsOfNotf == 1)
					{
						nWidOfNeighborf = 0;
					} // if (nIncludeEdgePixelsOfNotf == 1)
					else
					{
						continue;
					}//else

				} //if (nWidOfNeighborf < 0)
				nLenOfNeighborf = nLenCurf - nDistFor_ObjectNeighborsAtCertainAnglesCurf;
				if (nLenOfNeighborf < 0)
				{
					nLenOfNeighborf = 0;
					//continue;
				}//if (nLenOfNeighborf < 0)

			} // else if (iAnglef == 2) // down and back
			else if (iAnglef == 3) // up and back
			{
				nWidOfNeighborf = nWidCurf + nDistFor_ObjectNeighborsAtCertainAnglesCurf;

				if (nWidOfNeighborf >= nImageWidthf)
				{
					if (nIncludeEdgePixelsOfNotf == 1)
					{
						nWidOfNeighborf = nImageWidthf - 1;
					} // if (nIncludeEdgePixelsOfNotf == 1)
					else
					{
						continue;
					}//else
				} // if (nWidOfNeighborf >= nImageWidthf)

				nLenOfNeighborf = nLenCurf - nDistFor_ObjectNeighborsAtCertainAnglesCurf;
				if (nLenOfNeighborf < 0)
				{
					nLenOfNeighborf = 0;
					//continue;
				}//if (nLenOfNeighborf < 0)

			} // else if (iAnglef == 3) // up and back

			nIndexOfPixelCurf = nLenOfNeighborf + (nWidOfNeighborf*nImageLengthf);

			nRedOfNeighborf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

			if (nRedOfNeighborf == nIntensityStatMax)
			{
				nNumOfObjectNeighborsAtCertainAnglesf += 1;
			}//if (nRedOfNeighborf == nIntensityStatMax)

			if (nWidCurf == nOneWidToPrint)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n Left: iAnglef = %d, nLenOfNeighborf = %d, nWidOfNeighborf = %d, nIndexOfPixelCurf = %d", iAnglef,nLenOfNeighborf, nWidOfNeighborf, nIndexOfPixelCurf);

				fprintf(fout_lr, "\n nRedOfNeighborf = %d, nNumOfObjectNeighborsAtCertainAnglesf = %d, nDistFor_ObjectNeighborsAtCertainAnglesCurf = %d", 
					nRedOfNeighborf, nNumOfObjectNeighborsAtCertainAnglesf, nDistFor_ObjectNeighborsAtCertainAnglesCurf);

				fprintf(fout_lr, "\n nIncludeEdgePixelsOfNotf = %d, nItersFor_DistFor_ObjectNeighborsCurf = %d, nItersFor_DistFor_ObjectNeighborsMaxf = %d",
					nIncludeEdgePixelsOfNotf, nItersFor_DistFor_ObjectNeighborsCurf, nItersFor_DistFor_ObjectNeighborsMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			} //if (nWidCurf == nOneWidToPrint)

		}// for (iAnglef = 0; iAnglef < nNumOfAnglesf; iAnglef++)

		if (nWidCurf == nOneWidToPrint)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Verifying_If...' (left): nNumOfObjectNeighborsAtCertainAnglesf = %d, nItersFor_DistFor_ObjectNeighborsCurf = %d, nLenCurf = %d",
				nNumOfObjectNeighborsAtCertainAnglesf, nItersFor_DistFor_ObjectNeighborsCurf, nLenCurf);

			if (nNumOfObjectNeighborsAtCertainAnglesf > 0)
			{
				fprintf(fout_lr, "\n Exit by nNumOfObjectNeighborsAtCertainAnglesf > 0 at nWidCurf = %d", nWidCurf);
			} //if (nNumOfObjectNeighborsAtCertainAnglesf > 0)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		} //if (nWidCurf == nOneWidToPrint)
////////////////////////////////////////////////////////////////////////////////////


		if (nNumOfObjectNeighborsAtCertainAnglesf == 0 && nItersFor_DistFor_ObjectNeighborsCurf < nItersFor_DistFor_ObjectNeighborsMaxf)
		{
			//closer to the left edge than 'nDistFor_ObjectNeighborsAtCertainAnglesInitf'
			if (nLenCurf < nDistFor_ObjectNeighborsAtCertainAnglesInitf)
			{
	
				if (nWidCurf == nOneWidToPrint)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n nLenCurf = %d < nDistFor_ObjectNeighborsAtCertainAnglesInitf = %d",
						nLenCurf, nDistFor_ObjectNeighborsAtCertainAnglesInitf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				} //if (nWidCurf == nOneWidToPrint)	

				nItersFor_DistFor_ObjectNeighborsCurf += 1;
				nDistFor_ObjectNeighborsAtCertainAnglesCurf = nDistFor_ObjectNeighborsAtCertainAnglesCurf / 2;

				if (nWidCurf == nOneWidToPrint)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n A new nDistFor_ObjectNeighborsAtCertainAnglesCurf = %d, nItersFor_DistFor_ObjectNeighborsCurf = %d",
						nDistFor_ObjectNeighborsAtCertainAnglesCurf, nItersFor_DistFor_ObjectNeighborsCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				} //if (nWidCurf == nOneWidToPrint)

				goto MarkLoopFor_iAnglef_Left;
			}//if (nLenCurf < nDistFor_ObjectNeighborsAtCertainAnglesInitf)
			else if (nLenCurf >= nDistFor_ObjectNeighborsAtCertainAnglesInitf && nLenCurf < 2* nDistFor_ObjectNeighborsAtCertainAnglesInitf)
			{
				nItersFor_DistFor_ObjectNeighborsCurf += 1;
				nIncludeEdgePixelsOfNotf = 1;

				if (nWidCurf == nOneWidToPrint)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n nLenCurf = %d < 2*nDistFor_ObjectNeighborsAtCertainAnglesInitf = %d, switching to nIncludeEdgePixelsOfNotf = %d",
						nLenCurf, 2 * nDistFor_ObjectNeighborsAtCertainAnglesInitf, nIncludeEdgePixelsOfNotf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				} //if (nWidCurf == nOneWidToPrint)
				goto MarkLoopFor_iAnglef_Left;
			} //else if (nLenCurf >= nDistFor_ObjectNeighborsAtCertainAnglesInitf && nLenCurf < 2* nDistFor_ObjectNeighborsAtCertainAnglesInitf)

		} // if (nNumOfObjectNeighborsAtCertainAnglesf == 0 && nItersFor_DistFor_ObjectNeighborsCurf < nItersFor_DistFor_ObjectNeighborsMaxf)

	} //if (sColor_Imagef->nSideOfObjectLocation == -1)

	//////////////////////////////////////////
//right
	else if (sColor_Imagef->nSideOfObjectLocation == 1)  //object to the right
	{
		if (nLenCurf < nDistOf1stVerticalNeighbors_ToImageEdgeMinf)
		{
			nIncludeVerticalPixelsOfNotf = 0;
		}//if (nLenCurf < nDistOf1stVerticalNeighbors_ToImageEdgeMinf)
		else //if (nLenCurf >= nDistOf1stVerticalNeighbors_ToImageEdgeMinf)
		{
			nIncludeVerticalPixelsOfNotf = 1;
		}//else //if (nLenCurf >= nDistOf1stVerticalNeighbors_ToImageEdgeMinf)

MarkLoopFor_iAnglef_Right:		for (iAnglef = 0; iAnglef < nNumOfAnglesf; iAnglef++)
		{
			if (iAnglef == 0 && nIncludeVerticalPixelsOfNotf == 1) // up
			{
				nWidOfNeighborf = nWidCurf - nDistFor_ObjectNeighborsAtCertainAnglesCurf;
				if (nWidOfNeighborf < 0)
				{
					if (nIncludeEdgePixelsOfNotf == 1)
					{
						nWidOfNeighborf = 0;
					} // if (nIncludeEdgePixelsOfNotf == 1)
					else
					{
						continue;
					}//else

				} //if (nWidOfNeighborf < 0)

				nLenOfNeighborf = nLenCurf;

			} // if (iAnglef == 0 && nIncludeVerticalPixelsOfNotf == 1)
			else if (iAnglef == 0 && nIncludeVerticalPixelsOfNotf == 0) // up
			{
				continue;
			} // if (iAnglef == 0 && nIncludeVerticalPixelsOfNotf == 0)
			else if (iAnglef == 1 && nIncludeVerticalPixelsOfNotf == 1) // down
			{
				nWidOfNeighborf = nWidCurf + nDistFor_ObjectNeighborsAtCertainAnglesCurf;

				if (nWidOfNeighborf >= nImageWidthf)
				{
					if (nIncludeEdgePixelsOfNotf == 1)
					{
						nWidOfNeighborf = nImageWidthf - 1;
					} // if (nIncludeEdgePixelsOfNotf == 1)
					else
					{
						continue;
					}//else
				} // if (nWidOfNeighborf >= nImageWidthf)

				nLenOfNeighborf = nLenCurf;

			} // else if (iAnglef == 1 && nIncludeVerticalPixelsOfNotf == 1)
			else if (iAnglef == 1 && nIncludeVerticalPixelsOfNotf == 0) // up
			{
				continue;
			} // if (iAnglef == 1 && nIncludeVerticalPixelsOfNotf == 0)
			else if (iAnglef == 2) // up and back
			{
				nWidOfNeighborf = nWidCurf - nDistFor_ObjectNeighborsAtCertainAnglesCurf;
				if (nWidOfNeighborf < 0)
				{
					if (nIncludeEdgePixelsOfNotf == 1)
					{
						nWidOfNeighborf = 0;
					} // if (nIncludeEdgePixelsOfNotf == 1)
					else
					{
						continue;
					}//else

				} //if (nWidOfNeighborf < 0)

				nLenOfNeighborf = nLenCurf + nDistFor_ObjectNeighborsAtCertainAnglesCurf;
				if (nLenOfNeighborf >= nImageLengthf)
				{
					nLenOfNeighborf = nImageLengthf - 1;
					//continue;
				} //if (nLenOfNeighborf >= nImageLengthf)

			} // else if (iAnglef == 2) // up and back
			else if (iAnglef == 3) // down and back
			{
				nWidOfNeighborf = nWidCurf + nDistFor_ObjectNeighborsAtCertainAnglesCurf;

				if (nWidOfNeighborf >= nImageWidthf)
				{
					if (nIncludeEdgePixelsOfNotf == 1)
					{
						nWidOfNeighborf = nImageWidthf - 1;
					} // if (nIncludeEdgePixelsOfNotf == 1)
					else
					{
						continue;
					}//else
				} // if (nWidOfNeighborf >= nImageWidthf)

				nLenOfNeighborf = nLenCurf + nDistFor_ObjectNeighborsAtCertainAnglesCurf;
				if (nLenOfNeighborf >= nImageLengthf)
				{
					nLenOfNeighborf = nImageLengthf - 1;
					//continue;
				} //if (nLenOfNeighborf >= nImageLengthf)

			} // else if (iAnglef == 3) // down and back

			nIndexOfPixelCurf = nLenOfNeighborf + (nWidOfNeighborf*nImageLengthf);

			nRedOfNeighborf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

			if (nRedOfNeighborf == nIntensityStatMax)
			{
				nNumOfObjectNeighborsAtCertainAnglesf += 1;
			}//if (nRedOfNeighborf == nIntensityStatMax)

			if (nWidCurf == nOneWidToPrint)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n Right: iAnglef = %d, nLenOfNeighborf = %d, nWidOfNeighborf = %d, nIndexOfPixelCurf = %d", iAnglef, nLenOfNeighborf, nWidOfNeighborf, nIndexOfPixelCurf);

				fprintf(fout_lr, "\n nRedOfNeighborf = %d, nNumOfObjectNeighborsAtCertainAnglesf = %d, nDistFor_ObjectNeighborsAtCertainAnglesCurf = %d",
					nRedOfNeighborf, nNumOfObjectNeighborsAtCertainAnglesf, nDistFor_ObjectNeighborsAtCertainAnglesCurf);

				fprintf(fout_lr, "\n nIncludeEdgePixelsOfNotf = %d, nItersFor_DistFor_ObjectNeighborsCurf = %d, nItersFor_DistFor_ObjectNeighborsMaxf = %d",
					nIncludeEdgePixelsOfNotf,nItersFor_DistFor_ObjectNeighborsCurf, nItersFor_DistFor_ObjectNeighborsMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			} //if (nWidCurf == nOneWidToPrint)

		}// for (iAnglef = 0; iAnglef < nNumOfAnglesf; iAnglef++)

		if (nWidCurf == nOneWidToPrint)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Verifying_If...' (right): nNumOfObjectNeighborsAtCertainAnglesf = %d, nItersFor_DistFor_ObjectNeighborsCurf = %d, nLenCurf = %d",
				nNumOfObjectNeighborsAtCertainAnglesf, nItersFor_DistFor_ObjectNeighborsCurf, nLenCurf);

			if (nNumOfObjectNeighborsAtCertainAnglesf > 0)
			{
				fprintf(fout_lr, "\n Exit by nNumOfObjectNeighborsAtCertainAnglesf > 0 at nWidCurf = %d", nWidCurf);
			} //if (nNumOfObjectNeighborsAtCertainAnglesf > 0)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		} //if (nWidCurf == nOneWidToPrint)

/////////////////////////////////////////////////////////////////////

		if (nNumOfObjectNeighborsAtCertainAnglesf == 0 && nItersFor_DistFor_ObjectNeighborsCurf < nItersFor_DistFor_ObjectNeighborsMaxf)
		{
//closer to the right edge than 'nDistFor_ObjectNeighborsAtCertainAnglesCurf'
			if (nLenCurf > nImageLengthf - nDistFor_ObjectNeighborsAtCertainAnglesInitf)
			{
				if (nWidCurf == nOneWidToPrint)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n nLenCurf = %d > nImageLengthf - nDistFor_ObjectNeighborsAtCertainAnglesInitf = %d",
						nLenCurf, nImageLengthf - nDistFor_ObjectNeighborsAtCertainAnglesInitf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				} //if (nWidCurf == nOneWidToPrint)	

				nItersFor_DistFor_ObjectNeighborsCurf += 1;
				nDistFor_ObjectNeighborsAtCertainAnglesCurf = nDistFor_ObjectNeighborsAtCertainAnglesCurf / 2;

				if (nWidCurf == nOneWidToPrint)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n A new nDistFor_ObjectNeighborsAtCertainAnglesCurf = %d, nItersFor_DistFor_ObjectNeighborsCurf = %d",
						nDistFor_ObjectNeighborsAtCertainAnglesCurf, nItersFor_DistFor_ObjectNeighborsCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				} //if (nWidCurf == nOneWidToPrint)

				goto MarkLoopFor_iAnglef_Right;
			}//if (nLenCurf > nImageLengthf - nDistFor_ObjectNeighborsAtCertainAnglesInitf)

			else if (nLenCurf > nImageLengthf - 2* nDistFor_ObjectNeighborsAtCertainAnglesInitf && nLenCurf < nImageLengthf - nDistFor_ObjectNeighborsAtCertainAnglesInitf)
			{
				nItersFor_DistFor_ObjectNeighborsCurf += 1; //new
				nIncludeEdgePixelsOfNotf = 1; 

				if (nWidCurf == nOneWidToPrint)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n nLenCurf = %d > nImageLengthf - 2*nDistFor_ObjectNeighborsAtCertainAnglesInitf = %d, switching to nIncludeEdgePixelsOfNotf = %d",
						nLenCurf, nImageLengthf - 2 * nDistFor_ObjectNeighborsAtCertainAnglesInitf, nIncludeEdgePixelsOfNotf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				} //if (nWidCurf == nOneWidToPrint)

				goto MarkLoopFor_iAnglef_Right;
			} //else if (nLenCurf > nImageLengthf - 2*nDistFor_ObjectNeighborsAtCertainAnglesInitf && nLenCurf < nImageLengthf - nDistFor_ObjectNeighborsAtCertainAnglesInitf)

		} // if (nNumOfObjectNeighborsAtCertainAnglesf == 0 && nItersFor_DistFor_ObjectNeighborsCurf < nItersFor_DistFor_ObjectNeighborsMaxf)

	} //else if (sColor_Imagef->nSideOfObjectLocation == 1)

//	return SUCCESSFUL_RETURN;
}//void Verifying_IfAPixel_HasObjectNeighborsAtCertainAngles(...
 ////////////////////////////////////////////////////////////////////////////////////////////////

void MedianFiterTo_APixel_InAMaskImage(
	const int nWidOfPixelf,
	const int nLenOfPixelf,

	int &nPixelShadedToWhiteOrBlackf,
	COLOR_IMAGE *sColor_Imagef)
{
	int
		nIndexOfPixelCurf,

		nIndexOfPixelCentref, // = nLenOfPixelf + (nWidOfPixelf*sColor_Imagef->nLength,

		nWidOfWindowInitf,
		nWidOfWindowFinf,

		nLenOfWindowLeftf,
		nLenOfWindowRightf,

		nSumOfOnesForOutputOneMin_Curf,

		nSumOfOnesCurf = 0,
		nRedCurf, //0 or 1
							//nAreaf, // = (2 * nRadiusOfMedianFilter + 1)*(2 * nRadiusOfMedianFilter + 1), //init
		iWidf,
		iLenf;

	nPixelShadedToWhiteOrBlackf = 0; //no change initially
	nIndexOfPixelCentref = nLenOfPixelf + (nWidOfPixelf*sColor_Imagef->nLength);

	nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCentref];

	nWidOfWindowInitf = nWidOfPixelf - nRadiusOfMedianFilter;
	if (nWidOfWindowInitf < 0)
	{
		nWidOfWindowInitf = 0;
	} //if (nWidOfWindowInitf < 0)

	nWidOfWindowFinf = nWidOfPixelf + nRadiusOfMedianFilter;
	if (nWidOfWindowFinf > sColor_Imagef->nWidth - 1)
	{
		nWidOfWindowFinf = sColor_Imagef->nWidth - 1;
	} //if (nWidOfWindowFinf > sColor_Imagef->nWidth - 1)

	nLenOfWindowLeftf = nLenOfPixelf - nRadiusOfMedianFilter;
	if (nLenOfWindowLeftf < 0)
	{
		nLenOfWindowLeftf = 0;
	} //if (nLenOfWindowLeftf < 0)

	nLenOfWindowRightf = nLenOfPixelf + nRadiusOfMedianFilter;
	if (nLenOfWindowRightf > sColor_Imagef->nLength - 1)
	{
		nLenOfWindowRightf = sColor_Imagef->nLength - 1;
	}// if (nLenOfWindowRightf > sColor_Imagef->nLength - 1)

	nSumOfOnesForOutputOneMin_Curf = ((nWidOfWindowFinf - nWidOfWindowInitf + 1)*(nLenOfWindowRightf - nLenOfWindowLeftf + 1) / 2) + 1;

	for (iWidf = nWidOfWindowInitf; iWidf <= nWidOfWindowFinf; iWidf++)
	{
		for (iLenf = nLenOfWindowLeftf; iLenf <= nLenOfWindowRightf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength);

			if (sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] == nIntensityStatMax)
			{
				nSumOfOnesCurf += 1;
				if (nSumOfOnesCurf >= nSumOfOnesForOutputOneMin_Curf) 
				{
//' nSumOfOnesCurf' is sufficient for a white pixel
					if (nRedCurf == nIntensityStatBlack)
					{
						nPixelShadedToWhiteOrBlackf = 1; //changing from black to white

						sColor_Imagef->nRed_Arr[nIndexOfPixelCentref] = nIntensityStatMax;
						sColor_Imagef->nGreen_Arr[nIndexOfPixelCentref] = nIntensityStatMax;
						sColor_Imagef->nBlue_Arr[nIndexOfPixelCentref] = nIntensityStatMax;
					} //if (nRedCurf == nIntensityStatBlack)

//he pixel is already white -- no changes are needed
					goto MarkEndOfMedianFiterTo_APixel_InAMaskImage;
					//return SUCCESSFUL_RETURN;
				}//if (nSumOfOnesCurf >= nSumOfOnesForOutputOneMin_Curf)

			} //if (sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] == 1)

		} //for (iLenf = nLenOfWindowLeftf; iLenf <= nLenOfWindowRightf; iLenf++)

	} // for (iWidf = nWidOfWindowInitf; iWidf <= nWidOfWindowFinf; iWidf++)

//' nSumOfOnesCurf' is insufficient for a white pixel; a black pixel is the output
	if (nRedCurf == nIntensityStatMax)
	{
		nPixelShadedToWhiteOrBlackf = -1; //changing from white to black
	
		sColor_Imagef->nRed_Arr[nIndexOfPixelCentref] = nIntensityStatBlack;
		sColor_Imagef->nGreen_Arr[nIndexOfPixelCentref] = nIntensityStatBlack;
		sColor_Imagef->nBlue_Arr[nIndexOfPixelCentref] = nIntensityStatBlack;
	} //if (nRedCurf == nIntensityStatMax)

MarkEndOfMedianFiterTo_APixel_InAMaskImage: iWidf = -1;
	//return SUCCESSFUL_RETURN;
}//void MedianFiterTo_APixel_InAMaskImage(...

///////////////////////////////////////////////////////////////////////////////////////////

void MedianFiterTo_AnArea_InAMaskImage(
	const int nWidMinf,
	const int nWidMaxf,//inclusive

	const int nLenMinf,
	const int nLenMaxf,////inclusive

	COLOR_IMAGE *sColor_Imagef)
{
	void MedianFiterTo_APixel_InAMaskImage(
		const int nWidOfPixelf,
		const int nLenOfPixelf,

		int &nPixelShadedToWhiteOrBlackf,
		COLOR_IMAGE *sColor_Imagef);

	int
		nPixelShadedToWhiteOrBlackf,

		nNumOfPixels_ShadedToWhiteOrBlackTotf = 0,

		iWidf,
		iLenf;

	for (iWidf = nWidMinf; iWidf <= nWidMaxf; iWidf++)
	{
		for (iLenf = nLenMinf; iLenf <= nLenMaxf; iLenf++)
		{
			MedianFiterTo_APixel_InAMaskImage(
				iWidf, //const int nWidOfPixelf,
				iLenf, //const int nLenOfPixelf,

				nPixelShadedToWhiteOrBlackf, //int &nPixelShadedToWhiteOrBlackf,
				sColor_Imagef); // COLOR_IMAGE *sColor_Imagef);

			if (nPixelShadedToWhiteOrBlackf == 1 || nPixelShadedToWhiteOrBlackf == -1)
			{
				nNumOfPixels_ShadedToWhiteOrBlackTotf += 1;
			}//if (nPixelShadedToWhiteOrBlackf == 1 || nPixelShadedToWhiteOrBlackf == -1)

		} //for (iLenf = nLenMinf; iLenf <= nLenMaxf; iLenf++)

	} //for (iWidf = nWidMinf; iWidf <= nWidMaxf; iWidf++)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'MedianFiterTo_AnArea_InAMaskImage': nNumOfPixels_ShadedToWhiteOrBlackTotf = %d", nNumOfPixels_ShadedToWhiteOrBlackTotf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

}//void MedianFiterTo_AnArea_InAMaskImage(...
//////////////////////////////////////////////////////////////////////
void DoesABlackPixel_HaveAWhiteNeighbor(
	const int nWidOfPixelf,
	const int nLenOfPixelf,

	const COLOR_IMAGE *sColor_Imagef,

	int &nYesOrNo_ForAWhiteNeighboringPixelf) // 1 -- Yes, 0 -- No
{
int
	 nWidMinf = nWidOfPixelf - 1,
	nWidMaxf = nWidOfPixelf + 1,//inclusive

	nLenMinf = nLenOfPixelf - 1,
	nLenMaxf = nLenOfPixelf + 1, //inclusive

	nImageWidthf = sColor_Imagef->nWidth,
	nImageLengthf = sColor_Imagef->nLength,

	nIndexOfPixelCurf,
	nRedCurf,

	iWidf,
	iLenf;
/////////////////////////////////////
nYesOrNo_ForAWhiteNeighboringPixelf = 0; //initially

if (nWidMinf < 0)
	nWidMinf = 0;

if (nWidMaxf > nImageWidthf - 1)
	nWidMaxf = nImageWidthf - 1;

if (nLenMinf < 0)
	nLenMinf = 0;

if (nLenMaxf > nImageLengthf - 1)
nLenMaxf = nImageLengthf - 1;

for (iWidf = nWidMinf; iWidf <= nWidMaxf; iWidf++)
{
	for (iLenf = nLenMinf; iLenf <= nLenMaxf; iLenf++)
	{
		nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);
		nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

		if (nRedCurf == nIntensityStatMax) 
		{
//a white neighboring pixel exists
			nYesOrNo_ForAWhiteNeighboringPixelf = 1;
			goto MarkEndOfDoesABlackPixel_HaveAWhiteNeighbor;

		} // if (nRedCurf == nIntensityStatMax) 

	} //for (iLenf = nLenMinf; iLenf <= nLenMaxf; iLenf++)

} //for (iWidf = nWidMinf; iWidf <= nWidMaxf; iWidf++)

MarkEndOfDoesABlackPixel_HaveAWhiteNeighbor: nIndexOfPixelCurf = 0;
}//void DoesABlackPixel_HaveAWhiteNeighbor(
/////////////////////////////////////////////////////////////////////////////////

void DoesAWhitePixel_HaveABlackNeighbor(
	const int nWidOfPixelf,
	const int nLenOfPixelf,

	const COLOR_IMAGE *sColor_Imagef,

	int &nYesOrNo_ForABlackNeighboringPixelf) // 1 -- Yes, 0 -- No
{
	int
		nWidMinf = nWidOfPixelf - 1,
		nWidMaxf = nWidOfPixelf + 1,//inclusive

		nLenMinf = nLenOfPixelf - 1,
		nLenMaxf = nLenOfPixelf + 1, //inclusive

		nImageWidthf = sColor_Imagef->nWidth,
		nImageLengthf = sColor_Imagef->nLength,

		nIndexOfPixelCurf,
		nRedCurf,

		iWidf,
		iLenf;
	/////////////////////////////////////
	nYesOrNo_ForABlackNeighboringPixelf = 0; //initially

	if (nWidMinf < 0)
		nWidMinf = 0;

	if (nWidMaxf > nImageWidthf - 1)
		nWidMaxf = nImageWidthf - 1;

	if (nLenMinf < 0)
		nLenMinf = 0;

	if (nLenMaxf > nImageLengthf - 1)
		nLenMaxf = nImageLengthf - 1;

	for (iWidf = nWidMinf; iWidf <= nWidMaxf; iWidf++)
	{
		for (iLenf = nLenMinf; iLenf <= nLenMaxf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);
			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

			if (nRedCurf == nIntensityStatBlack)
			{
				//a black neighboring pixel exists
				nYesOrNo_ForABlackNeighboringPixelf = 1;
				goto MarkEndOfDoesABlackPixel_HaveAWhiteNeighbor;

			} // if (nRedCurf == nIntensityStatBlack) 

		} //for (iLenf = nLenMinf; iLenf <= nLenMaxf; iLenf++)

	} //for (iWidf = nWidMinf; iWidf <= nWidMaxf; iWidf++)

MarkEndOfDoesABlackPixel_HaveAWhiteNeighbor: nIndexOfPixelCurf = 0;
}//DoesAWhitePixel_HaveABlackNeighbor(
/////////////////////////////////////////////////////////////////////////////

void MedianFiterTo_BoundaryArea(

	int &nNumOfChangedPixelsCloseToBoundaryTotf,
	COLOR_IMAGE *sColor_Imagef)
{
	void MedianFiterTo_APixel_InAMaskImage(
		const int nWidOfPixelf,
		const int nLenOfPixelf,

		int &nPixelShadedToWhiteOrBlackf,
		COLOR_IMAGE *sColor_Imagef);

	int
		//nResf,
		nLenOfObjectBoundaryCurf,

		nLen_ToLeftOfBoundaryCurf,
		nLen_ToRightOfBoundaryCurf,

		nPixelShadedToWhiteOrBlackf,

		nNumOfPixelsTurnedToWhitef = 0,
		nNumOfPixelsTurnedToBlackf = 0,

		iWidf,
		iLenf;

	nNumOfChangedPixelsCloseToBoundaryTotf = 0;

	for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
	{
		nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];

		if (sColor_Imagef->nSideOfObjectLocation == -1) //left
		{
			nLen_ToLeftOfBoundaryCurf = nLenOfObjectBoundaryCurf - (nCoeff_DeviationFromBoundaryForMedianFilter_Into*nRadiusOfMedianFilter_ForBoundary);
			if (nLen_ToLeftOfBoundaryCurf < 0)
			{
				nLen_ToLeftOfBoundaryCurf = 0;
			} //if (nLen_ToLeftOfBoundaryCurf < 0)

			nLen_ToRightOfBoundaryCurf = nLenOfObjectBoundaryCurf + (nCoeff_DeviationFromBoundaryForMedianFilter_Out*nRadiusOfMedianFilter_ForBoundary);

			if (nLen_ToRightOfBoundaryCurf > sColor_Imagef->nLength - 1)
			{
				nLen_ToRightOfBoundaryCurf = sColor_Imagef->nLength - 1;
			}// if (nLen_ToRightOfBoundaryCurf > sColor_Imagef->nLength - 1)

		} //if (sColor_Imagef->nSideOfObjectLocation == -1) //left
		else if (sColor_Imagef->nSideOfObjectLocation == 1) //right
		{
			nLen_ToLeftOfBoundaryCurf = nLenOfObjectBoundaryCurf - (nCoeff_DeviationFromBoundaryForMedianFilter_Out*nRadiusOfMedianFilter_ForBoundary);
			if (nLen_ToLeftOfBoundaryCurf < 0)
			{
				nLen_ToLeftOfBoundaryCurf = 0;
			} //if (nLen_ToLeftOfBoundaryCurf < 0)

			nLen_ToRightOfBoundaryCurf = nLenOfObjectBoundaryCurf + (nCoeff_DeviationFromBoundaryForMedianFilter_Into*nRadiusOfMedianFilter_ForBoundary);
			if (nLen_ToRightOfBoundaryCurf > sColor_Imagef->nLength - 1)
			{
				nLen_ToRightOfBoundaryCurf = sColor_Imagef->nLength - 1;
			}// if (nLen_ToRightOfBoundaryCurf > sColor_Imagef->nLength - 1)
		} //else if (sColor_Imagef->nSideOfObjectLocation == 1) //right

		for (iLenf = nLen_ToLeftOfBoundaryCurf; iLenf <= nLen_ToRightOfBoundaryCurf; iLenf++)
		{
			 MedianFiterTo_APixel_InAMaskImage(
				iWidf, //const int nWidOfPixelf,
				iLenf, //const int nLenOfPixelf,

				nPixelShadedToWhiteOrBlackf, //int &nPixelShadedToWhiteOrBlackf,

				sColor_Imagef); // MASK_IMAGE *sColor_Imagef);

			if (nPixelShadedToWhiteOrBlackf == 1)
			{
				nNumOfPixelsTurnedToWhitef += 1;

				if (iLenf > sColor_Imagef->nLenObjectBoundary_Arr[iWidf] && sColor_Imagef->nSideOfObjectLocation == -1) //left
				{
					sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = iLenf;
				} //if (iLenf > sColor_Imagef->nLenObjectBoundary_Arr[iWidf] && sColor_Imagef->nSideOfObjectLocation == -1) //left

				if (iLenf < sColor_Imagef->nLenObjectBoundary_Arr[iWidf] && sColor_Imagef->nSideOfObjectLocation == 1) //right
				{
					sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = iLenf;
				} //if (iLenf < sColor_Imagef->nLenObjectBoundary_Arr[iWidf] && sColor_Imagef->nSideOfObjectLocation == 1) //right

			} //if (nPixelShadedToWhiteOrBlackf == 1)
			else if (nPixelShadedToWhiteOrBlackf == -1)
			{
				nNumOfPixelsTurnedToBlackf += 1;
			}//else if (nPixelShadedToWhiteOrBlackf == -1)
/*
			if (iWidf == 273)
			{
				fprintf(fout_lr, "\n\n iLenf = %d, nPixelShadedToWhiteOrBlackf = %d, nNumOfPixelsTurnedToWhitef = %d, nNumOfPixelsTurnedToBlackf = %d",
					iLenf, nPixelShadedToWhiteOrBlackf, nNumOfPixelsTurnedToWhitef, nNumOfPixelsTurnedToBlackf);
			}//if (iWidf == 273)
*/
		} //for (iLenf = nLen_ToLeftOfBoundaryCurf; iLenf <= nLen_ToRightOfBoundaryCurf; iLenf++)

	} // for (iWidf = 0; iWidf <  sColor_Imagef->nWidth; iWidf++)

	nNumOfChangedPixelsCloseToBoundaryTotf = nNumOfPixelsTurnedToWhitef + nNumOfPixelsTurnedToBlackf;

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n The end in 'MedianFiterTo_BoundaryArea': nNumOfPixelsTurnedToWhitef = %d, nNumOfPixelsTurnedToBlackf = %d", nNumOfPixelsTurnedToWhitef, nNumOfPixelsTurnedToBlackf);
	fprintf(fout_lr, "\n\n The end in 'MedianFiterTo_BoundaryArea': nNumOfPixelsTurnedToWhitef = %d, nNumOfPixelsTurnedToBlackf = %d", nNumOfPixelsTurnedToWhitef, nNumOfPixelsTurnedToBlackf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//return SUCCESSFUL_RETURN;
}//void MedianFiterTo_BoundaryArea(...
/////////////////////////////////////////////////////////////////////////////

int MedianFiterTo_BlackAreaBehindBoundary(

	int &nWidToStartForBlackAreaBehindBoundaryf,

	int &nWid_BlackPixelsBehindBoundaryMinf,
	int &nWid_BlackPixelsBehindBoundaryMaxf,

	int &nNumOfChangedPixelsBehindBoundaryTotf,

	COLOR_IMAGE *sColor_Imagef)
{
	int RangeOfWidthsWithBlackPixelsBehindBoundary_WithBoundaryAdjust(

		int &nWidToStartForBlackAreaBehindBoundaryf,

		int &nWid_BlackPixelsBehindBoundaryMinf,
		int &nWid_BlackPixelsBehindBoundaryMaxf,

		COLOR_IMAGE *sColor_Imagef);

	void DoesABlackPixel_HaveAWhiteNeighbor(
		const int nWidOfPixelf,
		const int nLenOfPixelf,

		const COLOR_IMAGE *sColor_Imagef,

		int &nYesOrNo_ForAWhiteNeighboringPixelf); // 1 -- Yes, 0 -- No

	void MedianFiterTo_APixel_InAMaskImage(
		const int nWidOfPixelf,
		const int nLenOfPixelf,

		int &nPixelShadedToWhiteOrBlackf,
		COLOR_IMAGE *sColor_Imagef);

	int
		nResf,
		nIndexOfPixelCurf,
		nLenOfObjectBoundaryCurf,

		nLen_ToLeftOfBoundaryCurf,
		nLen_ToRightOfBoundaryCurf,

		nPixelShadedToWhiteOrBlackf,

		nNumOfPixelsTurnedToWhitef = 0,
		nNumOfPixelsTurnedToBlackf = 0,

		nYesOrNo_ForAWhiteNeighboringPixelf,

		nRedCurf,

		nNumOfBlackPixelsBehindBoundaryTotf = 0,
		iWidf,
		iLenf;

	nNumOfChangedPixelsBehindBoundaryTotf = 0;
	nWid_BlackPixelsBehindBoundaryMinf = -1;
	nWid_BlackPixelsBehindBoundaryMaxf = -1;

	if (nWid_BlackPixelsBehindBoundaryMinf == -1)
	{
		nResf = RangeOfWidthsWithBlackPixelsBehindBoundary_WithBoundaryAdjust(

			nWidToStartForBlackAreaBehindBoundaryf, //int &nWidToStartForBlackAreaBehindBoundaryf,
			nWid_BlackPixelsBehindBoundaryMinf, //int &nWid_BlackPixelsBehindBoundaryMinf,
			nWid_BlackPixelsBehindBoundaryMaxf, //int &nWid_BlackPixelsBehindBoundaryMaxf,

			sColor_Imagef); // COLOR_IMAGE *sColor_Imagef);

		if (nResf == -2) //nWid_BlackPixelsBehindBoundaryMinf == -1)
		{

#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n 'MedianFiterTo_BlackAreaBehindBoundary' 1: no black pixels behind the boundary have been found initially");
			fprintf(fout_lr, "\n\n 'MedianFiterTo_BlackAreaBehindBoundary' 1: no black pixels behind the boundary have been found initially");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			return (-2);
		} // if (nResf == -2)
		else
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n 'MedianFiterTo_BlackAreaBehindBoundary': nWid_BlackPixelsBehindBoundaryMinf = %d, nWid_BlackPixelsBehindBoundaryMaxf = %d",
				nWid_BlackPixelsBehindBoundaryMinf, nWid_BlackPixelsBehindBoundaryMaxf);

			fprintf(fout_lr, "\n\n 'MedianFiterTo_BlackAreaBehindBoundary': nWid_BlackPixelsBehindBoundaryMinf = %d, nWid_BlackPixelsBehindBoundaryMaxf = %d",
				nWid_BlackPixelsBehindBoundaryMinf, nWid_BlackPixelsBehindBoundaryMaxf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		}//else
	} //if (nWid_BlackPixelsBehindBoundaryMinf == -1)

	 ///////////////////////////////////////////////
	nNumOfChangedPixelsBehindBoundaryTotf = 0;

	for (iWidf = nWid_BlackPixelsBehindBoundaryMinf; iWidf <= nWid_BlackPixelsBehindBoundaryMaxf; iWidf++)
	{
		nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];

		if (sColor_Imagef->nSideOfObjectLocation == -1) //left
		{
			nLen_ToLeftOfBoundaryCurf = 0;
			nLen_ToRightOfBoundaryCurf = nLenOfObjectBoundaryCurf - 1;
		}// if (sColor_Imagef->nSideOfObjectLocation == -1) //left
		else if (sColor_Imagef->nSideOfObjectLocation == 1) //right
		{
			nLen_ToLeftOfBoundaryCurf = nLenOfObjectBoundaryCurf + 1;
			nLen_ToRightOfBoundaryCurf = sColor_Imagef->nLength - 1;
		} //else if (sColor_Imagef->nSideOfObjectLocation == 1) //right

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'MedianFiterTo_BlackAreaBehindBoundary': iWidf = %d, nLenOfObjectBoundaryCurf = %d", iWidf, nLenOfObjectBoundaryCurf);
		fprintf(fout_lr, "\n\n nWid_BlackPixelsBehindBoundaryMinf = %d, nWid_BlackPixelsBehindBoundaryMaxf = %d",
			nWid_BlackPixelsBehindBoundaryMinf, nWid_BlackPixelsBehindBoundaryMaxf);

		fprintf(fout_lr, "\n nLen_ToLeftOfBoundaryCurf = %d, nLen_ToRightOfBoundaryCurf = %d", nLen_ToLeftOfBoundaryCurf, nLen_ToRightOfBoundaryCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		for (iLenf = nLen_ToLeftOfBoundaryCurf; iLenf <= nLen_ToRightOfBoundaryCurf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength);

			nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

//applying smoothing only to black pixels with white pixel neighbors
			if (nRedCurf == nIntensityStatBlack)
			{
				nNumOfBlackPixelsBehindBoundaryTotf += 1;

				DoesABlackPixel_HaveAWhiteNeighbor(
					iWidf, //const int nWidOfPixelf,
					iLenf, //const int nLenOfPixelf,

					sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef,

					nYesOrNo_ForAWhiteNeighboringPixelf); // int &nYesOrNo_ForAWhiteNeighboringPixelf); // 1 -- Yes, 0 -- No

				//a white pixel neighbor is present
				if (nYesOrNo_ForAWhiteNeighboringPixelf == 1)
				{
					MedianFiterTo_APixel_InAMaskImage(
						iWidf, //const int nWidOfPixelf,
						iLenf, //const int nLenOfPixelf,

						nPixelShadedToWhiteOrBlackf, //int &nPixelShadedToWhiteOrBlackf,

						sColor_Imagef); // MASK_IMAGE *sColor_Imagef);

					if (nPixelShadedToWhiteOrBlackf == 1)
					{
						nNumOfPixelsTurnedToWhitef += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n White: nNumOfPixelsTurnedToWhitef = %d, iWidf = %d, iLenf = %d", nNumOfPixelsTurnedToWhitef, iWidf, iLenf);
						fprintf(fout_lr, "\n nLen_ToLeftOfBoundaryCurf = %d, nLen_ToRightOfBoundaryCurf = %d", nLen_ToLeftOfBoundaryCurf, nLen_ToRightOfBoundaryCurf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					} //if (nPixelShadedToWhiteOrBlackf == 1)

					else if (nPixelShadedToWhiteOrBlackf == -1)
					{
					
#ifndef COMMENT_OUT_ALL_PRINTS
							printf("\n\n  An error in 'MedianFiterTo_BlackAreaBehindBoundary' : a black pixel can not be converted to another black one");
							printf("\n iLenf = %d, iWidf = %d", iLenf, iWidf);
							fprintf(fout_lr, "\n\n  An error in 'MedianFiterTo_BlackAreaBehindBoundary' : a black pixel can not be converted to another black one");
							fprintf(fout_lr, "\n iLenf = %d, iWidf = %d", iLenf, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS		

						return UNSUCCESSFUL_RETURN;
					}//else if (nPixelShadedToWhiteOrBlackf == -1)


				} //if (nYesOrNo_ForAWhiteNeighboringPixelf == 1)

				else //if (nYesOrNo_ForAWhiteNeighboringPixelf == 0)
				{
//no white pixel neighbor
					continue; //for (iLenf = ...
				} //else //if (nYesOrNo_ForAWhiteNeighboringPixelf == 0)

			}//if (nRedCurf == nIntensityStatBlack)

		} //for (iLenf = nLen_ToLeftOfBoundaryCurf; iLenf <= nLen_ToRightOfBoundaryCurf; iLenf++)

	} // for (iWidf = nWid_BlackPixelsBehindBoundaryMinf; iWidf <= nWid_BlackPixelsBehindBoundaryMaxf; iWidf++)

	if (nNumOfBlackPixelsBehindBoundaryTotf == 0)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'MedianFiterTo_BlackAreaBehindBoundary' 2: no black pixels behind the boundary have been found at nWid_BlackPixelsBehindBoundaryMinf = %d, nWid_BlackPixelsBehindBoundaryMaxf = %d",
			nWid_BlackPixelsBehindBoundaryMinf, nWid_BlackPixelsBehindBoundaryMaxf);
		fprintf(fout_lr, "\n\n 'MedianFiterTo_BlackAreaBehindBoundary' 2: no black pixels behind the boundary have been found at nWid_BlackPixelsBehindBoundaryMinf = %d, nWid_BlackPixelsBehindBoundaryMaxf = %d",
			nWid_BlackPixelsBehindBoundaryMinf, nWid_BlackPixelsBehindBoundaryMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return (-2);
	}//if (nNumOfBlackPixelsBehindBoundaryTotf == 0)

	nNumOfChangedPixelsBehindBoundaryTotf = nNumOfPixelsTurnedToWhitef + nNumOfPixelsTurnedToBlackf;

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n The end in 'MedianFiterTo_BlackAreaBehindBoundary': nNumOfPixelsTurnedToWhitef = %d, nNumOfPixelsTurnedToBlackf = %d", nNumOfPixelsTurnedToWhitef, nNumOfPixelsTurnedToBlackf);
	fprintf(fout_lr, "\n\n The end in 'MedianFiterTo_BlackAreaBehindBoundary': nNumOfPixelsTurnedToWhitef = %d, nNumOfPixelsTurnedToBlackf = %d", nNumOfPixelsTurnedToWhitef, nNumOfPixelsTurnedToBlackf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	return SUCCESSFUL_RETURN;
}//int MedianFiterTo_BlackAreaBehindBoundary(...

 ////////////////////////////////////////////////////////////////////
int RangeOfWidthsWithBlackPixelsBehindBoundary_WithBoundaryAdjust(

	int &nWidToStartForBlackAreaBehindBoundaryf,

	int &nWid_BlackPixelsBehindBoundaryMinf,
	int &nWid_BlackPixelsBehindBoundaryMaxf,

	COLOR_IMAGE *sColor_Imagef)
{
	int
		nLenOfObjectBoundaryPrevf,

		nLenOfObjectBoundaryCurf,

		nIndexOfPixelCurf,

		nLineHasBlackPixelsBehindBoundaryf,

		nNumOfInitLinesWithNoBlackPixelssBehindBoundaryf = 0,

	//	nLineWithBlackPixelsBehindBoundary_AlreadyFoundf = 0, //not initially

		n1stWhitePixelFoundInARowf, //1 - found, 0 -not
//find a new possible boundary after smoothing
		nShiftIntoBackgroundf = nCoeff_DeviationFromBoundaryForMedianFilter_Out* nRadiusOfMedianFilter_ForBoundary,

		nRedCurf,
		iLenInitf,
		iWidf,
		iLenf;
///////////////////////////////////////////////////////////////////
	nWid_BlackPixelsBehindBoundaryMinf = -1; //not set up yet
	nWid_BlackPixelsBehindBoundaryMaxf = -1; // sColor_Imagef->nWidth;

	
#ifndef COMMENT_OUT_ALL_PRINTS
/*
		nLenOfObjectBoundaryPrevf = sColor_Imagef->nLenObjectBoundary_Arr[nOneWidToPrint];
		fprintf(fout_lr, "\n\n 'RangeOfWidthsWithBlack...': printing 'nOneWidToPrint' row, nLenOfObjectBoundaryPrevf = %d\n", nLenOfObjectBoundaryPrevf);
	for (iLenf = nLenOfObjectBoundaryPrevf; iLenf < sColor_Imagef->nLength; iLenf++)
	{
		nIndexOfPixelCurf = iLenf + (nOneWidToPrint*sColor_Imagef->nLength);

		fprintf(fout_lr, "%d:%d, ", iLenf, sColor_Imagef->nRed_Arr[nIndexOfPixelCurf]);
	} //for (iLenf = nLenOfObjectBoundaryPrevf; iLenf < sColor_Imagef->nLength; iLenf++)
*/
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		
	   	 
	for (iWidf = nWidToStartForBlackAreaBehindBoundaryf; iWidf < sColor_Imagef->nWidth; iWidf++)
	{
		nLenOfObjectBoundaryPrevf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];

		nLineHasBlackPixelsBehindBoundaryf = 0; //no black pixels behind  initially

#ifndef COMMENT_OUT_ALL_PRINTS
	//fprintf(fout_lr, "\n The boundary: iWidf = %d, nLenOfObjectBoundaryPrevf = %d", iWidf, nLenOfObjectBoundaryPrevf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		if (sColor_Imagef->nSideOfObjectLocation == -1) //left
		{
			n1stWhitePixelFoundInARowf = 0; //not found yet
			nLineHasBlackPixelsBehindBoundaryf = 0;

			iLenInitf = nLenOfObjectBoundaryPrevf + nShiftIntoBackgroundf; // 

			if (iLenInitf > sColor_Imagef->nLength - 1)
				iLenInitf = sColor_Imagef->nLength - 1;

//coming from the right for left objects
			for (iLenf = iLenInitf; iLenf >= 0; iLenf--)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

				if (nRedCurf == nIntensityStatMax && n1stWhitePixelFoundInARowf == 0)
				{
					n1stWhitePixelFoundInARowf = 1;

					nLenOfObjectBoundaryCurf = iLenf;

					sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = nLenOfObjectBoundaryCurf;
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n 'RangeOfWidthsWithBlack...' (left): an adjusted boundary due to the smoothing, iWidf = %d, nLenOfObjectBoundaryCurf = %d, nLenOfObjectBoundaryPrevf = %d",
						iWidf, nLenOfObjectBoundaryCurf, nLenOfObjectBoundaryPrevf);

					fprintf(fout_lr, "\n sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				} //if (nRedCurf == nIntensityStatMax && n1stWhitePixelFoundInARowf == 0)

				if (nRedCurf == nIntensityStatBlack && n1stWhitePixelFoundInARowf == 1)
				{
					nLineHasBlackPixelsBehindBoundaryf = 1; //there are black pixels behind 

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n 'RangeOfWidthsWithBlack...' (left): a black pixel at iWidf = %d, iLenf = %d, nLenOfObjectBoundaryPrevf = %d", iWidf, iLenf, nLenOfObjectBoundaryPrevf);
					//fprintf(fout_lr, "\n iIterGlob = %d", iIterGlob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					if (nWid_BlackPixelsBehindBoundaryMinf == -1)
					{
						nWid_BlackPixelsBehindBoundaryMinf = iWidf;
					} //if (nWid_BlackPixelsBehindBoundaryMinf == -1)

					if (nWid_BlackPixelsBehindBoundaryMinf != -1 && iWidf > nWid_BlackPixelsBehindBoundaryMaxf)
					{
						nWid_BlackPixelsBehindBoundaryMaxf = iWidf;

					} //if (nWid_BlackPixelsBehindBoundaryMinf != -1 && iWidf > nWid_BlackPixelsBehindBoundaryMaxf)

					break;  // for (iLenf = 0; iLenf < nLenOfObjectBoundaryPrevf; iLenf++)
				} //if (sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] == nIntensityStatBlack)

			} //for (iLenf = iLenInitf; iLenf >= 0; iLenf--)

		} //if (sColor_Imagef->nSideOfObjectLocation == -1)
	
		else if (sColor_Imagef->nSideOfObjectLocation == 1) // right
		{
			//fprintf(fout_lr, "\n The boundary: iWidf = %d, nLenOfObjectBoundaryPrevf = %d", iWidf, nLenOfObjectBoundaryPrevf);
			
			n1stWhitePixelFoundInARowf = 0; //not found yet
			nLineHasBlackPixelsBehindBoundaryf = 0;

			iLenInitf = nLenOfObjectBoundaryPrevf - nShiftIntoBackgroundf; // 

			if (iLenInitf < 0)
				iLenInitf = 0;

			for (iLenf = iLenInitf; iLenf < sColor_Imagef->nLength - 1; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

				if (nRedCurf == nIntensityStatMax && n1stWhitePixelFoundInARowf == 0)
				{
					n1stWhitePixelFoundInARowf = 1;

					nLenOfObjectBoundaryCurf = iLenf;

					sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = nLenOfObjectBoundaryCurf;
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n 'RangeOfWidthsWithBlack...' (right): an adjusted boundary due to the smoothing, iWidf = %d, nLenOfObjectBoundaryCurf = %d, nLenOfObjectBoundaryPrevf = %d", 
						iWidf, nLenOfObjectBoundaryCurf, nLenOfObjectBoundaryPrevf);

					fprintf(fout_lr, "\n sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf,sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				} //if (nRedCurf == nIntensityStatMax && n1stWhitePixelFoundInARowf == 0)

				if (nRedCurf == nIntensityStatBlack && n1stWhitePixelFoundInARowf == 1)
				{
					nLineHasBlackPixelsBehindBoundaryf = 1; //there are black pixels behind 

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n 'RangeOfWidthsWithBlack...' (right): a black pixel at iWidf = %d, iLenf = %d, nLenOfObjectBoundaryCurf = %d", iWidf, iLenf, nLenOfObjectBoundaryCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					if (nWid_BlackPixelsBehindBoundaryMinf == -1)
					{
						nWid_BlackPixelsBehindBoundaryMinf = iWidf;
					} //if (nWid_BlackPixelsBehindBoundaryMinf == -1)

					if (nWid_BlackPixelsBehindBoundaryMinf != -1 && iWidf > nWid_BlackPixelsBehindBoundaryMaxf)
					{
						nWid_BlackPixelsBehindBoundaryMaxf = iWidf;

					} //if (nWid_BlackPixelsBehindBoundaryMinf != -1 && iWidf > nWid_BlackPixelsBehindBoundaryMaxf)

					break;  // for (iLenf = sColor_Imagef->nLength - 1; iLenf > nLenOfObjectBoundaryPrevf; iLenf--)
				} //if (nRedCurf == nIntensityStatBlack && n1stWhitePixelFoundInARowf == 1)

			} //for (iLenf = iLenInitf; iLenf < sColor_Imagef->nLength - 1; iLenf++)

		} //if (sColor_Imagef->nSideOfObjectLocation == 1) //right

		if (nLineHasBlackPixelsBehindBoundaryf == 0)
		{
			nNumOfInitLinesWithNoBlackPixelssBehindBoundaryf += 1;
		} // if (nLineHasBlackPixelsBehindBoundaryf == 0)

		if (nLineHasBlackPixelsBehindBoundaryf == 0 && n1stWhitePixelFoundInARowf == 0)
		{
			sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = sColor_Imagef->nLength - 1;
		} //if (nLineHasBlackPixelsBehindBoundaryf == 0 && n1stWhitePixelFoundInARowf == 0)

	} // for (iWidf = nWidToStartForBlackAreaBehindBoundaryf; iWidf <  sColor_Imagef->nWidth; iWidf++)

	if (nWid_BlackPixelsBehindBoundaryMinf != -1 &&	nWid_BlackPixelsBehindBoundaryMaxf != -1)
	{
		nWidToStartForBlackAreaBehindBoundaryf = nWidToStartForBlackAreaBehindBoundaryf + nNumOfInitLinesWithNoBlackPixelssBehindBoundaryf;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n The end of 'RangeOfWidthsWithBlack...': nWid_BlackPixelsBehindBoundaryMinf = %d, nWid_BlackPixelsBehindBoundaryMaxf = %d",
			 nWid_BlackPixelsBehindBoundaryMinf, nWid_BlackPixelsBehindBoundaryMaxf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return SUCCESSFUL_RETURN;
	}// if ( (nLineHasBlackPixelsBehindBoundaryf == 0 || iWidf ==  sColor_Imagef->nWidth - 1) && nWid_BlackPixelsBehindBoundaryMinf != -1 && nWid_BlackPixelsBehindBoundaryMaxf != -1)

	return (-2);
}//int RangeOfWidthsWithBlackPixelsBehindBoundary_WithBoundaryAdjust(...

//////////////////////////////////////////////////////////
void MedianFiterTo_ObjectArea_InAMaskImage(

	const int nNumOfIters_ForMedianFiterTo_ObjectAreaMaxf,
	int &nNumOfPixels_ShadedToWhiteTotf,

	COLOR_IMAGE *sColor_Imagef)
{
	void DoesABlackPixel_HaveAWhiteNeighbor(
		const int nWidOfPixelf,
		const int nLenOfPixelf,

		const COLOR_IMAGE *sColor_Imagef,

		int &nYesOrNo_ForAWhiteNeighboringPixelf); // 1 -- Yes, 0 -- No

	void MedianFiterTo_APixel_InAMaskImage(
		const int nWidOfPixelf,
		const int nLenOfPixelf,

		int &nPixelShadedToWhiteOrBlackf,
		COLOR_IMAGE *sColor_Imagef);

	int
		nIndexOfPixelCurf,
		nPixelShadedToWhiteOrBlackf,
		nRedCurf,

		nLenOfObjectBoundaryCurf,

		nYesOrNo_ForAWhiteNeighboringPixelf,

		iIterForMedianFiterTo_ObjectAreaf,

		iWidf,
		iLenf;
///////////////////////////////////////////////

	for (iIterForMedianFiterTo_ObjectAreaf = 0; iIterForMedianFiterTo_ObjectAreaf < nNumOfIters_ForMedianFiterTo_ObjectAreaMaxf; iIterForMedianFiterTo_ObjectAreaf++)
	{
		nNumOfPixels_ShadedToWhiteTotf = 0;

		//object to the left   
		if (sColor_Imagef->nSideOfObjectLocation == -1)
		{
			for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
			{
				//start counting from the right
				nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'MedianFiterTo_ObjectArea_InAMaskImage' (object to the left): nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				/////////////////////////////////////
				for (iLenf = 0; iLenf < nLenOfObjectBoundaryCurf; iLenf++)
				{
					nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);
					nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

					if (nRedCurf != nIntensityStatMax) //sColor_Imagef->nIntensityOfBackground_Red)
					{
						DoesABlackPixel_HaveAWhiteNeighbor(
							iWidf, //const int nWidOfPixelf,
							iLenf, //const int nLenOfPixelf,

							sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef,

							nYesOrNo_ForAWhiteNeighboringPixelf); // int &nYesOrNo_ForAWhiteNeighboringPixelf); // 1 -- Yes, 0 -- No

						if (nYesOrNo_ForAWhiteNeighboringPixelf == 0) //No
						{
							continue;
						}// if (nYesOrNo_ForAWhiteNeighboringPixelf == 0)

	//applying only to black pixels behind the boundary
						MedianFiterTo_APixel_InAMaskImage(
							iWidf, //const int nWidOfPixelf,
							iLenf, //const int nLenOfPixelf,

							nPixelShadedToWhiteOrBlackf, //int &nPixelShadedToWhiteOrBlackf,
							sColor_Imagef); // COLOR_IMAGE *sColor_Imagef);

						if (nPixelShadedToWhiteOrBlackf == 1) // || nPixelShadedToWhiteOrBlackf == -1)
						{
							nNumOfPixels_ShadedToWhiteTotf += 1;
						}//if (nPixelShadedToWhiteOrBlackf == 1) // || nPixelShadedToWhiteOrBlackf == -1)

					} // if (nRedCurf != nIntensityStatMax) 

				} //for (iLenf = 0; iLenf < nLenOfObjectBoundaryCurf; iLenf++)

			} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		} // if (sColor_Imagef->nSideOfObjectLocation == -1)

		  //object to the right
		if (sColor_Imagef->nSideOfObjectLocation == 1)
		{
			for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
			{
				//start counting from the left
				nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'MedianFiterTo_ObjectArea_InAMaskImage' (object to the right): nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				for (iLenf = sColor_Imagef->nLength - 1; iLenf > nLenOfObjectBoundaryCurf; iLenf--)
				{
					nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);
					nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

					if (nRedCurf != nIntensityStatMax) //sColor_Imagef->nIntensityOfBackground_Red)
					{
						DoesABlackPixel_HaveAWhiteNeighbor(
							iWidf, //const int nWidOfPixelf,
							iLenf, //const int nLenOfPixelf,

							sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef,

							nYesOrNo_ForAWhiteNeighboringPixelf); // int &nYesOrNo_ForAWhiteNeighboringPixelf); // 1 -- Yes, 0 -- No

						if (nYesOrNo_ForAWhiteNeighboringPixelf == 0) //No
						{
							continue;
						}// if (nYesOrNo_ForAWhiteNeighboringPixelf == 0)

						//applying only to black pixels behind the boundary
						MedianFiterTo_APixel_InAMaskImage(
							iWidf, //const int nWidOfPixelf,
							iLenf, //const int nLenOfPixelf,

							nPixelShadedToWhiteOrBlackf, //int &nPixelShadedToWhiteOrBlackf,
							sColor_Imagef); // COLOR_IMAGE *sColor_Imagef);

						if (nPixelShadedToWhiteOrBlackf == 1) // || nPixelShadedToWhiteOrBlackf == -1)
						{
							nNumOfPixels_ShadedToWhiteTotf += 1;
						}//if (nPixelShadedToWhiteOrBlackf == 1) // || nPixelShadedToWhiteOrBlackf == -1)

					} // if (nRedCurf != nIntensityStatMax) 

				} //for (iLenf = sColor_Imagef->nLength - 1; iLenf > nLenOfObjectBoundaryCurf; iLenf--)

			} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		} // if (sColor_Imagef->nSideOfObjectLocation == 1)

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'MedianFiterTo_ObjectArea_InAMaskImage': the end of iIterForMedianFiterTo_ObjectAreaf = %d, nNumOfPixels_ShadedToWhiteTotf = %d", 
			iIterForMedianFiterTo_ObjectAreaf,nNumOfPixels_ShadedToWhiteTotf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		if (nNumOfPixels_ShadedToWhiteTotf == 0)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'MedianFiterTo_ObjectArea_InAMaskImage': the end of all iters, nNumOfPixels_ShadedToWhiteTotf = %d",
				nNumOfPixels_ShadedToWhiteTotf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			break;
		}// if (nNumOfPixels_ShadedToWhiteTotf == 0)
	} // for (iIterForMedianFiterTo_ObjectAreaf = 0; iIterForMedianFiterTo_ObjectAreaf < nNumOfIters_ForMedianFiterTo_ObjectAreaMaxf; iIterForMedianFiterTo_ObjectAreaf++)

}//void MedianFiterTo_ObjectArea_InAMaskImage(...
///////////////////////////////////////////////////////////////////////

void MedianFiterTo_BackgroundArea_InAMaskImage(

	int &nNumOfPixels_ShadedToBlackTotf,

	COLOR_IMAGE *sColor_Imagef)
{
	void MedianFiterTo_APixel_InAMaskImage(
		const int nWidOfPixelf,
		const int nLenOfPixelf,

		int &nPixelShadedToWhiteOrBlackf,
		COLOR_IMAGE *sColor_Imagef);

	int
		nIndexOfPixelCurf,
		nPixelShadedToWhiteOrBlackf,
		nRedCurf,

		nLenOfObjectBoundaryCurf,

		iWidf,
		iLenf;

	nNumOfPixels_ShadedToBlackTotf = 0;

	//object to the left   
	if (sColor_Imagef->nSideOfObjectLocation == -1)
	{
		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			//start counting from the right
			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'MedianFiterTo_BackgroundArea_InAMaskImage' (object to the left): nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
				nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			/////////////////////////////////////
			//for (iLenf = 0; iLenf < nLenOfObjectBoundaryCurf; iLenf++)
			for (iLenf = sColor_Imagef->nLength - 1; iLenf > nLenOfObjectBoundaryCurf; iLenf--)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);
				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

				if (nRedCurf != nIntensityStatBlack) //sColor_Imagef->nIntensityOfBackground_Red)
				{
					//applying only to black pixels behind the boundary
					MedianFiterTo_APixel_InAMaskImage(
						iWidf, //const int nWidOfPixelf,
						iLenf, //const int nLenOfPixelf,

						nPixelShadedToWhiteOrBlackf, //int &nPixelShadedToWhiteOrBlackf,
						sColor_Imagef); // COLOR_IMAGE *sColor_Imagef);

					if (nPixelShadedToWhiteOrBlackf == -1) // || nPixelShadedToWhiteOrBlackf == -1)
					{
						nNumOfPixels_ShadedToBlackTotf += 1;
					}//if (nPixelShadedToWhiteOrBlackf == -1) // || nPixelShadedToWhiteOrBlackf == -1)

				} // if (nRedCurf != nIntensityStatBlack) 

			} //for (iLenf = sColor_Imagef->nLength - 1; iLenf > nLenOfObjectBoundaryCurf; iLenf--)

		} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
	} // if (sColor_Imagef->nSideOfObjectLocation == -1)

///////////////////////////////////////////////////////////////
	  //object to the right
	if (sColor_Imagef->nSideOfObjectLocation == 1)
	{
		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			//start counting from the left
			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'MedianFiterTo_BackgroundArea_InAMaskImage' (object to the right): nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
				nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			for (iLenf = 0; iLenf < nLenOfObjectBoundaryCurf; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);
				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

				if (nRedCurf != nIntensityStatBlack) //sColor_Imagef->nIntensityOfBackground_Red)
				{
					//applying only to black pixels behind the boundary
					MedianFiterTo_APixel_InAMaskImage(
						iWidf, //const int nWidOfPixelf,
						iLenf, //const int nLenOfPixelf,

						nPixelShadedToWhiteOrBlackf, //int &nPixelShadedToWhiteOrBlackf,
						sColor_Imagef); // COLOR_IMAGE *sColor_Imagef);

					if (nPixelShadedToWhiteOrBlackf == -1) // || nPixelShadedToWhiteOrBlackf == -1)
					{
						nNumOfPixels_ShadedToBlackTotf += 1;
					}//if (nPixelShadedToWhiteOrBlackf == -1) // || nPixelShadedToWhiteOrBlackf == -1)

				} // iif (nRedCurf != nIntensityStatBlack

			} //for (iLenf = 0; iLenf < nLenOfObjectBoundaryCurf; iLenf++)

		} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
	} // if (sColor_Imagef->nSideOfObjectLocation == 1)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'MedianFiterTo_BackgroundArea_InAMaskImage': nNumOfPixels_ShadedToBlackTotf = %d", nNumOfPixels_ShadedToBlackTotf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
}//void MedianFiterTo_BackgroundArea_InAMaskImage(...
////////////////////////////////////////////////////////////////////////

int Smoothing_Boundary_Jaggedness_InAMaskImage(

	const int nBoundaryJumpMaxf,
	const int nWidOfJaggedSegmentMaxf,

	const int nNumOfIters_ForShadingBondaryPixelsMaxf,

	const float fCoefDist_ForNippleToTopf,
	const float fCoefDist_ForNippleToBottomf,
//////////////////////////////
	int &nNumOfJaggedSegmentsTotf,

	COLOR_IMAGE *sColor_Imagef)
{
	void Finding_Next_JaggedBoundary_Segment_InAMaskImage(

		const int nBoundaryJumpMaxf,
		const int nWidOfJaggedSegmentMaxf,

		const int nWidToStartf,

		const float fCoefDist_ForNippleToTopf,
		const float fCoefDist_ForNippleToBottomf,

		int &nWidForSmoothingUpf,
		int &nWidForSmoothingDownf,

		COLOR_IMAGE *sColor_Imagef);

	int Smoothing_A_JaggedBoundarySegment_WithBoundaryAdjusment(

		const int nWidForSmoothingUpf,
		const int nWidForSmoothingDownf, //>= nWidForSmoothingUpf

		const int nNumOfIters_ForShadingBondaryPixelsMaxf,

		int &nNumOfIters_ForPixels_ShadingToBlackTotf,
		COLOR_IMAGE *sColor_Imagef);

	int
		nResf,
		nIndexOfPixelCurf,
		nPixelShadedToWhiteOrBlackf,
		nRedCurf,

	//	nLenOfObjectBoundaryCurf,

		iWidf,
		iLenf,
		
		nWidToStartf,

		nNumOfIters_ForPixels_ShadingToBlackTotf, 
		nBoundaryJumpCurf,

		nWidForSmoothingUpf,
		nWidForSmoothingDownf; // > nWidForSmoothingUpf
	///////////////////////////////////////////////////////
	nNumOfJaggedSegmentsTotf = 0;

	//object to the left   
	if (sColor_Imagef->nSideOfObjectLocation == -1)
	{
		nWidToStartf = 0; //initially

MarkLeft:		Finding_Next_JaggedBoundary_Segment_InAMaskImage(

			nBoundaryJumpMaxf, //const int nBoundaryJumpMaxf,
				nWidOfJaggedSegmentMaxf, //const int nWidOfJaggedSegmentMaxf,

			nWidToStartf, //const int nWidToStartf,

			fCoefDist_ForNippleToTopf, //const float fCoefDist_ForNippleToTopf,
			fCoefDist_ForNippleToBottomf, //const float fCoefDist_ForNippleToBottomf,

			nWidForSmoothingUpf, //int &nWidForSmoothingUpf,
			nWidForSmoothingDownf, //int &nWidForSmoothingDownf,

			sColor_Imagef); // COLOR_IMAGE *sColor_Imagef);

		if (nWidForSmoothingUpf == -1) //no jagged segment is found
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Smoothing_Boundary_Jagg...' (left): exiting, nNumOfJaggedSegmentsTotf = %d",nNumOfJaggedSegmentsTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		}//if (nWidForSmoothingUpf == -1)
		else //if (nWidForSmoothingUpf != -1) //there are jagged segment found
		{
			nNumOfJaggedSegmentsTotf += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Smoothing_Boundary_Jagg...' (left): a new nNumOfJaggedSegmentsTotf = %d, nWidForSmoothingUpf = %d, nWidForSmoothingDownf = %d", 
				nNumOfJaggedSegmentsTotf, nWidForSmoothingUpf, nWidForSmoothingDownf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			nResf = Smoothing_A_JaggedBoundarySegment_WithBoundaryAdjusment(

				nWidForSmoothingUpf, //const int nWidForSmoothingUpf,
				nWidForSmoothingDownf, //const int nWidForSmoothingDownf, //>= nWidForSmoothingUpf

				nNumOfIters_ForShadingBondaryPixelsMaxf, //const int nNumOfIters_ForShadingBondaryPixelsMaxf,

				nNumOfIters_ForPixels_ShadingToBlackTotf, //int &nNumOfIters_ForPixels_ShadingToBlackTotf,
				sColor_Imagef); // COLOR_IMAGE *sColor_Imagef);

			if (nResf == UNSUCCESSFUL_RETURN)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'Smoothing_Boundary_Jagg...' (left): nNumOfJaggedSegmentsTotf = %d", nNumOfJaggedSegmentsTotf);
				fprintf(fout_lr, "\n\n  An error in 'Smoothing_Boundary_Jagg...' (left): nNumOfJaggedSegmentsTotf = %d", nNumOfJaggedSegmentsTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS		

				return UNSUCCESSFUL_RETURN;
			} // if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n The end of 'Smoothing_Boundary_Jagg...' (left): nNumOfJaggedSegmentsTotf = %d has been smoothed", nNumOfJaggedSegmentsTotf);
			printf( "\n nNumOfIters_ForPixels_ShadingToBlackTotf = %d", nNumOfIters_ForPixels_ShadingToBlackTotf);

			fprintf(fout_lr, "\n\n The end of 'Smoothing_Boundary_Jagg...' (left): nNumOfJaggedSegmentsTotf = %d has been smoothed", nNumOfJaggedSegmentsTotf);
			fprintf(fout_lr, "\n nNumOfIters_ForPixels_ShadingToBlackTotf = %d", nNumOfIters_ForPixels_ShadingToBlackTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS		
			
			nWidToStartf = nWidForSmoothingDownf + 1;

			if (nWidToStartf >= sColor_Imagef->nWidth - 1)
			{
				goto MarkEndOfSmoothing_Boundary_Jaggedness;
			}//if (nWidToStartf >= sColor_Imagef->nWidth - 1)

			goto MarkLeft;
		}//else //if (nWidForSmoothingUpf != -1) //there are jagged segment found

	} // if (sColor_Imagef->nSideOfObjectLocation == -1)

///////////////////////////////////////////////////////////////
	  //object to the right
	if (sColor_Imagef->nSideOfObjectLocation == 1)
	{
		nWidToStartf = 0; //initially

	MarkRight:		Finding_Next_JaggedBoundary_Segment_InAMaskImage(

		nBoundaryJumpMaxf, //const int nBoundaryJumpMaxf,
			nWidOfJaggedSegmentMaxf, //const int nWidOfJaggedSegmentMaxf,

		nWidToStartf, //const int nWidToStartf,

		fCoefDist_ForNippleToTopf, //const float fCoefDist_ForNippleToTopf,
		fCoefDist_ForNippleToBottomf, //const float fCoefDist_ForNippleToBottomf,

		nWidForSmoothingUpf, //int &nWidForSmoothingUpf,
		nWidForSmoothingDownf, //int &nWidForSmoothingDownf,

		sColor_Imagef); // COLOR_IMAGE *sColor_Imagef);

		if (nWidForSmoothingUpf == -1) //no jagged segment is found
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Smoothing_Boundary_Jagg...' (right): exiting, nNumOfJaggedSegmentsTotf = %d", nNumOfJaggedSegmentsTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		}//if (nWidForSmoothingUpf == -1)
		else //if (nWidForSmoothingUpf != -1) //there are jagged segment found
		{
			nNumOfJaggedSegmentsTotf += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Smoothing_Boundary_Jagg...' (right): a new nNumOfJaggedSegmentsTotf = %d, nWidForSmoothingUpf = %d, nWidForSmoothingDownf = %d",
				nNumOfJaggedSegmentsTotf, nWidForSmoothingUpf, nWidForSmoothingDownf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			nResf = Smoothing_A_JaggedBoundarySegment_WithBoundaryAdjusment(

				nWidForSmoothingUpf, //const int nWidForSmoothingUpf,
				nWidForSmoothingDownf, //const int nWidForSmoothingDownf, //>= nWidForSmoothingUpf

				nNumOfIters_ForShadingBondaryPixelsMaxf, //const int nNumOfIters_ForShadingBondaryPixelsMaxf,

				nNumOfIters_ForPixels_ShadingToBlackTotf, //int &nNumOfIters_ForPixels_ShadingToBlackTotf,
				sColor_Imagef); // COLOR_IMAGE *sColor_Imagef);

			if (nResf == UNSUCCESSFUL_RETURN)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'Smoothing_Boundary_Jagg...' (right): nNumOfJaggedSegmentsTotf = %d", nNumOfJaggedSegmentsTotf);
				fprintf(fout_lr, "\n\n  An error in 'Smoothing_Boundary_Jagg...' (right): nNumOfJaggedSegmentsTotf = %d", nNumOfJaggedSegmentsTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS		

				return UNSUCCESSFUL_RETURN;
			} // if (nResf == UNSUCCESSFUL_RETUR

#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n The end of 'Smoothing_Boundary_Jagg...' (right): nNumOfJaggedSegmentsTotf = %d has/have been smoothed", nNumOfJaggedSegmentsTotf);
			printf("\n nNumOfIters_ForPixels_ShadingToBlackTotf = %d", nNumOfIters_ForPixels_ShadingToBlackTotf);

			fprintf(fout_lr, "\n\n The end of 'Smoothing_Boundary_Jagg...' (right): nNumOfJaggedSegmentsTotf = %d has/have been smoothed", nNumOfJaggedSegmentsTotf);
			fprintf(fout_lr, "\n nNumOfIters_ForPixels_ShadingToBlackTotf = %d", nNumOfIters_ForPixels_ShadingToBlackTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS		

			nWidToStartf = nWidForSmoothingDownf + 1;

			if (nWidToStartf >= sColor_Imagef->nWidth - 1)
			{
				goto MarkEndOfSmoothing_Boundary_Jaggedness;
			}//if (nWidToStartf >= sColor_Imagef->nWidth -

			goto MarkRight;
		}//else //if (nWidForSmoothingUpf != -1) //there are jagged segment found

#ifndef COMMENT_OUT_ALL_PRINTS	
		fprintf(fout_lr, "\n\n 'Smoothing_Boundary_Jagg... (right): the boundaries'");
		
		for (iWidf = nWidSubimageToPrintMin; iWidf < nWidSubimageToPrintMax; iWidf++)
		{
			fprintf(fout_lr, "\n sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d\n", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);

			for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength);
				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

				fprintf(fout_lr, "%d:%d, ", iLenf, nRedCurf);
			} //for (iLenf = 0; iLenf < sColor_Imagef->nLength; iLenf++)

		} //for (iWidf = nWidSubimageToPrintMin; iWidf < nWidSubimageToPrintMax; iWidf++)

	} // if (sColor_Imagef->nSideOfObjectLocation == 1)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

MarkEndOfSmoothing_Boundary_Jaggedness:	return SUCCESSFUL_RETURN;
}//int Smoothing_Boundary_Jaggedness_InAMaskImage(...
/////////////////////////////////////////////////////////////

void Finding_Next_JaggedBoundary_Segment_InAMaskImage(

	const int nBoundaryJumpMaxf,
	const int nWidOfJaggedSegmentMaxf,

	const int nWidToStartf,

	const float fCoefDist_ForNippleToTopf,
	const float fCoefDist_ForNippleToBottomf,
	////////////////////////////////
	int &nWidForSmoothingUpf,
	int &nWidForSmoothingDownf,

	COLOR_IMAGE *sColor_Imagef)
{
	void MedianFiterTo_APixel_InAMaskImage(
		const int nWidOfPixelf,
		const int nLenOfPixelf,

		int &nPixelShadedToWhiteOrBlackf,
		COLOR_IMAGE *sColor_Imagef);

	int
		iWidf,

		iWid_1f,

		nWidMax_Tempf,
		nTempf,
		nLenOfObjectBoundaryCurf,

		nBoundaryClosestToEdgeInASegmentf = nLarge,
		nWidToTopMaxf,
		nWidToBottomMinf,

		nBoundaryJumpCurf;
//////////////////////////////

	nWidToTopMaxf = (int)(fCoefDist_ForNippleToTopf*(float)(sColor_Imagef->nWidth)); // 0.3*
	nWidToBottomMinf = sColor_Imagef->nWidth - (int)(fCoefDist_ForNippleToBottomf*(float)(sColor_Imagef->nWidth));

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'Finding_Next_Jagged...': nWidToTopMaxf = %d, nWidToBottomMinf = %d, nWidToStartf = %d", 
		nWidToTopMaxf, nWidToBottomMinf, nWidToStartf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//no jagged segment has been found yet
	nWidForSmoothingUpf = -1; //
	nWidForSmoothingDownf = -1;

	//object to the left   
	if (sColor_Imagef->nSideOfObjectLocation == -1)
	{
//looking for 'nWidForSmoothingUpf' first

		for (iWidf = nWidToStartf + 1; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			//if (sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] == 0)
			if (sColor_Imagef->nLenObjectBoundary_Arr[iWidf] == 0 && nWidForSmoothingUpf == -1)
			{

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'Finding_Next_Jagged...' (left) 1: skipping iWidf = %d by the last black row", iWidf);
				fprintf(fout_lr, "\n sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d,  sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d",
					iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], iWidf - 1, sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1]);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				continue;
			}//if (sColor_Imagef->nLenObjectBoundary_Arr[iWidf] == 0 && nWidForSmoothingUpf == -1))

			nBoundaryJumpCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf] - sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1];

			//jumping up for the beginning of the segment
			if (nBoundaryJumpCurf > nBoundaryJumpMaxf)
			{
				if (iWidf > nWidToTopMaxf && iWidf < nWidToBottomMinf)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n 'Finding_Next_Jagged...'  (left): a jump up at iWidf = %d is skipped (close to the nipple area)", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					continue;
				}//if (iWidf > nWidToTopMaxf && iWidf < nWidToBottomMinf)

				nWidForSmoothingUpf = iWidf;

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'Finding_Next_Jagged...'  (left): nWidForSmoothingUpf = %d, nBoundaryJumpCurf = %d", nWidForSmoothingUpf, nBoundaryJumpCurf);

				fprintf(fout_lr, "\n sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d",
					iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], iWidf - 1,sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				break;
			}//if (nBoundaryJumpCurf > nBoundaryJumpMaxf)

		}//for (iWidf = nWidToStartf + 1; iWidf < sColor_Imagef->nWidth; iWidf++)

		if (nWidForSmoothingUpf != -1)
		{
			if (nWidForSmoothingUpf < sColor_Imagef->nWidth - 1)
			{
				nWidMax_Tempf = nWidForSmoothingUpf + nWidOfJaggedSegmentMaxf;
				if (nWidMax_Tempf > sColor_Imagef->nWidth - 1)
					nWidMax_Tempf = sColor_Imagef->nWidth - 1;

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'Finding_Next_Jagged...'  (left): nWidForSmoothingUpf + 1 = %d, nWidMax_Tempf = %d, iWidf = %d", 
					nWidForSmoothingUpf + 1,nWidMax_Tempf, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				//for (iWidf = nWidForSmoothingUpf + 1; iWidf < sColor_Imagef->nWidth; iWidf++)
				for (iWidf = nWidForSmoothingUpf + 1; iWidf <= nWidMax_Tempf; iWidf++)
				{
					nBoundaryJumpCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf] - sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1];

					//jumping down for the end of the segment
					if (nBoundaryJumpCurf < -nBoundaryJumpMaxf)
					{

						if (iWidf > nWidToTopMaxf && iWidf < nWidToBottomMinf)
						{
#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n 'Finding_Next_Jagged...'  (left): a jump down at iWidf = %d is skipped (close to the nipple area)", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							continue;
						}//if (iWidf > nWidToTopMaxf && iWidf < nWidToBottomMinf)

						nWidForSmoothingDownf = iWidf - 1;

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n 'Finding_Next_Jagged...'  (left): nWidForSmoothingDownf = %d, nBoundaryJumpCurf = %d",
							nWidForSmoothingDownf, nBoundaryJumpCurf);
						fprintf(fout_lr, "\n sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d",
							iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], iWidf - 1, sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						goto MarkOfFinding_Next_JaggedBoundary_Segment_InAMaskImage;
					}//if (nBoundaryJumpCurf < -nBoundaryJumpMaxf)

				}//for (iWidf = nWidForSmoothingUpf + 1; iWidf <= nWidMax_Tempf; iWidf++)

			} //if (nWidForSmoothingUpf < sColor_Imagef->nWidth - 1)
			else //if (nWidForSmoothingUpf == sColor_Imagef->nWidth - 1)
			{
				nWidForSmoothingDownf = sColor_Imagef->nWidth - 1;

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'Finding_Next_Jagged...'  (left 1): nWidForSmoothingDownf = sColor_Imagef->nWidth - 1 = %d", nWidForSmoothingDownf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				goto MarkOfFinding_Next_JaggedBoundary_Segment_InAMaskImage;
			} //if (nWidForSmoothingUpf == sColor_Imagef->nWidth - 1)
			
			if (nWidForSmoothingDownf == -1)
			{
				nBoundaryClosestToEdgeInASegmentf = nLarge;

				//'nWidForSmoothingDownf' has not been found by the boundary jump  
					//searching for a closest zero boundary for 'nWidForSmoothingUpf'
						//there could be other criteria
				nWidMax_Tempf = nWidForSmoothingUpf + nWidOfJaggedSegmentMaxf;
				if (nWidMax_Tempf > sColor_Imagef->nWidth - 1)
					nWidMax_Tempf = sColor_Imagef->nWidth - 1;

				for (iWid_1f = nWidForSmoothingUpf; iWid_1f < nWidMax_Tempf; iWid_1f++)
				{
					if (iWid_1f > nWidToTopMaxf && iWid_1f < nWidToBottomMinf)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n 'Finding_Next_Jagged...'  (left) 2: iWid_1f = %d is skipped (close to the nipple area)", iWid_1f);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						continue;
					}//if (iWid_1f > nWidToTopMaxf && iWid_1f < nWidToBottomMinf)


					if (sColor_Imagef->nLenObjectBoundary_Arr[iWid_1f] < nBoundaryClosestToEdgeInASegmentf)
					{
						nBoundaryClosestToEdgeInASegmentf = sColor_Imagef->nLenObjectBoundary_Arr[iWid_1f];

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n 'Finding_Next_Jagged...'  (left): a new nBoundaryClosestToEdgeInASegmentf = %d, iWid_1f = %d", nBoundaryClosestToEdgeInASegmentf, iWid_1f);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					} // if (sColor_Imagef->nLenObjectBoundary_Arr[iWid_1f] < nBoundaryClosestToEdgeInASegmentf)

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n Left: sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, current nBoundaryClosestToEdgeInASegmentf = %d",
						iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], nBoundaryClosestToEdgeInASegmentf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					if (sColor_Imagef->nLenObjectBoundary_Arr[iWid_1f] == 0)
					{
						nWidForSmoothingDownf = iWid_1f - 1;

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n 'Finding_Next_Jagged...' (left): nWidForSmoothingDownf = %d by reaching the edge, nWidMax_Tempf = %d",
							nWidForSmoothingDownf, nWidMax_Tempf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						goto MarkOfFinding_Next_JaggedBoundary_Segment_InAMaskImage;
						//break;
					} //if (sColor_Imagef->nLenObjectBoundary_Arr[iWid_1f] == 0)

				}//for (iWid_1f = nWidForSmoothingUpf; iWid_1f < nWidMax_Tempf; iWid_1f++)

				if (nWidForSmoothingDownf == -1)
				{
					//zero boundary has not been found -- a jagged segment (with the specified parameters) does not exist

					nWidForSmoothingUpf = -1;
					goto MarkOfFinding_Next_JaggedBoundary_Segment_InAMaskImage;
				} //if (nWidForSmoothingDownf == -1)

			} //if (nWidForSmoothingDownf == -1)
////////////////////////////////////////////////////////////////

		}//if (nWidForSmoothingUpf != -1)
////////////////////////////////////////////////////////////
		else //if (nWidForSmoothingUpf == -1)
		{
			// 'nWidForSmoothingUpf' has not been found -- a jagged segment (with the specified parameters) does not exist
			goto MarkOfFinding_Next_JaggedBoundary_Segment_InAMaskImage;

		} //else //if (nWidForSmoothingUpf == -1)

	} //if (sColor_Imagef->nSideOfObjectLocation == -1)

///////////////////////////////////////////////////////////////
		//object to the right   
	if (sColor_Imagef->nSideOfObjectLocation == 1)
	{

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'Finding_Next_Jagged...' (right): nWidToStartf = %d, sColor_Imagef->nWidth = %d",nWidToStartf, sColor_Imagef->nWidth);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		//looking for 'nWidForSmoothingUpf' first
		for (iWidf = nWidToStartf + 1; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			//if (sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] == sColor_Imagef->nLength - 1)
			if (sColor_Imagef->nLenObjectBoundary_Arr[iWidf] == sColor_Imagef->nLength - 1 && nWidForSmoothingUpf == -1)
			{

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'Finding_Next_Jagged...' (right) 1: skipping iWidf = %d by the last black row", iWidf);
				fprintf(fout_lr, "\n sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d,  sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d",
					iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], iWidf - 1, sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1]);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				continue;
			}//if (sColor_Imagef->nLenObjectBoundary_Arr[iWidf] == sColor_Imagef->nLength - 1 && nWidForSmoothingUpf == -1))

			nBoundaryJumpCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf] - sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1];

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Finding_Next_Jagged...' (right): iWidf = %d, nBoundaryJumpCurf = %d", iWidf, nBoundaryJumpCurf);
			fprintf(fout_lr, "\n sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d,  sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d",
				iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], iWidf - 1,sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1]);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			//jumping down for the beginning of the segment of the right object
			if (nBoundaryJumpCurf < -nBoundaryJumpMaxf)
			{
				if (iWidf > nWidToTopMaxf && iWidf < nWidToBottomMinf)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n 'Finding_Next_Jagged...'  (right): iWidf = %d is skipped (close to the nipple area)", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					continue;
				}//if (iWidf > nWidToTopMaxf && iWidf < nWidToBottomMinf)

				nWidForSmoothingUpf = iWidf;

#ifndef COMMENT_OUT_ALL_PRINTS

				fprintf(fout_lr, "\n\n 'Finding_Next_Jagged...'  (right): nWidForSmoothingUpf = %d, nBoundaryJumpCurf = %d",
					nWidForSmoothingUpf, nBoundaryJumpCurf);
				fprintf(fout_lr, "\n sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d",
					iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], iWidf - 1, sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				break;
			}//if (nBoundaryJumpCurf < -nBoundaryJumpMaxf)

		}//for (iWidf = nWidToStartf + 1; iWidf < sColor_Imagef->nWidth; iWidf++)

		if (nWidForSmoothingUpf != -1)
		{
			if (nWidForSmoothingUpf < sColor_Imagef->nWidth - 1)
			{
				nWidMax_Tempf = nWidForSmoothingUpf + nWidOfJaggedSegmentMaxf;
				if (nWidMax_Tempf > sColor_Imagef->nWidth - 1)
					nWidMax_Tempf = sColor_Imagef->nWidth - 1;

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'Finding_Next_Jagged...'  (right): nWidForSmoothingUpf + 1 = %d, nWidMax_Tempf = %d, iWidf = %d", nWidForSmoothingUpf + 1,nWidMax_Tempf, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				//for (iWidf = nWidForSmoothingUpf + 1; iWidf < sColor_Imagef->nWidth; iWidf++)
				for (iWidf = nWidForSmoothingUpf + 1; iWidf <= nWidMax_Tempf; iWidf++)
				{
					nBoundaryJumpCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf] - sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1];

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n Right 1: nBoundaryJumpCurf = %d, sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d",
						nBoundaryJumpCurf,iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], iWidf - 1, sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					//jumping up for the end of the segment of the right object
					if (nBoundaryJumpCurf > nBoundaryJumpMaxf)
					{
						if (iWidf > nWidToTopMaxf && iWidf < nWidToBottomMinf)
						{
#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n 'Finding_Next_Jagged...'  (right): a jump down at iWidf = %d is skipped (close to the nipple area)", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							continue;
						}//if (iWidf > nWidToTopMaxf && iWidf < nWidToBottomMinf)

						nWidForSmoothingDownf = iWidf - 1;

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n 'Finding_Next_Jagged...'  (right): nWidForSmoothingDownf = %d, nBoundaryJumpCurf = %d",
							nWidForSmoothingDownf, nBoundaryJumpCurf);
						fprintf(fout_lr, "\n sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d",
							iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], iWidf - 1, sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						goto MarkOfFinding_Next_JaggedBoundary_Segment_InAMaskImage;
					}//if (nBoundaryJumpCurf > nBoundaryJumpMaxf)

				}//for (iWidf = nWidForSmoothingUpf + 1; iWidf <= nWidMax_Tempf; iWidf++)

			} //if (nWidForSmoothingUpf < sColor_Imagef->nWidth - 1)
			else if (nWidForSmoothingUpf == sColor_Imagef->nWidth - 1)
			{
				nWidForSmoothingDownf = sColor_Imagef->nWidth - 1;

				goto MarkOfFinding_Next_JaggedBoundary_Segment_InAMaskImage;
			} //else if (nWidForSmoothingUpf == sColor_Imagef->nWidth - 1)

//////////////////////////////////////////////////////////////
			if (nWidForSmoothingDownf == -1)
			{
				nBoundaryClosestToEdgeInASegmentf = nLarge;

				//'nWidForSmoothingDownf' has not been found by the boundary jump  
				//searching for a closest zero boundary for 'nWidForSmoothingUpf'
					//there could be other criteria
				nWidMax_Tempf = nWidForSmoothingUpf + nWidOfJaggedSegmentMaxf;
				if (nWidMax_Tempf > sColor_Imagef->nWidth - 1)
					nWidMax_Tempf = sColor_Imagef->nWidth - 1;

				for (iWid_1f = nWidForSmoothingUpf; iWid_1f <= nWidMax_Tempf; iWid_1f++)
				{
					if (iWid_1f > nWidToTopMaxf && iWid_1f < nWidToBottomMinf)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n 'Finding_Next_Jagged...'  (right) 2: iWid_1f = %d is skipped (close to the nipple area)", iWid_1f);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						continue;
					}//if (iWid_1f > nWidToTopMaxf && iWid_1f < nWidToBottomMinf)

					if (sColor_Imagef->nLength - 1 - sColor_Imagef->nLenObjectBoundary_Arr[iWid_1f] < nBoundaryClosestToEdgeInASegmentf)
					{
						nBoundaryClosestToEdgeInASegmentf = sColor_Imagef->nLength - 1 - sColor_Imagef->nLenObjectBoundary_Arr[iWid_1f];

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n 'Finding_Next_Jagged...'  (right): a new nBoundaryClosestToEdgeInASegmentf = %d, iWid_1f = %d", nBoundaryClosestToEdgeInASegmentf,iWid_1f);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					} // if (sColor_Imagef->nLength - 1 - sColor_Imagef->nLenObjectBoundary_Arr[iWid_1f] < nBoundaryClosestToEdgeInASegmentf)

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n Right: sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, current nBoundaryClosestToEdgeInASegmentf = %d",
						iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf], nBoundaryClosestToEdgeInASegmentf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					if (sColor_Imagef->nLenObjectBoundary_Arr[iWid_1f] == sColor_Imagef->nLength - 1)
					{
						nWidForSmoothingDownf = iWid_1f - 1;

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n 'Finding_Next_Jagged...' (right): nWidForSmoothingDownf = %d by reaching the edge, nWidMax_Tempf = %d", 
							nWidForSmoothingDownf, nWidMax_Tempf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						goto MarkOfFinding_Next_JaggedBoundary_Segment_InAMaskImage;
					//break;
					} //if (sColor_Imagef->nLenObjectBoundary_Arr[iWid_1f] == sColor_Imagef->nLength - 1)

				} //for (iWid_1f = nWidForSmoothingUpf; iWid_1f < nWidMax_Tempf; iWid_1f++)

				if (nWidForSmoothingDownf == -1)
				{
					//zero boundary has not been found -- a jagged segment (with the specified parameters) does not exist

					nWidForSmoothingUpf = -1;
					goto MarkOfFinding_Next_JaggedBoundary_Segment_InAMaskImage;
				} //if (nWidForSmoothingDownf == -1)

			} // if (nWidForSmoothingDownf == -1)

		}//if (nWidForSmoothingUpf != -1)

///////////////////////////////////////////////////////////////
		else //if (nWidForSmoothingUpf == -1)
		{
			// 'nWidForSmoothingUpf' has not been found -- a jagged segment (with the specified parameters) does not exist
			goto MarkOfFinding_Next_JaggedBoundary_Segment_InAMaskImage;

		} //else //if (nWidForSmoothingUpf == -1)

	} //if (sColor_Imagef->nSideOfObjectLocation == 1) //right


MarkOfFinding_Next_JaggedBoundary_Segment_InAMaskImage: nTempf = 0;
} // void Finding_Next_JaggedBoundary_Segment_InAMaskImage(...
//////////////////////////////////////////////////////////////////////

int Smoothing_A_JaggedBoundarySegment_WithBoundaryAdjusment(

	const int nWidForSmoothingUpf,
	const int nWidForSmoothingDownf, //>= nWidForSmoothingUpf

	const int nNumOfIters_ForShadingBondaryPixelsMaxf,

	int &nNumOfIters_ForPixels_ShadingToBlackTotf,
	COLOR_IMAGE *sColor_Imagef)
{
	/////////////////////////////////////////////////////////////////////////
	int Density_Aver_OfAnArea_LimitedByBoundaries(
		const int nWidMinf,
		const int nWidMaxf,//inclusive

		const COLOR_IMAGE *sColor_Imagef,

		int &nDensityAverf);

	void MedianFiterTo_APixel_InAMaskImage(
		const int nWidOfPixelf,
		const int nLenOfPixelf,

		int &nPixelShadedToWhiteOrBlackf,
		COLOR_IMAGE *sColor_Imagef);

	int
		nResf,
		nDensityAverf,

		iWidf,

		nWidCurf,
		iLenf,

		nLenOfStraightApproximationCurf,
		nWidOfStraightApproximationCurf,

		nRedCurf,
		nTempf,
//does not belong to the segment
		nWidOfBoundaryPixelAboveSegmentf,
		nLenOfBoundaryPixelAboveSegmentf,

		nWidOfBoundaryPixelBelowSegmentf, //>= nWidOfBoundaryPixelAboveSegmentf
		nLenOfBoundaryPixelBelowSegmentf,

		nWidOfJaggedBoundarySegmentf,

		nLenOfObjectBoundaryCurf,

		nImageWidthf = sColor_Imagef->nWidth,
		nImageLengthf = sColor_Imagef->nLength,

		nRisef,
		nRunf,

		nPixelShadedToWhiteOrBlackf,

		nNumOfPixels_ShadedToBlackTotf = 0,

		iIterForShadingBondaryPixelsf,

		nNumOfPixels_ShadedToBlack_InOneIterf = 0,

		nDiffOfApproximatedAndRealBoundaryLensf,
		nIndexOfPixelCurf, // = iLenf + (iWidf*nImageLengthf);
		nBoundaryJumpCurf; //nBoundaryJumpCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf] - sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1];

	float
		fSlopef;
	////////////////////////////////////
	nNumOfIters_ForPixels_ShadingToBlackTotf = 0;
	//////////////////////////////
	if (nWidForSmoothingUpf > 0)
	{
		nWidOfBoundaryPixelAboveSegmentf = nWidForSmoothingUpf - 1;
	} // if (nWidForSmoothingUpf > 0)
	else
	{
		nWidOfBoundaryPixelAboveSegmentf = 0;
	}//else
/////////////////////////
	if (nWidForSmoothingDownf < nImageWidthf - 1)
	{
		nWidOfBoundaryPixelBelowSegmentf = nWidForSmoothingDownf + 1;
	} // if (nWidForSmoothingDownf < nImageWidthf - 1)
	else
	{
		nWidOfBoundaryPixelBelowSegmentf = nImageWidthf - 1;
	}//else

	nLenOfBoundaryPixelAboveSegmentf = sColor_Imagef->nLenObjectBoundary_Arr[nWidOfBoundaryPixelAboveSegmentf];
	nLenOfBoundaryPixelBelowSegmentf = sColor_Imagef->nLenObjectBoundary_Arr[nWidOfBoundaryPixelBelowSegmentf];

	////////////////////////////////
	// + 1 because 'nWidOfBoundaryPixelBelowSegmentf' could be equal to 'nWidOfBoundaryPixelAboveSegmentf'
	nWidOfJaggedBoundarySegmentf = nWidOfBoundaryPixelBelowSegmentf - nWidOfBoundaryPixelAboveSegmentf + 1;
	if (nWidOfJaggedBoundarySegmentf <= 0)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Smoothing_A_JaggedBoun...Adjusment': nWidOfJaggedBoundarySegmentf = %d", nWidOfJaggedBoundarySegmentf);
		fprintf(fout_lr, "\n\n  An error in 'Smoothing_A_JaggedBoun...Adjusment': nWidOfJaggedBoundarySegmentf = %d", nWidOfJaggedBoundarySegmentf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS		

		return UNSUCCESSFUL_RETURN;
	} // if (nLeftBoundaryOfBlackPixelsArrf == nullptr)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'Smoothing_A_JaggedBoun...Adjusment': nWidForSmoothingUpf = %d, nWidForSmoothingDownf = %d", nWidForSmoothingUpf, nWidForSmoothingDownf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
////////////////////////////////////////////////////////////////

	nResf = Density_Aver_OfAnArea_LimitedByBoundaries(
		nWidForSmoothingUpf, //const int nWidMinf,
		nWidForSmoothingDownf, //const int nWidMaxf,//inclusive

		sColor_Imagef, //const COLOR_IMAGE *sColor_Imagef,

		nDensityAverf); // int &nDensityAverf);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		return UNSUCCESSFUL_RETURN;
	} // if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'Smoothing_A_JaggedBoun...Adjusment': nDensityAverf = %d", nDensityAverf);
	fprintf(fout_lr, "\n\n 'Smoothing_A_JaggedBoun...Adjusment': nDensityAverf = %d", nDensityAverf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS		

	///////////////////////////////////////////////////////
//straight approximation of the jagged boundary
	nRisef = nLenOfBoundaryPixelBelowSegmentf - nLenOfBoundaryPixelAboveSegmentf;
	nRunf = nWidOfJaggedBoundarySegmentf; 

	fSlopef = (float)(nRisef) / (float)(nRunf);

	int *nLenOfStraightApproximationOfJaggedBoundaryArrf = new int[nWidOfJaggedBoundarySegmentf];
	////////////////////////////////
	if (nLenOfStraightApproximationOfJaggedBoundaryArrf == nullptr)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Smoothing_A_JaggedBoun...Adjusment': nLenOfStraightApproximationOfJaggedBoundaryArrf == nullptr");
		fprintf(fout_lr, "\n\n An error in 'Smoothing_A_JaggedBoun...Adjusment': nLenOfStraightApproximationOfJaggedBoundaryArrf == nullptr");

		//printf("\n\n Please press any key to exit"); getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS		

		return UNSUCCESSFUL_RETURN;
	} // if (nLeftBoundaryOfBlackPixelsArrf == nullptr)
/////////////////////////////////
	for (iWidf = 0; iWidf < nWidOfJaggedBoundarySegmentf; iWidf++)
	{
		nLenOfStraightApproximationCurf = nLenOfBoundaryPixelAboveSegmentf + (int)(fSlopef * (float)(iWidf));

		nLenOfStraightApproximationOfJaggedBoundaryArrf[iWidf] = nLenOfStraightApproximationCurf; //
	} // for (iWidf = 0; iWidf < nWidOfJaggedBoundarySegmentf; iWidf++)

/////////////////////////////////////////////////////////////	
	//object to the left   
	if (sColor_Imagef->nSideOfObjectLocation == -1)
	{
		for (iIterForShadingBondaryPixelsf = 0; iIterForShadingBondaryPixelsf < nNumOfIters_ForShadingBondaryPixelsMaxf; iIterForShadingBondaryPixelsf++)
		{
			nNumOfPixels_ShadedToBlack_InOneIterf = 0;

			//applying median filter to boundary pixels of jagged segment
			for (iWidf = nWidForSmoothingUpf; iWidf <= nWidForSmoothingDownf; iWidf++)
			{
				nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];
				if (nLenOfObjectBoundaryCurf > 0)
				{
					nIndexOfPixelCurf = nLenOfObjectBoundaryCurf + (iWidf*nImageLengthf);

					nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n 'Smoothing_A_JaggedBoun...Adjusment' (left): iWidf = %d, nLenOfObjectBoundaryCurf = %d", iWidf, nLenOfObjectBoundaryCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					if (nRedCurf == nIntensityStatBlack)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						printf("\n\n An error in 'Smoothing_A_JaggedBoun...Adjusment' (left): a boundary pixel not adjacent to an image edge must be white");
						printf("\n iWidf = %d, nLenOfObjectBoundaryCurf = %d ", iWidf, nLenOfObjectBoundaryCurf);

						fprintf(fout_lr, "\n\n An error in 'Smoothing_A_JaggedBoun...Adjusment' (left): a boundary pixel not adjacent to an image edge must be white");
						fprintf(fout_lr, "\n iWidf = %d, nLenOfObjectBoundaryCurf = %d ", iWidf, nLenOfObjectBoundaryCurf);

						//printf("\n\n Please press any key to exit"); getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS		

						delete[] nLenOfStraightApproximationOfJaggedBoundaryArrf;
						return UNSUCCESSFUL_RETURN;
					} // if (nRedCurf == nIntensityStatBlack)

					MedianFiterTo_APixel_InAMaskImage(
						iWidf, //const int nWidOfPixelf,
						nLenOfObjectBoundaryCurf, //const int nLenOfPixelf,

						nPixelShadedToWhiteOrBlackf, //int &nPixelShadedToWhiteOrBlackf,
						sColor_Imagef); // COLOR_IMAGE *sColor_Imagef);

					if (nPixelShadedToWhiteOrBlackf == -1)
					{
						// the white boundary pixel has been changed to black
						nNumOfPixels_ShadedToBlack_InOneIterf += 1;
						nNumOfPixels_ShadedToBlackTotf += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n 'Smoothing_A_JaggedBoun...Adjusment' (left): a new nNumOfPixels_ShadedToBlack_InOneIterf = %d, iIterForShadingBondaryPixelsf = %d",
							nNumOfPixels_ShadedToBlack_InOneIterf, iIterForShadingBondaryPixelsf);
						fprintf(fout_lr, "\n iWidf = %d, nLenOfObjectBoundaryCurf = %d", iWidf, nLenOfObjectBoundaryCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						//finding out the next boundary (closer to the left image edge)
						for (iLenf = nLenOfObjectBoundaryCurf - 1; iLenf >= 0; iLenf--)
						{
							nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);
							nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

							if (nRedCurf == nIntensityStatMax)
							{
								sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = iLenf;
#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n 'Smoothing_A_JaggedBoun...Adjusment' (left): the next boundary sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d",
									iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								goto MarkTheNext_iWid_Left;
							} // if (nRedCurf == nIntensityStatMax)

						}//for (iLenf = nLenOfObjectBoundaryCurf - 1; iLenf >= 0; iLenf--)

						sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = 0;
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n 'Smoothing_A_JaggedBoun...Adjusment' (left): no white boundary pixel exists for iWidf = %d", iWidf);
						fprintf(fout_lr, "\n sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						continue;
					}// if (nPixelShadedToWhiteOrBlackf == -1)

				} //if (nLenOfObjectBoundaryCurf > 0)

			MarkTheNext_iWid_Left: continue;
			}//for (iWidf = nWidForSmoothingUpf; iWidf <= nWidForSmoothingDownf; iWidf++)

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Smoothing_A_JaggedBoun...Adjusment' (left): the end of iIterForShadingBondaryPixelsf = %d",	iIterForShadingBondaryPixelsf);
			fprintf(fout_lr, "\n nNumOfPixels_ShadedToBlack_InOneIterf = %d, nNumOfPixels_ShadedToBlackTotf = %d", 
				nNumOfPixels_ShadedToBlack_InOneIterf, nNumOfPixels_ShadedToBlackTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			if (nNumOfPixels_ShadedToBlack_InOneIterf == 0)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'Smoothing_A_JaggedBoun...Adjusment' (left): the end of iterations at iIterForShadingBondaryPixelsf = %d", iIterForShadingBondaryPixelsf);
				fprintf(fout_lr, "\n nNumOfPixels_ShadedToBlack_InOneIterf = %d, nNumOfPixels_ShadedToBlackTotf = %d",
					nNumOfPixels_ShadedToBlack_InOneIterf, nNumOfPixels_ShadedToBlackTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				break;
			} // if (nNumOfPixels_ShadedToBlack_InOneIterf == 0)

			if (iIterForShadingBondaryPixelsf == nNumOfIters_ForShadingBondaryPixelsMaxf - 1)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'Smoothing_A_JaggedBoun...Adjusment' (left): the end by insufficient nNumOfIters_ForShadingBondaryPixelsMaxf = %d", nNumOfIters_ForShadingBondaryPixelsMaxf);
				fprintf(fout_lr, "\n nNumOfPixels_ShadedToBlack_InOneIterf = %d, nNumOfPixels_ShadedToBlackTotf = %d",
					nNumOfPixels_ShadedToBlack_InOneIterf, nNumOfPixels_ShadedToBlackTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			}//if (iIterForShadingBondaryPixelsf == nNumOfIters_ForShadingBondaryPixelsMaxf - 1)

		} //for (iIterForShadingBondaryPixelsf = 0; iIterForShadingBondaryPixelsf < nNumOfIters_ForShadingBondaryPixelsMaxf; iIterForShadingBondaryPixelsf++)

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'Smoothing_A_JaggedBoun...Adjusment' (left): comparing the adjusted boundaries with the linear approximation");

		//nWidOfJaggedBoundarySegmentf
		for (iWidf = nWidForSmoothingUpf; iWidf <= nWidForSmoothingDownf; iWidf++)
		{
			//the approximation starts above 'nWidForSmoothingUpf'
			nWidOfStraightApproximationCurf = iWidf - nWidForSmoothingUpf + 1; //

			if (nWidOfStraightApproximationCurf > nWidOfJaggedBoundarySegmentf - 1)
			{

//#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'Smoothing_A_JaggedBoun...Adjusment' (left): nWidOfStraightApproximationCurf = %d > nWidOfJaggedBoundarySegmentf - 1 = %d, iWidf = %d",
					nWidOfStraightApproximationCurf > nWidOfJaggedBoundarySegmentf - 1, iWidf);

				fprintf(fout_lr, "\n\nAn error in 'Smoothing_A_JaggedBoun...Adjusment' (left): nWidOfStraightApproximationCurf = %d > nWidOfJaggedBoundarySegmentf - 1 = %d, iWidf = %d",
					nWidOfStraightApproximationCurf > nWidOfJaggedBoundarySegmentf - 1, iWidf);

				//printf("\n\n Please press any key to exit"); getchar(); exit(1);
//#endif //#ifndef COMMENT_OUT_ALL_PRINTS		

				delete[] nLenOfStraightApproximationOfJaggedBoundaryArrf;
				return UNSUCCESSFUL_RETURN;
			}//if (nWidOfStraightApproximationCurf > nWidOfJaggedBoundarySegmentf - 1)

			nDiffOfApproximatedAndRealBoundaryLensf = nLenOfStraightApproximationOfJaggedBoundaryArrf[nWidOfStraightApproximationCurf] - sColor_Imagef->nLenObjectBoundary_Arr[iWidf];
			fprintf(fout_lr, "\n sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, nLenOfStraightApproximationOfJaggedBoundaryArrf[%d] = %d, nDiffOfApproximatedAndRealBoundaryLensf = %d",
				iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf],
				nWidOfStraightApproximationCurf, nLenOfStraightApproximationOfJaggedBoundaryArrf[nWidOfStraightApproximationCurf], nDiffOfApproximatedAndRealBoundaryLensf);

		} //for (iWidf = nWidForSmoothingUpf; iWidf <= nWidForSmoothingDownf; iWidf++)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	} //if (sColor_Imagef->nSideOfObjectLocation == -1)
///////////////////////////////////////////////////////////////
		//object to the right   
	if (sColor_Imagef->nSideOfObjectLocation == 1)
	{
		for (iIterForShadingBondaryPixelsf = 0; iIterForShadingBondaryPixelsf < nNumOfIters_ForShadingBondaryPixelsMaxf; iIterForShadingBondaryPixelsf++)
		{
			nNumOfPixels_ShadedToBlack_InOneIterf = 0;

			//applying median filter to boundary pixels of jagged segment
			for (iWidf = nWidForSmoothingUpf; iWidf <= nWidForSmoothingDownf; iWidf++)
			{
				nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];

				//if (nLenOfObjectBoundaryCurf > 0)
				if (nLenOfObjectBoundaryCurf < nImageLengthf - 1)
				{
					nIndexOfPixelCurf = nLenOfObjectBoundaryCurf + (iWidf*nImageLengthf);

					nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n 'Smoothing_A_JaggedBoun...Adjusment' (right): iWidf = %d, nLenOfObjectBoundaryCurf = %d", iWidf, nLenOfObjectBoundaryCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					if (nRedCurf == nIntensityStatBlack)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						printf("\n\n An error in 'Smoothing_A_JaggedBoun...Adjusment' (right): a boundary pixel not adjacent to an image edge must be white");
						printf("\n iWidf = %d, nLenOfObjectBoundaryCurf = %d ", iWidf, nLenOfObjectBoundaryCurf);

						fprintf(fout_lr, "\n\n An error in 'Smoothing_A_JaggedBoun...Adjusment' (right): a boundary pixel not adjacent to an image edge must be white");
						fprintf(fout_lr, "\n iWidf = %d, nLenOfObjectBoundaryCurf = %d ", iWidf, nLenOfObjectBoundaryCurf);

						//printf("\n\n Please press any key to exit"); getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS		

						delete[] nLenOfStraightApproximationOfJaggedBoundaryArrf;
						return UNSUCCESSFUL_RETURN;
					} // if (nRedCurf == nIntensityStatBlack)

					MedianFiterTo_APixel_InAMaskImage(
						iWidf, //const int nWidOfPixelf,
						nLenOfObjectBoundaryCurf, //const int nLenOfPixelf,

						nPixelShadedToWhiteOrBlackf, //int &nPixelShadedToWhiteOrBlackf,
						sColor_Imagef); // COLOR_IMAGE *sColor_Imagef);

					if (nPixelShadedToWhiteOrBlackf == -1)
					{
						// the white boundary pixel has been changed to black
						nNumOfPixels_ShadedToBlack_InOneIterf += 1;
						nNumOfPixels_ShadedToBlackTotf += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n 'Smoothing_A_JaggedBoun...Adjusment' (right): a new nNumOfPixels_ShadedToBlack_InOneIterf = %d, iIterForShadingBondaryPixelsf = %d",
							nNumOfPixels_ShadedToBlack_InOneIterf, iIterForShadingBondaryPixelsf);
						fprintf(fout_lr, "\n iWidf = %d, nLenOfObjectBoundaryCurf = %d", iWidf, nLenOfObjectBoundaryCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						//finding out the next boundary (closer to the right image edge)
						//for (iLenf = nLenOfObjectBoundaryCurf - 1; iLenf >= 0; iLenf--)
						for (iLenf = nLenOfObjectBoundaryCurf + 1; iLenf < nImageLengthf; iLenf++)
						{
							nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);
							nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

							if (nRedCurf == nIntensityStatMax)
							{
								sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = iLenf;
#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n 'Smoothing_A_JaggedBoun...Adjusment' (right): the next boundary sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d",
									iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								goto MarkTheNext_iWid_Right;
							} // if (nRedCurf == nIntensityStatMax)

						}//for (iLenf = nLenOfObjectBoundaryCurf + 1; iLenf < nImageLengthf; iLenf++)

						sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = nImageLengthf - 1;

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n 'Smoothing_A_JaggedBoun...Adjusment' (right): no white boundary pixel exists for iWidf = %d", iWidf);
						fprintf(fout_lr, "\n sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d", iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						continue;
					}// if (nPixelShadedToWhiteOrBlackf == -1)

				} //if (nLenOfObjectBoundaryCurf < nImageLengthf - 1) 

			MarkTheNext_iWid_Right: continue;
			}//for (iWidf = nWidForSmoothingUpf; iWidf <= nWidForSmoothingDownf; iWidf++)

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'Smoothing_A_JaggedBoun...Adjusment' (right): the end of iIterForShadingBondaryPixelsf = %d", iIterForShadingBondaryPixelsf);
			fprintf(fout_lr, "\n nNumOfPixels_ShadedToBlack_InOneIterf = %d, nNumOfPixels_ShadedToBlackTotf = %d",
				nNumOfPixels_ShadedToBlack_InOneIterf, nNumOfPixels_ShadedToBlackTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			if (nNumOfPixels_ShadedToBlack_InOneIterf == 0)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'Smoothing_A_JaggedBoun...Adjusment' (right): the end of iterations at iIterForShadingBondaryPixelsf = %d", iIterForShadingBondaryPixelsf);
				fprintf(fout_lr, "\n nNumOfPixels_ShadedToBlack_InOneIterf = %d, nNumOfPixels_ShadedToBlackTotf = %d",
					nNumOfPixels_ShadedToBlack_InOneIterf, nNumOfPixels_ShadedToBlackTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				break;
			} // if (nNumOfPixels_ShadedToBlack_InOneIterf == 0)

			if (iIterForShadingBondaryPixelsf == nNumOfIters_ForShadingBondaryPixelsMaxf - 1)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'Smoothing_A_JaggedBoun...Adjusment' (right): the end by insufficient nNumOfIters_ForShadingBondaryPixelsMaxf = %d", nNumOfIters_ForShadingBondaryPixelsMaxf);
				fprintf(fout_lr, "\n nNumOfPixels_ShadedToBlack_InOneIterf = %d, nNumOfPixels_ShadedToBlackTotf = %d",
					nNumOfPixels_ShadedToBlack_InOneIterf, nNumOfPixels_ShadedToBlackTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			}//if (iIterForShadingBondaryPixelsf == nNumOfIters_ForShadingBondaryPixelsMaxf - 1)

		} //for (iIterForShadingBondaryPixelsf = 0; iIterForShadingBondaryPixelsf < nNumOfIters_ForShadingBondaryPixelsMaxf; iIterForShadingBondaryPixelsf++)
///////////////////////////////////////////////////
//comparing 'nLenOfStraightApproximationOfJaggedBoundaryArrf[]' and the respective 'sColor_Imagef->nLenObjectBoundary_Arr[] '

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'Smoothing_A_JaggedBoun...Adjusment' (right): comparing the adjusted boundaries with the linear approximation");

		//nWidOfJaggedBoundarySegmentf
		for (iWidf = nWidForSmoothingUpf; iWidf <= nWidForSmoothingDownf; iWidf++)
		{
//the approximation starts above 'nWidForSmoothingUpf'
			nWidOfStraightApproximationCurf = iWidf - nWidForSmoothingUpf + 1; //

			if (nWidOfStraightApproximationCurf > nWidOfJaggedBoundarySegmentf - 1)
			{

//#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'Smoothing_A_JaggedBoun...Adjusment' (right): nWidOfStraightApproximationCurf = %d > nWidOfJaggedBoundarySegmentf - 1 = %d, iWidf = %d",
					nWidOfStraightApproximationCurf > nWidOfJaggedBoundarySegmentf - 1, iWidf);

				fprintf(fout_lr, "\n\nAn error in 'Smoothing_A_JaggedBoun...Adjusment' (right): nWidOfStraightApproximationCurf = %d > nWidOfJaggedBoundarySegmentf - 1 = %d, iWidf = %d",
					nWidOfStraightApproximationCurf > nWidOfJaggedBoundarySegmentf - 1, iWidf);
				
				//printf("\n\n Please press any key to exit"); getchar(); exit(1);
//#endif //#ifndef COMMENT_OUT_ALL_PRINTS		

				delete[] nLenOfStraightApproximationOfJaggedBoundaryArrf;
				return UNSUCCESSFUL_RETURN;
			}//if (nWidOfStraightApproximationCurf > nWidOfJaggedBoundarySegmentf - 1)

			nDiffOfApproximatedAndRealBoundaryLensf = nLenOfStraightApproximationOfJaggedBoundaryArrf[nWidOfStraightApproximationCurf] - sColor_Imagef->nLenObjectBoundary_Arr[iWidf];
			fprintf(fout_lr, "\n sColor_Imagef->nLenObjectBoundary_Arr[%d] = %d, nLenOfStraightApproximationOfJaggedBoundaryArrf[%d] = %d, nDiffOfApproximatedAndRealBoundaryLensf = %d",
				iWidf, sColor_Imagef->nLenObjectBoundary_Arr[iWidf],
				nWidOfStraightApproximationCurf, nLenOfStraightApproximationOfJaggedBoundaryArrf[nWidOfStraightApproximationCurf], nDiffOfApproximatedAndRealBoundaryLensf);

		} //for (iWidf = nWidForSmoothingUpf; iWidf <= nWidForSmoothingDownf; iWidf++)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	} //if (sColor_Imagef->nSideOfObjectLocation == 1)
///////////////////////////////////
//MarkOfSmoothing_A_JaggedBoundarySegment_WithBoundaryAdjusment: nTempf = 0;

	nNumOfIters_ForPixels_ShadingToBlackTotf = iIterForShadingBondaryPixelsf;
	delete[] nLenOfStraightApproximationOfJaggedBoundaryArrf;
	return SUCCESSFUL_RETURN;
} // int Smoothing_A_JaggedBoundarySegment_WithBoundaryAdjusment(...

/////////////////////////////////////////////////////////////////////////
int Density_Aver_OfAnArea_LimitedByBoundaries(
	const int nWidMinf,
	const int nWidMaxf,//inclusive

	const COLOR_IMAGE *sColor_Imagef,
	
	int &nDensityAverf)
{
	int
		nIndexOfPixelCurf,// = iLenf + (iWidf*nImageLengthf);

		nLenOfObjectBoundaryCurf, // = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];
	
		nRedCurf, // = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf]; ,

		nAreaf = 0, // = (nLenMaxf - nLenMinf + 1)*(nWidMaxf - nWidMinf + 1),
		iWidf,
		iLenf;

	float
	
		fSumf = 0.0; //to avoid overfloating in 'nDensityAverf'
///////////////////////////////////////////
	nDensityAverf = 0;

	if (sColor_Imagef->nSideOfObjectLocation == -1) //left
	{
		for (iWidf = nWidMinf; iWidf <= nWidMaxf; iWidf++)
		{
			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];
			if (nLenOfObjectBoundaryCurf > 0)
			{
				for (iLenf = 0; iLenf < nLenOfObjectBoundaryCurf; iLenf++)
				{
					nAreaf += 1;
					nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength);
					nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

					if (nRedCurf < 0)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						printf("\n\n An error in 'Density_Aver...': nRedCurf = %d < 0", nRedCurf);
						printf("\n nWidMinf = %d, nWidMaxf = %d ", nWidMinf, nWidMaxf);

						fprintf(fout_lr, "\n\n An error in 'Density_Aver...': nRedCurf = %d < 0", nRedCurf);
						fprintf(fout_lr, "\n nWidMinf = %d, nWidMaxf = %d ", nWidMinf, nWidMaxf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS		

						return UNSUCCESSFUL_RETURN;
					}//if (nRedCurf < 0)

				//nDensityAverf += nRedCurf;
					fSumf += (float)(nRedCurf);

				} //for (iLenf = 0; iLenf < nLenOfObjectBoundaryCurf; iLenf++)

			} //if (nLenOfObjectBoundaryCurf > 0)

		} //for (iWidf = nWidMinf; iWidf <= nWidMaxf; iWidf++)
	} //if (sColor_Imagef->nSideOfObjectLocation == -1) //left
//////////////////////////////////////////////////////////////////////////
	if (sColor_Imagef->nSideOfObjectLocation == 1) //right
	{
		for (iWidf = nWidMinf; iWidf <= nWidMaxf; iWidf++)
		{
			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];

#ifndef COMMENT_OUT_ALL_PRINTS
	
			fprintf(fout_lr, "\n\n 'Density_Aver...': iWidf = %d, nLenOfObjectBoundaryCurf  = %d", iWidf, nLenOfObjectBoundaryCurf);
			fprintf(fout_lr, "\n nWidMinf = %d, nWidMaxf = %d ", nWidMinf, nWidMaxf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS		

			if (nLenOfObjectBoundaryCurf > 0)
			{
				for (iLenf = sColor_Imagef->nLength - 1; iLenf > nLenOfObjectBoundaryCurf; iLenf--)
				{
					nAreaf += 1;
					nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength);
					nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

#ifndef COMMENT_OUT_ALL_PRINTS
				//	fprintf(fout_lr, "\n'Density_Aver...': iWidf = %d, iLenf = %d, nRedCurf = %d", iWidf, iLenf, nRedCurf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS		

					//nDensityAverf += nRedCurf;
					fSumf += (float)(nRedCurf);
				} //for (iLenf = sColor_Imagef->nLength - 1; iLenf > nLenOfObjectBoundaryCurf; iLenf--)

			} //if (nLenOfObjectBoundaryCurf > 0)

		} //for (iWidf = nWidMinf; iWidf <= nWidMaxf; iWidf++)
	} //if (sColor_Imagef->nSideOfObjectLocation == 1) //right

	if (nAreaf <= 0)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Density_Aver...': nAreaf = %d <= 0", nAreaf);
		printf("\n nWidMinf = %d, nWidMaxf = %d ", nWidMinf, nWidMaxf);

		fprintf(fout_lr, "\n\n An error in 'Density_Aver...': nAreaf = %d <= 0", nAreaf);
		fprintf(fout_lr, "\n nWidMinf = %d, nWidMaxf = %d ", nWidMinf, nWidMaxf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS		

		return UNSUCCESSFUL_RETURN;
	}//if (nAreaf <= 0)

/////////////////////////////////////////////////////
	if (nDensityAverf < 0)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Density_Aver...': nDensityAverf = %d < 0", nDensityAverf);
		printf("\n nWidMinf = %d, nWidMaxf = %d ", nWidMinf, nWidMaxf);

		fprintf(fout_lr, "\n\n An error in 'Density_Aver...': nDensityAverf = %d < 0", nDensityAverf);
		fprintf(fout_lr, "\n nWidMinf = %d, nWidMaxf = %d ", nWidMinf, nWidMaxf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS		

		return UNSUCCESSFUL_RETURN;
	}//if (nDensityAverf < 0)

	//nDensityAverf = nDensityAverf / nAreaf;
	nDensityAverf = (int)(fSumf / (float)(nAreaf) );

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'Density_Aver_OfAnArea_LimitedByBoundaries': nDensityAverf = %d", nDensityAverf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	return SUCCESSFUL_RETURN;

}// void Density_Aver_OfAnArea_LimitedByBoundaries(...
///////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
int FillingOut_BlackObjectPixels_WhiteBlack_Image(
	const int nWidOfABlackAreaMaxf,
	const int nLenOfABlackAreaMaxf,

		const int nDropInBoundary_ForBlackAreaMaxf,

	int &nNumOf_ErasedBlackPixelsTotf,
	COLOR_IMAGE *sColor_Imagef) //[nImageSizeMax]
{
	int
		nIndexOfPixelCurf,
		nIndexOfBlackPixelCurf,

		iWidf,
		iWid_BlackPixelsf,
		iLenf,

		nRedCurf,
		//nGreenCurf,
		//nBlueCurf,

		nWidOfABlackAreaCurf,
		nLenOfABlackAreaCurf,

		nBottomOfBlackPixelsf = -1,
		nTopOfBlackPixelsf = -1,

		nNumOfContinuousRowsWithBlackPixelsTotf = 0,
		//nBottomOfBlackPixels_PrevValidf = -1,
		//nTopOfBlackPixels_PrevValidf = -1,

		nDropInBoundary_ForBlackAreaCurf,
		nNumOfBlackPixelsInALineTotf,
		nLenOfObjectBoundaryCurf;

	int *nLeftBoundaryOfBlackPixelsArrf = new int[sColor_Imagef->nWidth];

	////////////////////////////////
	if (nLeftBoundaryOfBlackPixelsArrf == nullptr)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'FillingOut_Black...' (left): nLeftBoundaryOfBlackPixelsArrf == nullptr");
		fprintf(fout_lr, "\n\n An error in 'FillingOut_Black...' (left): nLeftBoundaryOfBlackPixelsArrf == nullptr");

		printf("\n\n Please press any key to exit"); getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS		

		return UNSUCCESSFUL_RETURN;
	} // if (nLeftBoundaryOfBlackPixelsArrf == nullptr)

	int *nRightBoundaryOfBlackPixelsArrf = new int[sColor_Imagef->nWidth];
	if (nRightBoundaryOfBlackPixelsArrf == nullptr)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'FillingOut_Black...' (left): nRightBoundaryOfBlackPixelsArrf == nullptr");
		fprintf(fout_lr, "\n\n An error in 'FillingOut_Black...' (left): nRightBoundaryOfBlackPixelsArrf == nullptr");

		printf("\n\n Please press any key to exit"); getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS		

		delete[] nLeftBoundaryOfBlackPixelsArrf;
		//delete[] nRightBoundaryOfBlackPixelsArrf;

		return UNSUCCESSFUL_RETURN;
	} // if (nRightBoundaryOfBlackPixelsArrf == nullptr)

	nNumOf_ErasedBlackPixelsTotf = 0;
//////////////////////////////////////////////////////////////
	  //object to the left   
	if (sColor_Imagef->nSideOfObjectLocation == -1)
	{
		//object to the left   
		if (sColor_Imagef->nSideOfObjectLocation == -1)
		{
			nBottomOfBlackPixelsf = -1;
			nTopOfBlackPixelsf = -1;

#ifndef COMMENT_OUT_ALL_PRINTS
				//fprintf(fout_lr, "\n 'FillingOut_Black...' (left) 1: sColor_Imagef->nLenObjectBoundary_Arr[5296] = %d", sColor_Imagef->nLenObjectBoundary_Arr[5296]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			//finding the left and right boundaries of black object pixels 
			for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
			{

				nLeftBoundaryOfBlackPixelsArrf[iWidf] = -1;
				nRightBoundaryOfBlackPixelsArrf[iWidf] = -1;

				nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];

				if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)
				{
					if (nLenOfObjectBoundaryCurf == -1)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n No boundary/erasing in 'FillingOut_Black...' (left): nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
							nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

						fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						continue;
					} //if (nLenOfObjectBoundaryCurf == -1)

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'FillingOut_Black...' (left): nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
						nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
					printf("\n\n An error in 'FillingOut_Black...' (left): nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
						nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

					printf("\n\n Please press any key to exit"); getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

					delete[] nLeftBoundaryOfBlackPixelsArrf;
					delete[] nRightBoundaryOfBlackPixelsArrf;
					return UNSUCCESSFUL_RETURN;
				}// if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'FillingOut_Black...' (left):  iWidf = %d, nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d",
					iWidf,nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1);

				//fprintf(fout_lr, "\n 'FillingOut_Black...' (left) 1: sColor_Imagef->nLenObjectBoundary_Arr[5296] = %d", sColor_Imagef->nLenObjectBoundary_Arr[5296]);
				fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				//start counting from the left
				nNumOfBlackPixelsInALineTotf = 0;
				for (iLenf = 0; iLenf < nLenOfObjectBoundaryCurf; iLenf++)
				{
					nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

					nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

					if (nRedCurf == nIntensityStatBlack)
					{
						//if (sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] == 0)
						{
							nNumOfBlackPixelsInALineTotf += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n The next black pixel found (left): nNumOfBlackPixelsInALineTotf = %d, nRedCurf = %d, iLenf = %d, iWidf = %d",
								nNumOfBlackPixelsInALineTotf, nRedCurf, iLenf, iWidf);
							fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							if (nTopOfBlackPixelsf == -1)
							{
								nTopOfBlackPixelsf = iWidf;
							} //if (nTopOfBlackPixelsf == -1)

							if (nTopOfBlackPixelsf != -1 && nBottomOfBlackPixelsf != iWidf )
							{
								nBottomOfBlackPixelsf = iWidf;
							} //if (nTopOfBlackPixelsf != -1 && ..)

							  //sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = sColor_Imagef->nIntensityOfBackground_Red;
							if (nLeftBoundaryOfBlackPixelsArrf[iWidf] == -1)
							{
								nLeftBoundaryOfBlackPixelsArrf[iWidf] = iLenf;
							} //if (nLeftBoundaryOfBlackPixelsArrf[iWidf] == -1)

							if (nLeftBoundaryOfBlackPixelsArrf[iWidf] != -1)
							{
								nRightBoundaryOfBlackPixelsArrf[iWidf] = iLenf; //inclusive
							} //if (nLeftBoundaryOfBlackPixelsArrf[iWidf] != -1)

						} // if (sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] == 0)
//////////////////////////////////////////////////////////

						nLenOfABlackAreaCurf = nRightBoundaryOfBlackPixelsArrf[iWidf] - nLeftBoundaryOfBlackPixelsArrf[iWidf];

						if (nLenOfABlackAreaCurf > nLenOfABlackAreaMaxf)
						{
							//the length of black area is atypically large -- no erasing of black pixels
								//it could be a balck area between the breast and the image edge

							if (nBottomOfBlackPixelsf != -1 && nTopOfBlackPixelsf != -1)
							{
								//nBottomOfBlackPixels_PrevValidf = nBottomOfBlackPixelsf;
								//nTopOfBlackPixels_PrevValidf = nTopOfBlackPixelsf;

								for (iWid_BlackPixelsf = nTopOfBlackPixelsf; iWid_BlackPixelsf <= nBottomOfBlackPixelsf; iWid_BlackPixelsf++)
								{
									nLeftBoundaryOfBlackPixelsArrf[iWid_BlackPixelsf] = -1;
									nRightBoundaryOfBlackPixelsArrf[iWid_BlackPixelsf] = -1;

								} //for (iWid_BlackPixelsf = nTopOfBlackPixelsf; iWid_BlackPixelsf <= nBottomOfBlackPixelsf; iWid_BlackPixelsf++)

								//nBottomOfBlackPixels_PrevValidf = -1;
								//nTopOfBlackPixels_PrevValidf = -1;

							}//if (nBottomOfBlackPixelsf != -1 && nTopOfBlackPixelsf != -1)

							nBottomOfBlackPixelsf = -1;
							nTopOfBlackPixelsf = -1;

#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n 'break' for (iLenf... at iWidf = %d (left), nLenOfABlackAreaCurf = %d > ..., nNumOfBlackPixelsInALineTotf = %d",
								iWidf, nLenOfABlackAreaCurf, nNumOfBlackPixelsInALineTotf);

							//fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							break; //for (iLenf = 0; iLenf < nLenOfObjectBoundaryCurf; iLenf++)
							//goto MarkEndOfiWid_Left; //for (iWidf = 0; iWidf < sColor_Imagef->nLenth; iWidf++)

						} // if (nLenOfABlackAreaCurf > nLenOfABlackAreaMaxf)

					} // if (nRedCurf == nIntensityStatBlack)

				} //for (iLenf = 0; iLenf < nLenOfObjectBoundaryCurf; iLenf++)

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n The end for (iLenf... at iWidf = %d (left), nNumOfBlackPixelsInALineTotf = %d, nLenOfObjectBoundaryCurf = %d",
					iWidf, nNumOfBlackPixelsInALineTotf, nLenOfObjectBoundaryCurf);

				fprintf(fout_lr, "\n nBottomOfBlackPixelsf = %d, nTopOfBlackPixelsf = %d", nBottomOfBlackPixelsf, nTopOfBlackPixelsf);

				if (iWidf > 0)
				{
					fprintf(fout_lr, "\n nLeftBoundaryOfBlackPixelsArrf[%d] = %d, nRightBoundaryOfBlackPixelsArrf[%d] = %d, nLeftBoundaryOfBlackPixelsArrf[iWidf - 1] = %d",
						iWidf, nLeftBoundaryOfBlackPixelsArrf[iWidf], iWidf, nRightBoundaryOfBlackPixelsArrf[iWidf], nLeftBoundaryOfBlackPixelsArrf[iWidf - 1]);
				} // if (iWidf > 0)

				//fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				if (nNumOfBlackPixelsInALineTotf == 0)
				{
					nNumOfContinuousRowsWithBlackPixelsTotf = 0;

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n Left: no black pixels in the object area for iWidf = %d, nNumOfContinuousRowsWithBlackPixelsTotf = 0", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				}//if (nNumOfBlackPixelsInALineTotf == 0)
				else //if (nNumOfBlackPixelsInALineTotf > 0)
				{
					nNumOfContinuousRowsWithBlackPixelsTotf += 1;
					
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n Left: the next row nNumOfContinuousRowsWithBlackPixelsTotf = %d with black pixels in the object area for iWidf = %d", 
						nNumOfContinuousRowsWithBlackPixelsTotf,iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
//////////////////////////////////////////////////////////

					if (nNumOfContinuousRowsWithBlackPixelsTotf > nWidOfABlackAreaMaxf)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n Left: continue by nNumOfContinuousRowsWithBlackPixelsTotf = %d > nWidOfABlackAreaMaxf = %d, iWidf = %d", 
							 nNumOfContinuousRowsWithBlackPixelsTotf, nWidOfABlackAreaMaxf, iWidf);
						//fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						if (nBottomOfBlackPixelsf != -1)
						{
							nWidOfABlackAreaCurf = nTopOfBlackPixelsf - nBottomOfBlackPixelsf;

							if (nWidOfABlackAreaCurf > nWidOfABlackAreaMaxf)
							{
								//the width of black area is atypically big -- no erasing of black pixels
													//it could be a black area between the breast and the image edge
								if (nBottomOfBlackPixelsf != -1 && nTopOfBlackPixelsf != -1)
								{
									//nBottomOfBlackPixels_PrevValidf = nBottomOfBlackPixelsf;
									//nTopOfBlackPixels_PrevValidf = nTopOfBlackPixelsf;

									for (iWid_BlackPixelsf = nTopOfBlackPixelsf; iWid_BlackPixelsf <= nBottomOfBlackPixelsf; iWid_BlackPixelsf++)
									{
										nLeftBoundaryOfBlackPixelsArrf[iWid_BlackPixelsf] = -1;
										nRightBoundaryOfBlackPixelsArrf[iWid_BlackPixelsf] = -1;

									} //for (iWid_BlackPixelsf = nTopOfBlackPixelsf; iWid_BlackPixelsf <= nBottomOfBlackPixelsf; iWid_BlackPixelsf++)

									//nBottomOfBlackPixels_PrevValidf = -1;
									//nTopOfBlackPixels_PrevValidf = -1;

								}//if (nBottomOfBlackPixelsf != -1 && nTopOfBlackPixelsf != -1)

								nBottomOfBlackPixelsf = -1;
								nTopOfBlackPixelsf = -1;

#ifndef COMMENT_OUT_ALL_PRINTS
								//fprintf(fout_lr, "\n\n Continue at iWidf = %d (left), nNumOfBlackPixelsInALineTotf = %d", iWidf, nNumOfBlackPixelsInALineTotf);
								
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

								//continue; //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
							} // if (nWidOfABlackAreaCurf > nWidOfABlackAreaMaxf)

						} // if (nBottomOfBlackPixelsf != -1)

						continue;
					}//if (nNumOfContinuousRowsWithBlackPixelsTotf > nWidOfABlackAreaMaxf)

				}//else //if (nNumOfBlackPixelsInALineTotf > 0)


#ifndef COMMENT_OUT_ALL_PRINTS
				//fprintf(fout_lr, "\n 'FillingOut_Black...' (left) 2: sColor_Imagef->nLenObjectBoundary_Arr[5296] = %d", sColor_Imagef->nLenObjectBoundary_Arr[5296]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

////////////////////////////////////////////////////////
 // black pixel(s) is in the prev row but no black pixel has been found in the cur row 
				if (iWidf > 0 && nLeftBoundaryOfBlackPixelsArrf[iWidf - 1] != -1 && nLeftBoundaryOfBlackPixelsArrf[iWidf] == -1)
				{
					//verifying that it is not bottom of a sagging breast
					nDropInBoundary_ForBlackAreaCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] - sColor_Imagef->nLenObjectBoundary_Arr[iWidf];

					//if (iWidf == 56)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n The end of a black area (left)");
						fprintf(fout_lr, "\n iWidf = %d, nBottomOfBlackPixelsf = %d, nTopOfBlackPixelsf = %d, nDropInBoundary_ForBlackAreaCurf = %d",
							iWidf, nBottomOfBlackPixelsf, nTopOfBlackPixelsf, nDropInBoundary_ForBlackAreaCurf);
						fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					} //if (iWidf == 56)

					if (nDropInBoundary_ForBlackAreaCurf < nDropInBoundary_ForBlackAreaMaxf)
					{

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n Checking for erasing: nDropInBoundary_ForBlackAreaCurf = %d < nDropInBoundary_ForBlackAreaMaxf = %d",
							nDropInBoundary_ForBlackAreaCurf, nDropInBoundary_ForBlackAreaMaxf);
						
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						if (nBottomOfBlackPixelsf != -1 && nTopOfBlackPixelsf != -1)
						{
							//erasing black pixels in all lines from 'nBottomOfBlackPixelsf' to 'nTopOfBlackPixelsf'
							for (iWid_BlackPixelsf = nTopOfBlackPixelsf; iWid_BlackPixelsf <= nBottomOfBlackPixelsf; iWid_BlackPixelsf++)
							{
								//if (iWidf == 56)
								{
#ifndef COMMENT_OUT_ALL_PRINTS
									fprintf(fout_lr, "\n\n Left: iWid_BlackPixelsf = %d, sColor_Imagef->nLenObjectBoundary_Arr[iWid_BlackPixelsf] = %d",
										iWid_BlackPixelsf, sColor_Imagef->nLenObjectBoundary_Arr[iWid_BlackPixelsf]);

									fprintf(fout_lr, "\n nDropInBoundary_ForBlackAreaCurf = %d, nBottomOfBlackPixelsf = %d, nTopOfBlackPixelsf = %d",
										nDropInBoundary_ForBlackAreaCurf, nBottomOfBlackPixelsf, nTopOfBlackPixelsf);
									//fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
								} //if (iWidf == 56)

								//for (iLenf = 0; iLenf < nLenOfObjectBoundaryCurf; iLenf++)
								for (iLenf = 0; iLenf < sColor_Imagef->nLenObjectBoundary_Arr[iWid_BlackPixelsf]; iLenf++)
								{
									nIndexOfBlackPixelCurf = iLenf + (iWid_BlackPixelsf*sColor_Imagef->nLength); // nLenMax);

									nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfBlackPixelCurf];

									if (nRedCurf == nIntensityStatBlack)
									{
										nNumOf_ErasedBlackPixelsTotf += 1;
#ifndef COMMENT_OUT_ALL_PRINTS
										fprintf(fout_lr, "\n 'FillingOut_Black...' (left): the next nNumOf_ErasedBlackPixelsTotf = %d, iWid_BlackPixelsf = %d, iLenf = %d",
											nNumOf_ErasedBlackPixelsTotf, iWid_BlackPixelsf, iLenf);
										//fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

										//if (sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfBlackPixelCurf] == 0)
										{
											sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfBlackPixelCurf] = 3; //was replaced

											sColor_Imagef->nRed_Arr[nIndexOfBlackPixelCurf] = nIntensityStatMax;
											sColor_Imagef->nGreen_Arr[nIndexOfBlackPixelCurf] = nIntensityStatMax;
											sColor_Imagef->nBlue_Arr[nIndexOfBlackPixelCurf] = nIntensityStatMax;
										} // if (sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] == 0)

									} // if (nRedCurf == nIntensityStatBlack)

								} //for (iLenf = 0; iLenf < sColor_Imagef->nLenObjectBoundary_Arr[iWid_BlackPixelsf]; iLenf++)

							} // for (iWid_BlackPixelsf = nTopOfBlackPixelsf; iWid_BlackPixelsf <= nBottomOfBlackPixelsf; iWid_BlackPixelsf++)

						} //if (nBottomOfBlackPixelsf != -1 && nTopOfBlackPixelsf != -1)

						nBottomOfBlackPixelsf = -1;
						nTopOfBlackPixelsf = -1;
					} //if (nDropInBoundary_ForBlackAreaCurf < nDropInBoundary_ForBlackAreaMaxf) 
					else //if (nDropInBoundary_ForBlackAreaCurf >= nDropInBoundary_ForBlackAreaMaxf) 
					{
						//the width of black area is atypically big -- no erasing of black pixels

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n The width of black area is atypically large at iWidf = %d (left), nDropInBoundary_ForBlackAreaCurf = %d >= nDropInBoundary_ForBlackAreaMaxf = %d",
							iWidf, nDropInBoundary_ForBlackAreaCurf, nDropInBoundary_ForBlackAreaMaxf);
						//fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						if (nBottomOfBlackPixelsf != -1 && nTopOfBlackPixelsf != -1)
						{
							//nBottomOfBlackPixels_PrevValidf = nBottomOfBlackPixelsf;
							//nTopOfBlackPixels_PrevValidf = nTopOfBlackPixelsf;

							for (iWid_BlackPixelsf = nTopOfBlackPixelsf; iWid_BlackPixelsf <= nBottomOfBlackPixelsf; iWid_BlackPixelsf++)
							{
								nLeftBoundaryOfBlackPixelsArrf[iWid_BlackPixelsf] = -1;
								nRightBoundaryOfBlackPixelsArrf[iWid_BlackPixelsf] = -1;

							} //for (iWid_BlackPixelsf = nTopOfBlackPixelsf; iWid_BlackPixelsf <= nBottomOfBlackPixelsf; iWid_BlackPixelsf++)

							//nBottomOfBlackPixels_PrevValidf = -1;
							//nTopOfBlackPixels_PrevValidf = -1;

						}//if (nBottomOfBlackPixelsf != -1 && nTopOfBlackPixelsf != -1)

						nBottomOfBlackPixelsf = -1;
						nTopOfBlackPixelsf = -1;

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n Continue at iWidf = %d (left), nDropInBoundary_ForBlackAreaCurf = %d, nNumOfBlackPixelsInALineTotf = %d",
							iWidf, nDropInBoundary_ForBlackAreaCurf, nNumOfBlackPixelsInALineTotf);
						fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

						continue;
					} //else  //if (nDropInBoundary_ForBlackAreaCurf >= nDropInBoundary_ForBlackAreaMaxf) 
				} // if (iWidf > 0 && nLeftBoundaryOfBlackPixelsArrf[iWidf - 1] != -1 && nLeftBoundaryOfBlackPixelsArrf[iWidf] == -1)

	///////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n The end of iWidf = %d (left), nNumOfBlackPixelsInALineTotf = %d", iWidf, nNumOfBlackPixelsInALineTotf);
				fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS


#ifndef COMMENT_OUT_ALL_PRINTS
				//fprintf(fout_lr, "\n 'FillingOut_Black...' (left) 3: sColor_Imagef->nLenObjectBoundary_Arr[5296] = %d", sColor_Imagef->nLenObjectBoundary_Arr[5296]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				if (nNumOfBlackPixelsInALineTotf == 0)
				{
					nBottomOfBlackPixelsf = -1;
					nTopOfBlackPixelsf = -1;
				}//if (nNumOfBlackPixelsInALineTotf == 0)

			} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)

		//MarkEndOfiWid_Left: iLenf = 0;
		} // if (sColor_Imagef->nSideOfObjectLocation == -1)
	} // if (sColor_Imagef->nSideOfObjectLocation == -1)

////////////////////////////////////////
//object to the right
	if (sColor_Imagef->nSideOfObjectLocation == 1)
	{
		nBottomOfBlackPixelsf = -1;
		nTopOfBlackPixelsf = -1;

		for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
		{
			nLeftBoundaryOfBlackPixelsArrf[iWidf] = -1;
			nRightBoundaryOfBlackPixelsArrf[iWidf] = -1;

			//start counting from the right
			nLenOfObjectBoundaryCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf];

			if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)
			{
				if (nLenOfObjectBoundaryCurf == -1)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n No boundary/erasing in 'FillingOut_Black...' (right): nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
						nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
					fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					continue;
				} //if (nLenOfObjectBoundaryCurf == -1)

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n An error in 'FillingOut_Black...' (right): nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);
				printf("\n\n An error in 'FillingOut_Black...' (right): nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d, iWidf = %d",
					nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1, iWidf);

				printf("\n\n Please press any key to exit");	getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				delete[] nLeftBoundaryOfBlackPixelsArrf;
				delete[] nRightBoundaryOfBlackPixelsArrf;
				return UNSUCCESSFUL_RETURN;
			}// if (nLenOfObjectBoundaryCurf < 0 || nLenOfObjectBoundaryCurf > sColor_Imagef->nLength - 1)

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n 'FillingOut_Black...' (right):  iWidf = %d, nLenOfObjectBoundaryCurf = %d, sColor_Imagef->nLength - 1 = %d",
				iWidf, nLenOfObjectBoundaryCurf, sColor_Imagef->nLength - 1);

			fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	
////////////////////////////////////////////////////////////////////////////////////////////////////
			 //start counting from the right
			nNumOfBlackPixelsInALineTotf = 0;
			for (iLenf = sColor_Imagef->nLength - 1; iLenf > nLenOfObjectBoundaryCurf; iLenf--)
			{
				nIndexOfPixelCurf = iLenf + (iWidf*sColor_Imagef->nLength); // nLenMax);

				nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfPixelCurf];

				if (iWidf == 56)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n nRedCurf = %d, iLenf = %d, iWidf = %d", nRedCurf, iLenf, iWidf);
					fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				} //if (iWidf == 56)

				if (nRedCurf == nIntensityStatBlack)
				{
					//if (sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] == 0)
					{
						nNumOfBlackPixelsInALineTotf += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n The next black pixel (right): nNumOfBlackPixelsInALineTotf = %d, nRedCurf = %d, iLenf = %d, iWidf = %d", 
							nNumOfBlackPixelsInALineTotf,nRedCurf, iLenf, iWidf);
						fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
						if (nTopOfBlackPixelsf == -1)
						{
							nTopOfBlackPixelsf = iWidf;
						} //if (nTopOfBlackPixelsf == -1)

						if (nTopOfBlackPixelsf != -1 && nBottomOfBlackPixelsf != iWidf)
						{
							nBottomOfBlackPixelsf = iWidf;
						} //if (nTopOfBlackPixelsf != -1 && ..)

//interchanging the left and right boundaries of black area
						if (nRightBoundaryOfBlackPixelsArrf[iWidf] == -1)
						{
							nRightBoundaryOfBlackPixelsArrf[iWidf] = iLenf;
						} //if (nRightBoundaryOfBlackPixelsArrf[iWidf] == -1)

						if (nRightBoundaryOfBlackPixelsArrf[iWidf] != -1)
						{
							nLeftBoundaryOfBlackPixelsArrf[iWidf] = iLenf; //nRightBoundaryOfBlackPixelsArrf[iWidf] >= nLeftBoundaryOfBlackPixelsArrf[iWidf]
						} //if (nRightBoundaryOfBlackPixelsArrf[iWidf] != -1)

					} // if (sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] == 0)
//////////////////////////////////////////////////////////////

					if (iWidf == 56)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n nNumOfBlackPixelsInALineTotf = %d, iLenf = %d, iWidf = %d", nNumOfBlackPixelsInALineTotf, iLenf, iWidf);
						fprintf(fout_lr, "\n nRightBoundaryOfBlackPixelsArrf[%d] = %d, nLeftBoundaryOfBlackPixelsArrf[%d] = %d", 
							iWidf,nRightBoundaryOfBlackPixelsArrf[iWidf], iWidf, nLeftBoundaryOfBlackPixelsArrf[iWidf]);
						
						fprintf(fout_lr, "\n nTopOfBlackPixelsf = %d, nBottomOfBlackPixelsf = %d", nTopOfBlackPixelsf, nBottomOfBlackPixelsf);
						fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					} //if (iWidf == 56)

					nLenOfABlackAreaCurf = nRightBoundaryOfBlackPixelsArrf[iWidf] - nLeftBoundaryOfBlackPixelsArrf[iWidf];

					if (nLenOfABlackAreaCurf > nLenOfABlackAreaMaxf)
					{
						//the length of black area is atypically large -- no erasing of black pixels
						//it could be a balck area between the breast and the image edge
						if (nBottomOfBlackPixelsf != -1 && nTopOfBlackPixelsf != -1)
						{

#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n The length of black area is atypically large at iWidf = %d (right), nLenOfABlackAreaCurf = %d > nLenOfABlackAreaMaxf = %d",
								iWidf, nLenOfABlackAreaCurf, nLenOfABlackAreaMaxf);
							//fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							for (iWid_BlackPixelsf = nTopOfBlackPixelsf; iWid_BlackPixelsf <= nBottomOfBlackPixelsf; iWid_BlackPixelsf++)
							{
								nLeftBoundaryOfBlackPixelsArrf[iWid_BlackPixelsf] = -1;
								nRightBoundaryOfBlackPixelsArrf[iWid_BlackPixelsf] = -1;
							} //for (iWid_BlackPixelsf = nTopOfBlackPixelsf; iWid_BlackPixelsf <= nBottomOfBlackPixelsf; iWid_BlackPixelsf++)

							if (iWidf == 56)
							{
#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n Exit by nLenOfABlackAreaCurf = %d > nLenOfABlackAreaMaxf = %d, iWidf = %d",
									nLenOfABlackAreaCurf, nLenOfABlackAreaMaxf, iWidf);
								fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
							} //if (iWidf == 56)

	
#ifndef COMMENT_OUT_ALL_PRINTS
							fprintf(fout_lr, "\n\n 'break' for (iLenf... at iWidf = %d (right), nLenOfABlackAreaCurf = %d, nNumOfBlackPixelsInALineTotf = %d",
								iWidf, nLenOfABlackAreaCurf, nNumOfBlackPixelsInALineTotf);
							//fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							nBottomOfBlackPixelsf = -1;
							nTopOfBlackPixelsf = -1;

							break; //for (iLenf = sColor_Imagef->nLength - 1; iLenf > nLenOfObjectBoundaryCurf; iLenf--)
						} //if (nBottomOfBlackPixelsf != -1 && nTopOfBlackPixelsf != -1)

					} // if (nLenOfABlackAreaCurf > nLenOfABlackAreaMaxf)

				} // if (nRedCurf == nIntensityStatBlack)

			} //for (iLenf = sColor_Imagef->nLength - 1; iLenf > nLenOfObjectBoundaryCurf; iLenf--)

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n The end for (iLenf... at iWidf = %d (right), nNumOfBlackPixelsInALineTotf = %d, nLenOfObjectBoundaryCurf = %d",
				iWidf, nNumOfBlackPixelsInALineTotf, nLenOfObjectBoundaryCurf);;

			fprintf(fout_lr, "\n nBottomOfBlackPixelsf = %d, nTopOfBlackPixelsf = %d", nBottomOfBlackPixelsf, nTopOfBlackPixelsf);

			if (nNumOfBlackPixelsInALineTotf == 0)
			{
				fprintf(fout_lr, "\n\n Right: no black pixels in the object area for iWidf = %d", iWidf);
			}//if (nNumOfBlackPixelsInALineTotf == 0)

			if (iWidf > 0)
			{
				fprintf(fout_lr, "\n nLeftBoundaryOfBlackPixelsArrf[%d] = %d, nRightBoundaryOfBlackPixelsArrf[%d] = %d, nLeftBoundaryOfBlackPixelsArrf[iWidf - 1] = %d",
					iWidf, nLeftBoundaryOfBlackPixelsArrf[iWidf], iWidf, nRightBoundaryOfBlackPixelsArrf[iWidf], nLeftBoundaryOfBlackPixelsArrf[iWidf - 1]);
			} // if (iWidf > 0)

			//fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
///////////////////////////////////////////////////////////////////////////////////
			if (nNumOfBlackPixelsInALineTotf == 0)
			{
				nNumOfContinuousRowsWithBlackPixelsTotf = 0;

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n Right: no black pixels in the object area for iWidf = %d, nNumOfContinuousRowsWithBlackPixelsTotf = 0", iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			}//if (nNumOfBlackPixelsInALineTotf == 0)
			else //if (nNumOfBlackPixelsInALineTotf > 0)
			{
				nNumOfContinuousRowsWithBlackPixelsTotf += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n Right: the next row nNumOfContinuousRowsWithBlackPixelsTotf = %d with black pixels in the object area for iWidf = %d",
					nNumOfContinuousRowsWithBlackPixelsTotf, iWidf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				//////////////////////////////////////////////////////////

				if (nNumOfContinuousRowsWithBlackPixelsTotf > nWidOfABlackAreaMaxf)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n Right: continue by nNumOfContinuousRowsWithBlackPixelsTotf = %d > nWidOfABlackAreaMaxf = %d, iWidf = %d",
						nNumOfContinuousRowsWithBlackPixelsTotf, nWidOfABlackAreaMaxf, iWidf);
					//fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					if (nBottomOfBlackPixelsf != -1)
					{
						nWidOfABlackAreaCurf = nTopOfBlackPixelsf - nBottomOfBlackPixelsf;

						if (nWidOfABlackAreaCurf > nWidOfABlackAreaMaxf)
						{
							//the width of black area is atypically big -- no erasing of black pixels
												//it could be a black area between the breast and the image edge
							if (nBottomOfBlackPixelsf != -1 && nTopOfBlackPixelsf != -1)
							{
								//nBottomOfBlackPixels_PrevValidf = nBottomOfBlackPixelsf;
								//nTopOfBlackPixels_PrevValidf = nTopOfBlackPixelsf;

								for (iWid_BlackPixelsf = nTopOfBlackPixelsf; iWid_BlackPixelsf <= nBottomOfBlackPixelsf; iWid_BlackPixelsf++)
								{
									nLeftBoundaryOfBlackPixelsArrf[iWid_BlackPixelsf] = -1;
									nRightBoundaryOfBlackPixelsArrf[iWid_BlackPixelsf] = -1;

								} //for (iWid_BlackPixelsf = nTopOfBlackPixelsf; iWid_BlackPixelsf <= nBottomOfBlackPixelsf; iWid_BlackPixelsf++)

	
							}//if (nBottomOfBlackPixelsf != -1 && nTopOfBlackPixelsf != -1)

							nBottomOfBlackPixelsf = -1;
							nTopOfBlackPixelsf = -1;

#ifndef COMMENT_OUT_ALL_PRINTS
						//	fprintf(fout_lr, "\n\n Right: continue at iWidf = %d , nNumOfBlackPixelsInALineTotf = %d", iWidf, nNumOfBlackPixelsInALineTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

							//continue; //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)
						} // if (nWidOfABlackAreaCurf > nWidOfABlackAreaMaxf)

					} // if (nBottomOfBlackPixelsf != -1)

					continue;
				}//if (nNumOfContinuousRowsWithBlackPixelsTotf > nWidOfABlackAreaMaxf)

			}//else //if (nNumOfBlackPixelsInALineTotf > 0)

////////////////////////////////////////////////////////
 // black pixel(s) is in the prev row but no black pixel has been found in the cur row 
			if (iWidf > 0 && nLeftBoundaryOfBlackPixelsArrf[iWidf - 1] != -1 && nLeftBoundaryOfBlackPixelsArrf[iWidf] == -1)
			{

			//verifying that it is not bottom of a sagging breast
				//nDropInBoundary_ForBlackAreaCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1] - sColor_Imagef->nLenObjectBoundary_Arr[iWidf];
//for a right object -- vice versa than for the left one
				nDropInBoundary_ForBlackAreaCurf = sColor_Imagef->nLenObjectBoundary_Arr[iWidf] - sColor_Imagef->nLenObjectBoundary_Arr[iWidf - 1];

				//if (iWidf == 56)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n The end of a black area (right)");
					fprintf(fout_lr, "\n iWidf = %d, nBottomOfBlackPixelsf = %d, nTopOfBlackPixelsf = %d, nDropInBoundary_ForBlackAreaCurf = %d",
						iWidf, nBottomOfBlackPixelsf, nTopOfBlackPixelsf, nDropInBoundary_ForBlackAreaCurf);

					fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				} //if (iWidf == 56)

				if (nDropInBoundary_ForBlackAreaCurf < nDropInBoundary_ForBlackAreaMaxf)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n Checking for erasing: nDropInBoundary_ForBlackAreaCurf = %d < nDropInBoundary_ForBlackAreaMaxf = %d",
						nDropInBoundary_ForBlackAreaCurf, nDropInBoundary_ForBlackAreaMaxf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				//erasing black pixels in all lines from 'nBottomOfBlackPixelsf' to 'nTopOfBlackPixelsf'
					//for (iWid_BlackPixelsf = nBottomOfBlackPixelsf; iWid_BlackPixelsf <= nTopOfBlackPixelsf; iWid_BlackPixelsf++)
					if (nBottomOfBlackPixelsf != -1 && nTopOfBlackPixelsf != -1)
					{
						for (iWid_BlackPixelsf = nTopOfBlackPixelsf; iWid_BlackPixelsf <= nBottomOfBlackPixelsf; iWid_BlackPixelsf++)
						{
							//if (iWidf == 56)
							{
#ifndef COMMENT_OUT_ALL_PRINTS
								fprintf(fout_lr, "\n\n Right: iWid_BlackPixelsf = %d, sColor_Imagef->nLenObjectBoundary_Arr[iWid_BlackPixelsf = %d",
									iWid_BlackPixelsf, sColor_Imagef->nLenObjectBoundary_Arr[iWid_BlackPixelsf]);

								fprintf(fout_lr, "\n nDropInBoundary_ForBlackAreaCurf = %d, nBottomOfBlackPixelsf = %d, nTopOfBlackPixelsf = %d",
									nDropInBoundary_ForBlackAreaCurf, nBottomOfBlackPixelsf, nTopOfBlackPixelsf);
								//fflush(fout_lr);
								fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
							} //if (iWidf == 56)

							
							//for (iLenf = sColor_Imagef->nLength - 1; iLenf > nLenOfObjectBoundaryCurf; iLenf--)
							for (iLenf = sColor_Imagef->nLength - 1; iLenf > sColor_Imagef->nLenObjectBoundary_Arr[iWid_BlackPixelsf]; iLenf--)
							{
								nIndexOfBlackPixelCurf = iLenf + (iWid_BlackPixelsf*sColor_Imagef->nLength); // nLenMax);

								nRedCurf = sColor_Imagef->nRed_Arr[nIndexOfBlackPixelCurf];

								if (nRedCurf == nIntensityStatBlack)
								{
									//if (sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfBlackPixelCurf] == 0)
									{
										nNumOf_ErasedBlackPixelsTotf += 1;

#ifndef COMMENT_OUT_ALL_PRINTS
										fprintf(fout_lr, "\n 'FillingOut_Black...' (right): the next nNumOf_ErasedBlackPixelsTotf = %d, iWid_BlackPixelsf = %d, iLenf = %d",
											nNumOf_ErasedBlackPixelsTotf, iWid_BlackPixelsf, iLenf);
										fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

										sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfBlackPixelCurf] = 3; //was replaced

										sColor_Imagef->nRed_Arr[nIndexOfBlackPixelCurf] = nIntensityStatMax;
										sColor_Imagef->nGreen_Arr[nIndexOfBlackPixelCurf] = nIntensityStatMax;
										sColor_Imagef->nBlue_Arr[nIndexOfBlackPixelCurf] = nIntensityStatMax;
									} // if (sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] == 0)

								} // if (nRedCurf == nIntensityStatBlack)

							} //for (iLenf = sColor_Imagef->nLength - 1; iLenf > sColor_Imagef->nLenObjectBoundary_Arr[iWid_BlackPixelsf]; iLenf--)

						} // for (iWid_BlackPixelsf = nTopOfBlackPixelsf; iWid_BlackPixelsf <= nBottomOfBlackPixelsf; iWid_BlackPixelsf++)

					} // if (nBottomOfBlackPixelsf != -1 && nTopOfBlackPixelsf != -1)

					nBottomOfBlackPixelsf = -1;
					nTopOfBlackPixelsf = -1;

				} //if (nDropInBoundary_ForBlackAreaCurf < nDropInBoundary_ForBlackAreaMaxf)
				else
				{
					//the width of black area is atypically big -- no erasing of black pixels
										//it could be a balck area between the breast and the image edge

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n The width of black area is atypically large at iWidf = %d (right), nDropInBoundary_ForBlackAreaCurf = %d >= nDropInBoundary_ForBlackAreaMaxf = %d",
						iWidf, nDropInBoundary_ForBlackAreaCurf, nDropInBoundary_ForBlackAreaMaxf);
					//fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					if (nBottomOfBlackPixelsf != -1 && nTopOfBlackPixelsf != -1)
					{
						for (iWid_BlackPixelsf = nTopOfBlackPixelsf; iWid_BlackPixelsf <= nBottomOfBlackPixelsf; iWid_BlackPixelsf++)
						{
							nLeftBoundaryOfBlackPixelsArrf[iWid_BlackPixelsf] = -1;
							nRightBoundaryOfBlackPixelsArrf[iWid_BlackPixelsf] = -1;

						} //for (iWid_BlackPixelsf = nTopOfBlackPixelsf; iWid_BlackPixelsf <= nBottomOfBlackPixelsf; iWid_BlackPixelsf++)

					} //if (nBottomOfBlackPixelsf != -1 && nTopOfBlackPixelsf != -1)

					nBottomOfBlackPixelsf = -1;
					nTopOfBlackPixelsf = -1;

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n Continue at iWidf = %d (right), nDropInBoundary_ForBlackAreaCurf = %d, nNumOfBlackPixelsInALineTotf = %d", 
						iWidf, nDropInBoundary_ForBlackAreaCurf,nNumOfBlackPixelsInALineTotf);
					fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

					continue;
				} //else

			} // if (iWidf > 0 && nLeftBoundaryOfBlackPixelsArrf[iWidf - 1] != -1 && nLeftBoundaryOfBlackPixelsArrf[iWidf] == -1)

	///////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n The end of iWidf = %d (right), nNumOfBlackPixelsInALineTotf = %d", iWidf, nNumOfBlackPixelsInALineTotf);
			fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			if (nNumOfBlackPixelsInALineTotf == 0)
			{
				nBottomOfBlackPixelsf = -1;
				nTopOfBlackPixelsf = -1;
			}//if (nNumOfBlackPixelsInALineTotf == 0)

		} //for (iWidf = 0; iWidf < sColor_Imagef->nWidth; iWidf++)

	} // if (sColor_Imagef->nSideOfObjectLocation == 1)

	delete[] nLeftBoundaryOfBlackPixelsArrf;
	delete[] nRightBoundaryOfBlackPixelsArrf;

	return SUCCESSFUL_RETURN;
} //int FillingOut_BlackObjectPixels_WhiteBlack_Image(...

  //printf("\n\n Please press any key:"); getchar();



