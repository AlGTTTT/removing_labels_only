#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

//using namespace imago;

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
//#include <iostream.h>
#include <string.h>
//#include <iomanip.h>
//#include <fstream.h>
#include <assert.h>

#include "image.h"
//#include "median_filter.h"


#define SUCCESSFUL_RETURN 0 //2 and (-2) are also normal returns
#define UNSUCCESSFUL_RETURN (-1)

#define SQRT2 1.414213562
#define BLACK 0
#define WHITE 1

#define MASK_OUTPUT
	//#define COMMENT_OUT_ALL_PRINTS

#define nLenMax 6000 
#define nWidMax 6000 

#define nLenMin 50 
#define nWidMin 50

#define nImageSizeMax (nLenMax*nWidMax)

///////////////////////////////////////////////////////////////////
#define nLarge 1000000
#define fLarge 1.0E+10 //6

#define feps 0.000001
//#define pi 3.14159
#define PI 3.1415926535

//////////////////////////////////////////////////////////////////
	#define nLenForSideOfObjectLocation 300 //600
	#define fPercentOfWhitePixels_ForSideMin 0.9
//////////////////////////////////////////////////////
#define nLenSubimageToPrintMin  0 //342 
#define nLenSubimageToPrintMax  4000  //2500

#define nWidSubimageToPrintMin 0 //185 //3320 //40 //5410 //370 //2470 

#define nOneWidToPrint  226 //56 //371 //2476 //
#define nWidSubimageToPrintMax 300 //250 //3390 //5470 //2670 //470 //3970 

//#define nWidSubimageToPrintMin 9 //10 //3712 //1980 //499 //229 //415 //478 //191 //344 //193 //416 //814 
//#define nWidSubimageToPrintMax 9 //3714 //502 //416 //192 //361 //195 //240 //420
///////////////////////////////////////////////////////////////////////
#define nIntensityStatMin 10 //85 
#define nIntensityStatMinForEqualization 0 //85 

#define nIntensityStatForReadMax 65535

#define nIntensityStatBlack 0 //85 
#define nIntensityStatMax 255 

#define nIntensityStatWhite (nIntensityStatMax)
///////////////////////////////////////////

#define nDiffOfLensBetweenLast_2WhitePixelsMin 220 //200 //100 //80 //60
#define nDistFor_ObjectNeighborsAtCertainAngles (nDiffOfLensBetweenLast_2WhitePixelsMin)
	#define nDistFor_ObjectNeighborsAtCertainAngles_CloseToBoundary 5
	
	#define nLength_HalfRange_CloseToBoundary 60

#define nWidToCloseToEdge 300
#define nLen_ToFindBlackPixels 200 //> nDiffOfLensBetweenLast_2WhitePixelsMin
/////////////////////////////////////////////////////////
//median filter

#define nNumOfItersForMedianFilter 3 //1

///////////////////////////////////////////////////////////
// for 'Smoothing_Boundary_Jaggedness_InAMaskImage'

//#define nBoundaryJumpMax 50
#define nBoundaryJumpMax 20 //10 //5 //50

#define nWidOfJaggedSegmentMax 200

#define nNumOfIters_ForShadingBondaryPixelsMax 100

#define nNumOfIters_ForMedianFiterTo_ObjectAreaMax 50

#define fCoefDist_ForNippleToTopOrBottom 0.3  

#define fCoefDist_ForNippleToTop 0.45
#define fCoefDist_ForNippleToBottom 0.3

#define nIterOfSmoothingMax 6 //3
//////////////////////////////////////////////////////////
//for 'MedianFiterTo_BoundaryArea'
#define nCoeff_DeviationFromBoundaryForMedianFilter_Into 10 //20

#define nCoeff_DeviationFromBoundaryForMedianFilter_Out 10 

#define nRadiusOfMedianFilter_ForBoundary 2 //4

//#define nNumOfIterForMedianFilter_BoundaryMax 25 //100 //400 //200 //
#define nNumOfIterForMedianFilter_BoundaryMax 5 //15 //20 

#define nNumOfIterForMedianFilter_BlackAreasBehindBoundaryMax 4 //15 //20 

//#define nNumOfIterForMedianFilter_InsideMax 2 //15 //20 
#define nNumOfIterForMedianFilter_InsideMax 10 //30 //15 //20 

#define nNumOfIterForSmoothingMax 1 //3 //1 //2

#define nNumOfChangedPixelsMin 100 //5

#define nNumOfChangedPixels_BehindBoundaryMin 1 //50 

////////////////////////////////////////////////////////
// for 'FillingOut_BlackObjectPixels_WhiteBlack_Image'
#define nWidOfABlackAreaMax 300
#define nLenOfABlackAreaMax 200

#define nDropInBoundary_ForBlackAreaMax 20 //10 //200
/////////////////////////////////////////////////////////
#define nRadiusOfMedianFilter 12
/////////////////////////////////////////////////////////////////
#define nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min 4 //40 //20 //10 //5 //3
//#define nLengthOfTheSamePixelIntensities_ForObjectBoundary_Min 90 //350 //250 //200 //150

//#define nLenOfALabel_Max 400

#define fCoefDist_ForVerticalNeighbors_ToImageEdgeMax 0.35 //0.65 

#define fCoefDist_ForWhiteObjectPixels_ToImageEdgeMax 0.15 //0.85

#define nWidDiffFor1stObjectPixelVerification 110 

#define nLenDiffFor1stObjectPixelVerification 250 

//#define nDistBetweenBoundariesOfNeighborWids_Max 150 //90 //70 //50
#define nDistBetweenBoundariesOfNeighborWids_Max 200 //

#define fShareOfWidthForTheSamePixelIntensities_1 0.5

#define fRatioOfWhiteAndBlackPixelsInARowMax 4.0 //0.8
///////////////////////////////////////////////////////////////////////////////////////
//object
// All pixels with intensities <= nIntensityCloseToBlackMax are counted as black ones, i.e. 
// (nIntensityCloseToBlackMax + 1) = nAverIntensOfNeighboing_ObjectPixels_LongMin = nAverIntensOfNeighboing_ObjectPixels_ShortMin = 
// = nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin
#define nIntensityCloseToBlackMax 9 //8 // < nAverIntensOfNeighboing_ObjectPixels_LongMin and < nAverIntensOfNeighboing_ObjectPixels_ShortMin

#define nLenLongOfNeighboring_ObjectPixelsFrTheSameRow 70 //40 //60

#define nLenShortOfNeighboring_ObjectPixelsFrTheSameRow 5 //6 //9 //30

#define nNumOfBlackNeighboing_ObjectPixels_LongMax 4 //5 //<=nLenLongOfNeighboring_ObjectPixelsFrTheSameRow
#define nNumOfBlackNeighboing_ObjectPixels_ShortMax 2 //3

#define nAverIntensOfNeighboing_ObjectPixels_LongMin 10 //12 // > nIntensityCloseToBlackMax
#define nAverIntensOfNeighboing_ObjectPixels_ShortMin 10 //15 // > nIntensityCloseToBlackMax

//background
#define nLenShortOfNeighboring_BackgroundPixelsFrTheSameRow 10 //20
#define nNumOf_NonBlackNeighboing_BackgroundPixels_ShortMax 3

#define nAverIntensOfNeighboing_BackgroundPixels_ShortMax (nIntensityCloseToBlackMax) //5 // <= nIntensityCloseToBlackMax

//smeared boundaries
	#define nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRow 5

	//#define nNumOfBlackNeighboing_ObjectPixels_SmearedRowMax 2 
	#define nAverIntensOfNeighboing_ObjectPixels_SmearedRowMin 210 //(4*255)/5 = 204, i.e. at least 4 out of 5 (nLenOf_ObjectNeighboringPixelsFrTheSame_SmearedRow) must be white

	#define nScaleForPixelSelection_SmearedRow 5
/////////////////////////////////////////////////////////////

typedef struct
{
	int
		nRed;

	int
		nGreen;
	int
		nBlue;

} WEIGHTED_RGB_COLORS;

typedef struct
{
	int nSideOfObjectLocation; //-1 - left, 1 - right

	int nIntensityOfBackground_Red; //
	int nIntensityOfBackground_Green; //
	int nIntensityOfBackground_Blue; //

	int nWidth;
	int nLength;

	int *nLenObjectBoundary_Arr;

	//	int nRed_Arr[nImageSizeMax];
	//	int nGreen_Arr[nImageSizeMax];
	//	int nBlue_Arr[nImageSizeMax];
	int *nRed_Arr;
	int *nGreen_Arr;
	int *nBlue_Arr;

	int *nIsAPixelBackground_Arr;

} COLOR_IMAGE;


typedef struct
{
	int nSideOfObjectLocation; //-1 - left, 1 - right

	int nWidth;
	int nLength;

	int nIs_WidthDivisibleBynScaleForPadding; //1 -- divisible, 0 -- no

	int nNumOfPaddedLengths;

	int *nScaledPaddedWid_Arr;
	int *nScaledPaddedLen_Arr;

	int *nLenObjectBoundary_Arr;

	//int *nPixel_ValidOrNotArr; //1 - valid, 0 - invalid
	int *nMaskImage_Arr; //0-object, 1-background

} MASK_IMAGE;

typedef struct
{
	float fT;

	float fCoefLenArr[4];
	float fCoefWidArr[4];

} COEFS_OF_B_SPLINE_BETWEEN_2_POINTS;

//
typedef struct
{
	int nX_Arr[4];
	int nY_Arr[4];

} FOUR_X_Y_COORDINS_FOR_SPLINE_BETWEEN_2_POINTS; //4 points are used to define a spline beween 2 central poins

typedef struct
{
	int nX_Arr[2];
	int nY_Arr[2];

} TWO_X_Y_COORDINS_FOR_LINE_BETWEEN_2_POINTS; //4 points are used to define a spline beween 2 central poins

